2019-09-05 Phys v26r2
=====================

Release for use with Run 1 or Run 2 data, prepared on the run2-patches branch.
It is based on Gaudi v32r1, LHCb v45r1, Lbcom v23r0p1 and Rec v24r1, and uses LCG_96 with ROOT 6.18.00.

- TMVATransform - Fix some memory leaks
  - See merge request lhcb/Phys!588
- Two minor fixes in Selections
  - See merge request lhcb/Phys!585
