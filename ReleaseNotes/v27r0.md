2021-11-30 Phys v27r0
===

This version uses Rec v25r0, Lbcom v24r0, LHCb v46r0, Gaudi v36r2 and LCG101 with ROOT 6.24.06.

This version is released on `run2-patches` branch and is intended for use with Run1 or Run2 data. For Run3, use a version released on `master` branch
Built relative to Phys [v26r8](../-/tags/v26r8), with the following changes:


### Fixes ~"bug fix" ~workaround

- Fix gcc11 compilation, !984 (@cattanem)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Build | (run2) Rewrite CMake configuration in "modern CMake", !1013 (@clemenci) [LBCOMP-23]
- Remove deprecated use of Boost.bind (cherry pick !1014 + !1016), !1015 (@rmatev)


