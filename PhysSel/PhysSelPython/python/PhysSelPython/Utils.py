###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Set of utilities for handling and general manipulation of Configurables.

"""
from __future__ import print_function

__all__ = ('configurableExists', 'isConfigurable')

__author__ = 'Juan Palacios palacios@physik.uzh.ch'

from GaudiConfUtils import configurableExists
from GaudiConfUtils import isConfigurable

print("Warning: PhysSelPython.Utils deprecated! Use GaudiConfUtils.")
