#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
# test Selection-like classes
#
from pytest import raises
from PhysSelPython.Wrappers import (
    Selection, AutomaticData, MergedSelection, VoidEventSelection,
    EventSelection, NameError, NonEmptyInputLocations,
    IncompatibleInputLocations, ChargedProtoParticleSelection)
from SelPy.configurabloids import MockConfGenerator, DummySequencer
from Configurables import FilterDesktop


def test_automatic_data():
    sel00 = AutomaticData(Location='Phys/Sel00x')
    assert sel00.name() == 'Phys/Sel00x'
    assert sel00.algorithm().name() == 'SELECT:Phys/Sel00x'
    assert sel00.outputLocation() == 'Phys/Sel00x'

    sel00 = AutomaticData(Location='Phys/Sel00x/Particles')
    assert sel00.name() == 'Phys/Sel00x'
    assert sel00.algorithm().name() == 'SELECT:Phys/Sel00x'
    assert sel00.outputLocation() == 'Phys/Sel00x/Particles'


def test_automatic_data_does_not_accept_more_than_one_ctor_argument():
    with raises(TypeError):
        AutomaticData('name', Location='Phys/Sel00')


def test_automatic_data_with_no_location_raises():
    with raises(Exception):
        AutomaticData()


def test_VoidEventSelection():
    sel00 = AutomaticData(Location='Phys/Sel00x/Particles')
    ves00 = VoidEventSelection(
        'VES00', Code='Test (<Location>) > X', RequiredSelection=sel00)
    assert sel00.outputLocation() == ves00.outputLocation()
    assert ves00.outputLocation() == 'Phys/Sel00x/Particles'
    assert ves00.algorithm().Code == "Test ('Phys/Sel00x/Particles') > X"


def test_VoidEventSelection_code_integrity():

    sel00 = AutomaticData(Location='Phys/Sel00x/Particles')

    ves01 = VoidEventSelection(
        'VES01', Code='Test (<Location>) > X', RequiredSelection=sel00)
    coderef = "Test ('Phys/Sel00x/Particles') > X"

    assert ves01.algorithm().Code == coderef

    ves02 = VoidEventSelection(
        'VES02', Code="Test (<Location>) > X", RequiredSelection=sel00)
    assert ves02.algorithm().Code == coderef

    ves03 = VoidEventSelection(
        'VES03', Code="Test ('<Location>') > X", RequiredSelection=sel00)
    assert ves03.algorithm().Code == coderef

    ves04 = VoidEventSelection(
        'VES04', Code='Test (\'<Location>\') > X', RequiredSelection=sel00)
    assert ves04.algorithm().Code == coderef


def test_VoidEventSelection_with_existing_name_raises():
    with raises(NameError):
        VoidEventSelection(
            'VES00',
            Code='blah',
            RequiredSelection=AutomaticData(Location='blah/blah'))


def test_VoidEventSelection_with_existing_configurable_name_raises():
    with raises(NameError):
        selFilter = FilterDesktop('SelFilter0')
        sel = AutomaticData(Location='Phys/Sel')

        VoidEventSelection(
            'SelFilter0', Code='<Location>', RequiredSelection=sel)


def test_selection_with_existing_configurable_name_raises():
    with raises(NameError):
        _fd0 = FilterDesktop('_fd0')
        selFilter = FilterDesktop('SelFilter1')
        Selection('SelFilter1', Algorithm=_fd0)


def test_mergedselection_with_existing_configurable_name_raises():
    with raises(NameError):
        sel00 = AutomaticData(Location='Phys/Sel00')
        sel01 = AutomaticData(Location='Phys/Sel01')
        sel02 = AutomaticData(Location='Phys/Sel02')
        sel03 = AutomaticData(Location='Phys/Sel03')
        selFilter = FilterDesktop('MergeFilter')
        MergedSelection(
            'MergeFilter', RequiredSelections=[sel00, sel01, sel02, sel03])


def test_merged_selection():
    sel00 = AutomaticData(Location='Phys/Sel00/Particles')
    sel01 = AutomaticData(Location='Phys/Sel01/Particles')
    ms = MergedSelection('Merge00And01', RequiredSelections=[sel00, sel01])
    assert ms.name() == 'Merge00And01'
    assert ms.requiredSelections() == [
    ]  # should not export its required selections. Algos contained internally.
    assert ms.outputLocation() == 'Phys/Merge00And01/Particles'
    assert ms._sel.requiredSelections() == [sel00, sel01]


def test_clone_merged_selection():
    sel00 = AutomaticData(Location='Phys/Sel00/Particles')
    sel01 = AutomaticData(Location='Phys/Sel01/Particles')
    ms = MergedSelection(
        'Merge00And01Original', RequiredSelections=[sel00, sel01])
    msClone = ms.clone(name='Merge00And01Clone')
    assert msClone.name() == 'Merge00And01Clone'
    assert msClone.requiredSelections() == [
    ]  # should not export its required selections. Algos contained internally.
    assert msClone.outputLocation() == 'Phys/Merge00And01Clone/Particles'
    assert msClone._sel.requiredSelections() == [sel00, sel01]


def test_merged_selection_with_existing_selection_name_raises():
    with raises(NameError):
        sel00 = AutomaticData(Location='Phys/Sel00')
        sel01 = AutomaticData(Location='Phys/Sel01')

        sel0 = MergedSelection(
            'MergedSel001', RequiredSelections=[sel00, sel01])

        MergedSelection('MergedSel001', RequiredSelections=[sel00])


def test_instantiate_tree(selID='0000'):
    sel00 = AutomaticData(Location='Phys/Sel00/Particles')
    sel01 = AutomaticData(Location='Phys/Sel01/Particles')
    alg0 = MockConfGenerator()
    sel0 = Selection(selID, Algorithm=alg0, RequiredSelections=[sel00, sel01])
    return sel0


def test_outputLocation():
    sel00 = AutomaticData(Location='Phys/Sel00/Particles')
    assert sel00.outputLocation() == 'Phys/Sel00/Particles'
    alg0 = MockConfGenerator()
    sel0 = Selection('SomeName001', Algorithm=alg0)
    assert sel0.outputLocation() == 'Phys/SomeName001/Particles'
    sel1 = Selection(
        'SomeName002',
        OutputBranch='HLT2',
        Algorithm=alg0,
        Extension='HltParticles')
    assert sel1.outputLocation() == 'HLT2/SomeName002/HltParticles'


def test_tree_Inputs_propagated():

    sel00 = AutomaticData(Location='Phys/Sel00/Particles')
    sel01 = AutomaticData(Location='Phys/Sel01/Particles')
    alg0 = MockConfGenerator(Inputs=[])
    sel0 = Selection(
        'Sel001', Algorithm=alg0, RequiredSelections=[sel00, sel01])
    assert len(alg0.Inputs) == 0
    assert len(sel0.algorithm().Inputs) == 2
    assert sel0.algorithm().Inputs.count('Phys/Sel00/Particles') == 1
    assert sel0.algorithm().Inputs.count('Phys/Sel01/Particles') == 1


'''
#This requires the algorithm to raise. Gaudi algorithms do not seem to do this.
def test_selection_with_existing_algo_name_raises() :

    sel02 = AutomaticData(Location = 'Phys/Sel02')
    sel03 = AutomaticData(Location = 'Phys/Sel03')
    alg0 = MockConfGenerator(Inputs = ['Phys/Sel00', 'Phys/Sel01'])
    try :
        sel0 = Selection('Alg003', Algorithm = alg0,
                         RequiredSelections = [sel02, sel03])
        assert alg0.Inputs == ['Phys/Sel00', 'Phys/Sel01']
    except NameError :
        print "NameError caught"
'''


def test_selection_with_existing_selection_name_raises():

    sel02 = AutomaticData(Location='Phys/Sel02/Particles')
    sel03 = AutomaticData(Location='Phys/Sel03/Particles')
    alg0 = MockConfGenerator()

    sel0 = Selection(
        'Sel003', Algorithm=alg0, RequiredSelections=[sel02, sel03])
    try:
        Selection('Sel003', Algorithm=alg0, RequiredSelections=[sel02, sel03])
        assert False, 'NameError exception expected'
    except NameError:
        pass

    assert sel0.algorithm().Inputs == [
        'Phys/Sel02/Particles', 'Phys/Sel03/Particles'
    ]


def test_clone_selection_with_existing_selection_name_raises():

    sel02 = AutomaticData(Location='Phys/Sel02')
    sel03 = AutomaticData(Location='Phys/Sel03')
    alg0 = MockConfGenerator()
    sel0 = Selection(
        'Sel004', Algorithm=alg0, RequiredSelections=[sel02, sel03])

    try:
        sel0.clone(
            name='Sel004', Algorithm=alg0, RequiredSelections=[sel02, sel03])
        assert False, 'NameError exception expected'
    except NameError:
        pass

    assert sel0.algorithm().Inputs == ['Phys/Sel02', 'Phys/Sel03']


def test_clone_selection_with_new_alg():
    sel0 = test_instantiate_tree('0001')
    alg1 = MockConfGenerator(Inputs=[])
    selClone = sel0.clone(name='sel0_clone0', Algorithm=alg1)
    assert len(alg1.Inputs) == 0

    assert len(sel0.algorithm().Inputs) == 2
    assert sel0.algorithm().Inputs.count('Phys/Sel00/Particles') == 1
    assert sel0.algorithm().Inputs.count('Phys/Sel01/Particles') == 1


def test_clone_selection_with_new_Inputs():
    sel = test_instantiate_tree('0002')
    clone00 = AutomaticData(Location='Phys/Clone00/Particles')
    clone01 = AutomaticData(Location='Phys/Clone01/Particles')
    selClone = sel.clone(
        name='sel0_clone1', RequiredSelections=[clone00, clone01])
    assert len(selClone.algorithm().Inputs) == 2
    assert selClone.algorithm().Inputs.count('Phys/Clone00/Particles') == 1
    assert selClone.algorithm().Inputs.count('Phys/Clone01/Particles') == 1


def test_selection_with_name_overlap_doesnt_raise():
    sel02 = AutomaticData(Location='Phys/Sel02/Particles')
    sel03 = AutomaticData(Location='Phys/Sel03/Particles')
    alg0 = MockConfGenerator()
    sel0 = Selection('Sel005', Algorithm=alg0)
    sel1 = Selection('Sel005Loose', Algorithm=alg0)
    assert sel0.outputLocation() == 'Phys/Sel005/Particles'
    assert sel1.outputLocation() == 'Phys/Sel005Loose/Particles'


def test_selection_with_different_Inputs_set_in_algo_raises():
    with raises(IncompatibleInputLocations):
        sel02 = AutomaticData(Location='Phys/Sel02')
        sel03 = AutomaticData(Location='Phys/Sel03')
        alg0 = MockConfGenerator(Inputs=['Phys/Sel00', 'Phys/Sel01'])
        Selection('Sel006', Algorithm=alg0, RequiredSelections=[sel02, sel03])


def test_selection_with_same_Inputs_set_in_algo():
    sel02 = AutomaticData(Location='Phys/Sel02/Particles')
    sel03 = AutomaticData(Location='Phys/Sel03/Particles')
    alg0 = MockConfGenerator(
        Inputs=['Phys/Sel02/Particles', 'Phys/Sel03/Particles'])
    sel0 = Selection(
        'Sel007', Algorithm=alg0, RequiredSelections=[sel02, sel03])
