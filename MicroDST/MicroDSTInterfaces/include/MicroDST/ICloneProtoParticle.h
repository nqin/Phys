/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include "MicroDST/ICloner.h"

// Forward declarations
namespace LHCb {
  class ProtoParticle;
  class Particle;
} // namespace LHCb

/** @class ICloneProtoParticle MicroDST/ICloneProtoParticle.h
 *
 *
 *  @author Juan PALACIOS
 *  @date   2007-12-05
 */
class GAUDI_API ICloneProtoParticle : virtual public MicroDST::ICloner<LHCb::ProtoParticle> {

public:
  /// Interface ID
  DeclareInterfaceID( ICloneProtoParticle, 3, 0 );

  /// Destructor
  virtual ~ICloneProtoParticle() = default;

public:
  /// Clone the given ProtoParticle
  virtual LHCb::ProtoParticle* clone( const LHCb::ProtoParticle* protoParticle,
                                      const LHCb::Particle*      parent = nullptr ) = 0;
};
