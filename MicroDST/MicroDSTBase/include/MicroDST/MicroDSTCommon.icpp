/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
// Implementation file for templated methods of class : MicroDSTCommon
//
// 2007-12-04 : Juan PALACIOS
//-----------------------------------------------------------------------------

#include "GaudiKernel/IDataManagerSvc.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/SmartIF.h"
#include "GaudiKernel/StatusCode.h"

//=============================================================================

template <class PBASE>
StatusCode MicroDSTCommon<PBASE>::initialize() {
  const StatusCode sc = PBASE::initialize();
  if ( sc.isFailure() ) return sc;

  if ( !PBASE::rootInTES().empty() ) m_rootInTES = PBASE::rootInTES();
  this->debug() << "Set rootInTES to " << m_rootInTES << endmsg;

  return sc;
}

//=============================================================================

template <class PBASE>
StatusCode MicroDSTCommon<PBASE>::finalize() {
  return PBASE::finalize();
}

//=============================================================================

template <class PBASE>
template <class CopyFunctor>
const typename CopyFunctor::Type* MicroDSTCommon<PBASE>::copyAndStoreObject( const std::string& from,
                                                                             const std::string& to )

{
  if ( this->msgLevel( MSG::VERBOSE ) ) this->verbose() << "try to get data container" << endmsg;
  typedef typename CopyFunctor::Type _type;
  if ( exist<_type>( to ) ) {
    this->Warning( "Object " + to + " already exists. Not cloning.", StatusCode::SUCCESS, 0 ).ignore();
    return this->get<_type>( to );
  }
  if ( exist<_type>( from ) ) {
    const _type* data = this->get<_type>( from );
    if ( this->msgLevel( MSG::VERBOSE ) ) this->verbose() << "now copy information" << endmsg;
    auto newData = CopyFunctor::copy( data );
    newData->setVersion( data->version() ); // preserve original version
    this->put( newData, to );
    if ( this->msgLevel( MSG::VERBOSE ) ) this->verbose() << "Data values set to\n" << *newData << "\n" << endmsg;
    return newData;
  } else {
    this->Warning( "No data container found at " + from, StatusCode::FAILURE, 0 ).ignore();
    return nullptr;
  }
}

//=============================================================================

template <class PBASE>
template <class Cloner>
const typename Cloner::Type::Container* MicroDSTCommon<PBASE>::copyKeyedContainer( const std::string& from,
                                                                                   Cloner*            cloner ) {

  typedef typename Cloner::Type     _type;
  typedef typename _type::Container _container;
  // if ( this->msgLevel(MSG::VERBOSE) )
  //  this->verbose() << "Trying to load data at '" << from << "'" << endmsg;
  const auto data = this->getInputContainer<_container>( from );
  if ( !data ) {
    // if ( this->msgLevel(MSG::VERBOSE) )
    //  this->verbose() << "  -> No data object" << endmsg;
    // this->Warning("copyKeyedContainer found no data at "+from,
    //              StatusCode::FAILURE, 0).ignore();
    return nullptr;
  }

  return this->copyKeyedContainer<Cloner>( data, cloner );
}

//=============================================================================

template <class PBASE>
template <class Cloner>
const typename Cloner::Type::Container*
MicroDSTCommon<PBASE>::copyKeyedContainer( const typename Cloner::Type::Container* from, Cloner* cloner ) {
  typedef typename Cloner::Type     _type;
  typedef typename _type::Container _container;

  if ( !from ) { return nullptr; }

  const auto location        = MicroDST::objectLocation( from );
  const auto cloned_location = outputTESLocation( location );

  if ( this->msgLevel( MSG::VERBOSE ) )
    this->verbose() << "now copy container for location " << location << " to location " << cloned_location << endmsg;

  for ( const auto& i : *from ) { cloner->operator()( i ); }

  auto clones = getOutputContainer<_container>( cloned_location );

  if ( clones ) {
    clones->setVersion( from->version() );

    if ( this->msgLevel( MSG::VERBOSE ) )
      this->verbose() << "copyKeyedContainer copied  " << from->size() << " elements from " << location << " into "
                      << cloned_location << ", size " << clones->size() << endmsg;

    if ( clones->size() != from->size() ) {
      this->Warning( "# entries in cloned " + cloned_location + " differs from " + location ).ignore();
    }

  } else {
    this->Warning( "Failed to clone KeyedContainer " + location, StatusCode::FAILURE, 0 ).ignore();
  }

  return clones;
}

//=============================================================================

template <class PBASE>
template <class itemCloner>
typename itemCloner::Type* MicroDSTCommon<PBASE>::cloneKeyedContainerItem( const typename itemCloner::Type* item ) {
  if ( !item ) {
    if ( this->msgLevel( MSG::DEBUG ) ) this->debug() << "Cannot clone a NULL pointer !" << endmsg;
    return nullptr;
  }

  if ( !item->parent() ) {
    this->Warning( "Cannot clone an object with no parent!" ).ignore();
    return nullptr;
  }

  const auto cloneLocation = outputTESLocation( MicroDST::objectLocation( item->parent() ) );

  auto clones = getOutputContainer<typename itemCloner::Type::Container>( cloneLocation );
  if ( !clones ) return nullptr;

  // Propagate DataObject version from original to clone container
  clones->setVersion( item->parent()->version() );

  // try and get clone
  auto clonedItem = clones->object( item->key() );
  if ( !clonedItem ) {
    if ( this->msgLevel( MSG::DEBUG ) ) {
      this->debug() << "cloneKeyedContainerItem: Cloning item key " << item->key() << " in "
                    << item->parent()->registry()->identifier() << " to " << cloneLocation << endmsg;
      if ( this->msgLevel( MSG::VERBOSE ) ) this->verbose() << *item << endmsg;
    }

    clonedItem = itemCloner::clone( item );
    if ( clonedItem ) {
      clones->insert( clonedItem, item->key() );
      if ( this->msgLevel( MSG::VERBOSE ) ) this->verbose() << "Cloned item " << *clonedItem << endmsg;
    } else {
      if ( this->msgLevel( MSG::DEBUG ) ) this->debug() << "cloneKeyedContainerItem: Cloning FAILED" << endmsg;
    }
  }

  return clonedItem;
}

//=============================================================================

template <class PBASE>
template <class T>
const T* MicroDSTCommon<PBASE>::getStoredClone( const KeyedObject<int>* original ) const {
  return getStoredClone<T>( original );
}

//=============================================================================

template <class PBASE>
template <class T>
T* MicroDSTCommon<PBASE>::getStoredClone( const KeyedObject<int>* original ) {
  if ( !original || !original->parent() ) return nullptr;

  const auto cloneLocation = outputTESLocation( MicroDST::objectLocation( original->parent() ) );

  auto clones = getIfExists<typename T::Container>( cloneLocation );

  return ( clones ? clones->object( original->key() ) : nullptr );
}

//=============================================================================

template <class PBASE>
template <class T>
T* MicroDSTCommon<PBASE>::getOutputContainer( const std::string& location ) {
  auto t = getIfExists<T>( location );
  if ( !t ) {
    t = new T();
    this->put( t, location );
  }
  return t;
}

//=============================================================================

template <class PBASE>
template <class T>
const T* MicroDSTCommon<PBASE>::getInputContainer( const std::string& location ) {
  return getIfExists<T>( location );
}

//=============================================================================

template <class PBASE>
std::string MicroDSTCommon<PBASE>::niceLocationName( const std::string& location ) const {
  std::string        tmp( location );
  const std::string& tmpString = m_rootInTES;
  const auto         loc       = tmp.find( tmpString );
  if ( loc != std::string::npos ) { tmp.replace( loc, tmpString.length(), "" ); }
  return tmp;
}

//=============================================================================

template <class PBASE>
void MicroDSTCommon<PBASE>::selectContainers( const DataObject* obj, std::set<std::string>& names,
                                              const unsigned int classID, const bool forceRead,
                                              const unsigned int depth ) {
  // protect against infinite recursion (should never happen)
  if ( depth > 999999 ) { return; }
  SmartIF<IDataManagerSvc>        mgr( this->eventSvc() );
  typedef std::vector<IRegistry*> Leaves;
  Leaves                          leaves;
  StatusCode                      sc = mgr->objectLeaves( obj, leaves );
  if ( sc ) {
    for ( const auto& leaf : leaves ) {
      const std::string& id  = leaf->identifier();
      DataObject*        tmp = nullptr;
      if ( forceRead ) {
        sc = this->eventSvc()->retrieveObject( id, tmp );
      } else {
        sc = this->eventSvc()->findObject( id, tmp );
      }
      if ( sc && nullptr != tmp ) {
        if ( 0xFFFFFFFF == classID ) {
          if ( tmp->clID() != CLID_DataObject ) {
            this->info() << format( "Class %8.8x (%5d) name ", tmp->clID(), tmp->clID() & 0xFFFF ) << id << endmsg;
          }
        }
        if ( this->msgLevel( MSG::DEBUG ) )
          this->debug() << "Found container '" << id << "' ClassID=" << tmp->clID() << " Type='"
                        << System::typeinfoName( typeid( *tmp ) ) << endmsg;
        if ( tmp->clID() == classID ) {
          if ( this->msgLevel( MSG::DEBUG ) ) this->debug() << " -> Matches target class ID " << classID << endmsg;
          names.insert( id );
        }
        selectContainers( tmp, names, classID, forceRead, depth + 1 );
      }
    }
  }
}

//=============================================================================
