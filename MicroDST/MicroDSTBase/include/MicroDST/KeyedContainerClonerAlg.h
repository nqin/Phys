/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MICRODST_KEYEDCONTAINERCLONERALG_H
#define MICRODST_KEYEDCONTAINERCLONERALG_H 1

// From MicroDST
#include "MicroDST/BindType2ClonerDef.h"
#include "MicroDST/Defaults.h"
#include "MicroDST/MicroDSTAlgorithm.h"

namespace MicroDST {

  /** @class KeyedContainerClonerAlg KeyedContainerClonerAlg.h MicroDST/KeyedContainerClonerAlg.h
   *
   *  Clones objects in Keyed Containers
   *
   *  @author Juan PALACIOS
   *  @date   2008-08-29
   */

  template <typename T>
  class KeyedContainerClonerAlg : public MicroDSTAlgorithm {

  private:
    typedef Defaults<T>                         DEFAULTS;
    typedef Location<T>                         LOCATION;
    typedef typename BindType2Cloner<T>::Cloner CLONER;

  public:
    /// Standard constructor
    KeyedContainerClonerAlg( const std::string& name, ISvcLocator* pSvcLocator )
        : MicroDSTAlgorithm( name, pSvcLocator ), m_cloner( NULL ), m_clonerType( DEFAULTS::clonerType ) {
      declareProperty( "ClonerType", m_clonerType );
    }

    StatusCode initialize() override {
      StatusCode sc = MicroDSTAlgorithm::initialize();
      if ( sc.isFailure() ) return sc;

      if ( inputTESLocation().empty() ) { setInputTESLocation( LOCATION::Default ); }
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << "inputTESLocation() is " << inputTESLocation() << endmsg;

      m_cloner = tool<CLONER>( m_clonerType, this );

      if ( m_cloner ) {
        if ( msgLevel( MSG::DEBUG ) ) debug() << "Found cloner " << m_clonerType << endmsg;
      } else {
        sc = Error( "Failed to find cloner " + m_clonerType );
      }

      return sc;
    }

    StatusCode execute() override {
      for ( const auto& loc : this->inputTESLocations() ) {
        copyKeyedContainer<CLONER>( niceLocationName( loc ), m_cloner );
      }

      setFilterPassed( true );

      return StatusCode::SUCCESS;
    }

  private:
    CLONER*     m_cloner;
    std::string m_clonerType;
  };

} // namespace MicroDST

#endif // MICRODST_KEYEDCONTAINERCLONERALG_H
