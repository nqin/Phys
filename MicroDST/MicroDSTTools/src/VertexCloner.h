/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MICRODST_VERTEXCLONER_H
#define MICRODST_VERTEXCLONER_H 1

#include "ObjectClonerBase.h"

#include <MicroDST/ICloneParticle.h>
#include <MicroDST/ICloneVertex.h>

/** @class VertexCloner VertexCloner.h src/VertexCloner.h
 *
 *
 *  Implementation of the ICloneVertex interface.
 *
 *  Clone an LHCb::Vertex and it's outgoing LHCb::Particles.
 *
 *  @author Juan PALACIOS
 *  @date   2007-11-30
 */
class VertexCloner : public extends1<ObjectClonerBase, ICloneVertex> {

public:
  /// Standard constructor
  VertexCloner( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override;

  LHCb::Vertex* operator()( const LHCb::Vertex* vertex ) override;

private:
  LHCb::Vertex* clone( const LHCb::Vertex* vertex );

private:
  typedef MicroDST::BasicItemCloner<LHCb::Vertex> BasicVertexCloner;

private:
  ICloneParticle* m_particleCloner;

  std::string m_particleClonerName;
};

#endif // MICRODST_VERTEXCLONER_H
