/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from LHCb
#include "Event/WeightsVector.h"

// local
#include "WeightsVectorCloner.h"

//-----------------------------------------------------------------------------
// Implementation file for class : WeightsVectorCloner
//
// 2008-04-01 : Juan PALACIOS
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
WeightsVectorCloner::WeightsVectorCloner( const std::string& type, const std::string& name, const IInterface* parent )
    : base_class( type, name, parent ) {}

//=============================================================================

LHCb::WeightsVector* WeightsVectorCloner::operator()( const LHCb::WeightsVector* weights ) {
  return this->clone( weights );
}

//=============================================================================

LHCb::WeightsVector* WeightsVectorCloner::clone( const LHCb::WeightsVector* weights ) {
  LHCb::WeightsVector* clone = cloneKeyedContainerItem<BasicWeightsVectorCloner>( weights );
  return clone;
}

//=============================================================================

// Declaration of the Tool Factory
DECLARE_COMPONENT( WeightsVectorCloner )
