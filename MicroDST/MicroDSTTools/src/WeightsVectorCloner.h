/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MICRODST_WEIGHTSVECTORCLONER_H
#define MICRODST_WEIGHTSVECTORCLONER_H 1

#include "ObjectClonerBase.h"

#include <MicroDST/ICloneWeightsVector.h>

/** @class WeightsVectorCloner WeightsVectorCloner.h src/WeightsVectorCloner.h
 *
 *  Clone an LHCb::WeightsVector. At the moment this does nothing other than
 *  performing a simple clone and storing the clone in the appropriate
 *  TES location.
 *
 *  @author Juan PALACIOS
 *  @date   2010-10-03
 */

class WeightsVectorCloner : public extends1<ObjectClonerBase, ICloneWeightsVector> {

public:
  /// Standard constructor
  WeightsVectorCloner( const std::string& type, const std::string& name, const IInterface* parent );

  LHCb::WeightsVector* operator()( const LHCb::WeightsVector* track ) override;

private:
  LHCb::WeightsVector* clone( const LHCb::WeightsVector* track );

private:
  typedef MicroDST::BasicCopy<LHCb::WeightsVector> BasicWeightsVectorCloner;
};

#endif // MICRODST_WEIGHTSVECTORCLONER_H
