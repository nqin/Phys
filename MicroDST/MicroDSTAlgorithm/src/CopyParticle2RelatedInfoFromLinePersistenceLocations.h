/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef COPYPARTICLE2RELATEDINFOFROMLINEPERSISTENCELOCATIONS_H
#define COPYPARTICLE2RELATEDINFOFROMLINEPERSISTENCELOCATIONS_H 1

#include <set>
#include <string>
#include <vector>

#include <Event/HltDecReports.h>
#include <Kernel/ILinePersistenceSvc.h>

#include "CopyParticle2RelatedInfo.h"

/** @class CopyParticle2RelatedInfoFromLinePersistenceLocations CopyParticle2RelatedInfoFromLinePersistenceLocations.h
 *
 * @brief Clone TES locations, of LHCb::Particle to LHCb::RelatedInfoMap relations
 * tables, defined by the ILinePersistenceSvc.
 *
 * Most of the actual cloning logic of this algorithm is implemented in the
 * parent class, CopyParticle2RelatedInfo. This algorithm effectively
 * 'interfaces' the base class with the ILinePersistenceSvc.
 *
 * Example usage:
 *
 * @code
 * from Configurables import CopyParticle2RelatedInfoFromLinePersistenceLocations
 * p2relinfo_cloner = CopyParticle2RelatedInfoFromLinePersistenceLocations(
 *     OutputPrefix='/Event/SomeStreamName',
 *     # Clone the locations requested by one Turbo line
 *     LinesToCopy=['Hlt2CharmHadD02KmPipTurbo'],
 *     # Get the list of locations to copy from the TCK
 *     ILinePersistenceSvc='TCKLinePersistenceSvc'
 * )
 * @endcode
 *
 * @see CopyParticle2RelatedInfo as the parent class
 * @see CopyLinePersistenceLocations for cloning KeyedContainer objects
 */
class CopyParticle2RelatedInfoFromLinePersistenceLocations : public CopyParticle2RelatedInfo {

public:
  CopyParticle2RelatedInfoFromLinePersistenceLocations( const std::string& name, ISvcLocator* svcLocator );

  StatusCode initialize() override;

protected:
  const std::vector<std::string>& inputTESLocations() override;

private:
  /// Implementation of ILinePersistenceSvc used to get the list of locations to clone.
  Gaudi::Property<std::string> m_linePersistenceSvcName{this, "ILinePersistenceSvc", "TCKLinePersistenceSvc"};

  /// List of HLT2 lines whose outputs are to be copied.
  Gaudi::Property<std::vector<std::string>> m_linesToCopy{this, "LinesToCopy", {}};

  std::set<std::string> m_linesToCopySet;

  std::vector<std::string> m_tesLocationsFromPersistenceSvc;

  /// TES location of the HltDecReports object to give to ILinePersistenceSvc.
  Gaudi::Property<std::string> m_hltDecReportsLocation{this, "Hlt2DecReportsLocation",
                                                       LHCb::HltDecReportsLocation::Hlt2Default};

  SmartIF<ILinePersistenceSvc> m_linePersistenceSvc;
};
#endif // COPYPARTICLE2RELATEDINFOFROMLINEPERSISTENCELOCATIONS_H
