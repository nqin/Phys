/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CopyParticle2RelatedInfoFromLinePersistenceLocations.h"

DECLARE_COMPONENT( CopyParticle2RelatedInfoFromLinePersistenceLocations )

CopyParticle2RelatedInfoFromLinePersistenceLocations::CopyParticle2RelatedInfoFromLinePersistenceLocations(
    const std::string& name, ISvcLocator* svcLocator )
    : CopyParticle2RelatedInfo( name, svcLocator ) {
  m_linesToCopy.declareUpdateHandler( [this]( auto& ) {
    this->m_linesToCopySet = std::set<std::string>( m_linesToCopy.begin(), m_linesToCopy.end() );
  } );
  m_linesToCopy.useUpdateHandler();
}

StatusCode CopyParticle2RelatedInfoFromLinePersistenceLocations::initialize() {
  StatusCode sc = CopyParticle2RelatedInfo::initialize();
  if ( sc.isFailure() ) { return sc; }

  // We get the list of TES locations to clone dynamically from the
  // ILinePersistenceSvc, so warn the user if they're trying to set the
  // locations manually in the configuration as it will do nothing
  // To check if the value has been set, we compare it to the default
  if ( inputTESLocation() != Location<Particle2RelatedInfo::Table>::Default ) {
    warning() << "The InputLocation and/or InputLocations properties were set, but these will be ignored." << endmsg;
  }

  m_linePersistenceSvc = svc<ILinePersistenceSvc>( m_linePersistenceSvcName.value() );
  if ( !m_linePersistenceSvc ) {
    throw GaudiException( "Could not acquire ILinePersistenceSvc", this->name(), StatusCode::FAILURE );
  }

  return StatusCode::SUCCESS;
}

const std::vector<std::string>& CopyParticle2RelatedInfoFromLinePersistenceLocations::inputTESLocations() {
  m_tesLocationsFromPersistenceSvc.clear();

  const auto decReports = getIfExists<LHCb::HltDecReports>( m_hltDecReportsLocation.value() );
  if ( !decReports ) {
    warning() << "Could not retrieve HltDecReports from " << m_hltDecReportsLocation.value() << endmsg;
  } else {
    const auto tmp = m_linePersistenceSvc->locationsToPersist( *decReports, m_linesToCopySet );
    if ( msgLevel( MSG::VERBOSE ) ) {
      if ( tmp.empty() ) {
        verbose() << "No locations to copy" << endmsg;
      } else {
        verbose() << "Will attempt to copy the following locations:" << endmsg;
      }
      for ( const auto& loc : tmp ) { verbose() << "  - " << loc << endmsg; }
    }
    m_tesLocationsFromPersistenceSvc.assign( std::begin( tmp ), std::end( tmp ) );
  }

  return m_tesLocationsFromPersistenceSvc;
}
