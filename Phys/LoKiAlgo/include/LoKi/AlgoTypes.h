/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_ALGOTYPES_H
#define LOKI_ALGOTYPES_H 1
// ============================================================================
// Include files
// ============================================================================
// LoKiCore
// ============================================================================
#include "LoKi/RangeList.h"
// ============================================================================
// LoKiPhys
// ============================================================================
#include "LoKi/PhysRangeTypes.h"
#include "LoKi/PhysTypes.h"
// ============================================================================
/** @file
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-03-23
 */
// ============================================================================
namespace LoKi {
  // ==========================================================================
  /** @namespace AlgoTypes LoKi/AlgoTypes.h
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2006-03-23
   */
  namespace AlgoTypes {
    // ========================================================================
    /// the actual type of RangeList
    typedef LoKi::RangeList_<LoKi::Types::Range> RangeList;
    // ========================================================================
  } // namespace AlgoTypes
  // ==========================================================================
  namespace Types {
    // ========================================================================
    /// the actual type of RangeList
    // ========================================================================
    typedef LoKi::AlgoTypes::RangeList RangeList;
    // ========================================================================
  } // namespace Types
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
// The END
// ============================================================================
#endif // LOKI_ALGOTYPES_H
