###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Phys/LoKiArrayFunctors
----------------------
#]=======================================================================]

gaudi_add_library(LoKiArrayFunctorsLib
    SOURCES
        src/AChild.cpp
        src/AParticles.cpp
        src/HybridEngine.cpp
        src/HybridEngineActor.cpp
        src/HybridLock.cpp
        src/LoKiArrayFunctors.cpp
        src/LoKiArrayFunctors_dct.cpp
    LINK
        PUBLIC
            Gaudi::GaudiAlgLib
            Gaudi::GaudiKernel
            LHCb::LoKiCoreLib
            LHCb::PhysEvent
            Phys::DaVinciInterfacesLib
            Phys::LoKiPhysLib
            Phys::LoKiProtoParticles
        PRIVATE
            LHCb::LHCbKernel
            LHCb::LHCbMathLib
            LHCb::PartPropLib
            Phys::DaVinciTypesLib
)

gaudi_add_module(LoKiArrayFunctors
    SOURCES
        src/Components/ArrayTupleTool.cpp
        src/Components/CheckOverlap.cpp
        src/Components/DTFDict.cpp
        src/Components/Dict2tuple.cpp
        src/Components/DictOfFunctors.cpp
        src/Components/DictValue.cpp
        src/Components/DummyTransform.cpp
        src/Components/HybridFilterCriterion.cpp
        src/Components/HybridFilterParticles.cpp
        src/Components/HybridParticleArrayFilter.cpp
        src/Components/HybridProtoParticleFilter.cpp
        src/Components/HybridTool.cpp
        src/Components/PlotTool.cpp
        src/Components/PrintDecay.cpp
        src/Components/PrintTool.cpp
        src/Components/TestValue.cpp
        src/Components/TupleTool.cpp
    LINK
        AIDA::aida
        Boost::headers
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        LHCb::LHCbMathLib
        LHCb::LoKiCoreLib
        LHCb::PartPropLib
        LHCb::PhysEvent
        LHCb::RecEvent
        LoKiArrayFunctorsLib
        Phys::DaVinciInterfacesLib
        Phys::DaVinciKernelLib
        Phys::DaVinciTypesLib
        Phys::LoKiPhysLib
        Phys::LoKiUtils
)

gaudi_add_dictionary(LoKiArrayFunctorsDict
    HEADERFILES dict/LoKiArrayFunctorsDict.h
    SELECTION dict/LoKiArrayFunctors.xml
    LINK LoKiArrayFunctorsLib
    OPTIONS ${PHYS_DICT_GEN_DEFAULT_OPTS}
)

gaudi_install(PYTHON)

gaudi_add_tests(QMTest)
