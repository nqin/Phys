#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Test suite for selection module.
'''

__author__ = "Juan PALACIOS juan.palacios@nikhef.nl"

from pytest import raises
from SelPy.configurabloids import MockConfGenerator
from SelPy.selection import EventSelection, NameError


def test_EventSelection_duplicate_name_raises():
    with raises(NameError):
        alg = MockConfGenerator()
        es = EventSelection('EvtUniqueSelNameTest', ConfGenerator=alg)
        EventSelection('EvtUniqueSelNameTest', ConfGenerator=alg)


def test_EventSelection_name():
    alg = MockConfGenerator()
    sel = EventSelection('EvtSelNameTest', ConfGenerator=alg)
    assert sel.name() == 'EvtSelNameTest'


def test_EventSelection_outputLocaiton():
    alg = MockConfGenerator()
    sel = EventSelection('EvtSelOutputTest', ConfGenerator=alg)
    assert sel.outputLocation() == ''
