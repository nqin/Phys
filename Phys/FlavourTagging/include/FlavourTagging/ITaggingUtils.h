/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PHYS_PHYS_FLAVOURTAGGING_ITAGGINGUTILS_H
#define PHYS_PHYS_FLAVOURTAGGING_ITAGGINGUTILS_H 1

#include "Event/Particle.h"
#include "Event/RecVertex.h"
#include "GaudiKernel/IAlgTool.h"
#include <string>

//#include <Kernel/IDistanceCalculator.h>
struct IPVReFitter;
struct ILifetimeFitter;
struct IVertexFit;
struct IDistanceCalculator;
struct IParticleDescendants;

class ReadBDT_bS;

namespace CharmTaggerSpace {
  enum CharmMode { None = -1, Dz2kpi, Dz2kpipipi, Dz2kpipiz, Dp2kpipi, Dz2kpiX, Dz2keX, Dz2kmuX, LambdaC2pkpi, Max };
}

static const InterfaceID IID_ITaggingUtils( "ITaggingUtils", 1, 0 );

/** @class ITaggingUtils ITaggingUtils.h
 *
 *  v1.0
 *  @author Marco Musy (Milano)
 *  @date   2007-02-07
 */

class ITaggingUtils : virtual public IAlgTool {

public:
  /// Retrieve interface ID
  static const InterfaceID& interfaceID() { return IID_ITaggingUtils; };

  virtual StatusCode calcIP( const LHCb::Particle*, const LHCb::VertexBase*, double&, double& ) const = 0;

  virtual StatusCode GetVCHI2NDOF( const LHCb::Particle* sigp, const LHCb::Particle* tagp,
                                   double& VCHI2NDOF ) const = 0;

  virtual StatusCode calcIP( const LHCb::Particle*, const LHCb::RecVertex::ConstVector&, double&, double& ) const = 0;

  virtual StatusCode calcDOCAmin( const LHCb::Particle*, const LHCb::Particle*, const LHCb::Particle*, double&,
                                  double& ) const = 0;

  virtual int countTracks( const LHCb::Particle::ConstVector& ) const noexcept = 0;

  virtual bool isInTree( const LHCb::Particle*              B0Candidate,
                         const LHCb::Particle::ConstVector& daughterCandidates ) const = 0;

  virtual bool isInTree( const LHCb::Particle* B0Candidate, const LHCb::Particle::ConstVector& daughterCandidates,
                         double& distPhi ) const = 0;

  virtual const IPVReFitter*         getPVReFitter() const          = 0;
  virtual const ILifetimeFitter*     getLifetimeFitter() const      = 0;
  virtual const IVertexFit*          getVertexFitter() const        = 0;
  virtual const IDistanceCalculator* getDistanceCalculator() const  = 0;
  virtual IParticleDescendants*      getParticleDescendants() const = 0; // had to break const correctness

  virtual double TPVTAU( const LHCb::Particle*, const LHCb::RecVertex* ) const                              = 0;
  virtual double TPVDIRA( const LHCb::Particle*, const LHCb::RecVertex* ) const                             = 0;
  virtual double TPVFD( const LHCb::Particle*, const LHCb::RecVertex* ) const                               = 0;
  virtual double TPVFDCHI2( const LHCb::Particle*, const LHCb::RecVertex* ) const                           = 0;
  virtual double TPVIPCHI2( const LHCb::Particle*, const LHCb::RecVertex*, const char* id = nullptr ) const = 0;
  virtual bool   isBestPV( const LHCb::Particle*, const LHCb::RecVertex* ) const                            = 0;

  virtual CharmTaggerSpace::CharmMode getCharmDecayMode( const LHCb::Particle*, int ) const = 0;

  // remove candidates with daughters in common with signal B
  virtual LHCb::Particle::ConstVector purgeCands( const LHCb::Particle::Range& cands,
                                                  const LHCb::Particle&        BS ) const = 0;

  virtual double GetSumBDT_ult( const LHCb::Particle* tagp, const LHCb::Particle* sigp, Gaudi::XYZPoint PosPV,
                                Gaudi::XYZPoint PosSV ) const = 0;

  virtual double GetIsoBDT( const LHCb::Particle* axp_one, const LHCb::Particle* axp_two, const Gaudi::XYZPoint& PosPV,
                            const Gaudi::XYZPoint& PosSV ) const = 0;

  virtual void closest_point_with_doca_and_angle( const Gaudi::XYZPoint& o1, const Gaudi::XYZVector& p1,
                                                  const Gaudi::XYZPoint& o2, const Gaudi::XYZVector& p2,
                                                  Gaudi::XYZPoint& vtx, double& doca, double& angle ) const = 0;

  virtual Gaudi::XYZPoint closest_point( const Gaudi::XYZPoint& o, const Gaudi::XYZVector& p,
                                         const Gaudi::XYZPoint& o_mu, const Gaudi::XYZVector& p_mu,
                                         Gaudi::XYZPoint& close1, Gaudi::XYZPoint& close2, bool& fail ) const = 0;

  virtual double get_MINIPCHI2( const LHCb::Particle* p ) const                                 = 0;
  virtual double isolation_fc( const Gaudi::XYZVector& vertex, const Gaudi::XYZVector& p,
                               const Gaudi::XYZVector& p_mu ) const                             = 0;
  virtual double enclosed_angle( const Gaudi::XYZVector& p1, const Gaudi::XYZVector& p2 ) const = 0;
};

#endif // PHYS_PHYS_FLAVOURTAGGING_ITAGGINGUTILS_H
