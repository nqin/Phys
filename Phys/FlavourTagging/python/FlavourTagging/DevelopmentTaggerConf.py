###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import (
    DevelopmentTagger, )

Dev = DevelopmentTagger(name='DevelopmentTagger_Dev')

Dev.Aliases = {
    'BPVIPCHI2': 'BPVIPCHI2()',  # prevent root tuple errors
    'PVndof': 'BPV(VDOF)',
}

Dev.SelectionPipeline = [
    [
        'PT',  # order by first feature
        'ABSID',  # duplicate features, but meh
        'SumBDT_ult',
        'diff_eta',
        'cos_diff_phi',
        'P_proj',
        'diff_z',
        "PT",
        "PX",
        "PY",
        "PZ",
        "P",
        "E",
        "ETA",
        "atan ((PT/PZ))",
        "PHI",
        "MIPDV(PRIMARY)",
        "MIPCHI2DV(PRIMARY)",
        "BPVIP()",
        "BPVIPCHI2()",
        "PIDe",
        "PIDmu",
        "PIDp",
        "PIDK",
        "PROBNNe",  #"PPINFO(PROBNNe)"
        "PROBNNmu",  #"PPINFO(PROBNNmu)"
        "PROBNNk",  #"PPINFO(PROBNNk)"
        "PROBNNpi",  #"PPINFO(PROBNNpi)"
        "PROBNNp",  #"PPINFO(PROBNNp)"
        "PROBNNghost",  #"PPINFO(PROBNNghost)"
        "TRCHI2DOF",
        "TRPCHI2",
        "TRTYPE",
        #"KEY",
        #"MCMOTHER(MCID,-1e6)",
        #"MCMOTHER[MCMOTHER]",
        "Q",
        "CLONEDIST",
        "TRGHOSTPROB",
        # "TRFUN( TrFIRSTHITZ )" ,
        # "TRFUN( TrFITTCHI2/TrFITTNDOF )" ,
        # "TRFUN( TrFITVELOCHI2/TrFITVELONDOF )" ,
        # "TRFUN( TrFITTCHI2 )" ,
        # "TRFUN( TrFITMATCHCHI2 )" ,
        # "switch( HASMUON , 1., 0.)" ,
        # "switch( ISMUON  , 1., 0.)" ,
        # "switch( HASRICH , 1., 0.)" ,
        # "PPINFO( LHCb.ProtoParticle.CaloHcalE , -10)" ,
        # "PPINFO( LHCb.ProtoParticle.CaloEcalE , -10)" ,
        # "PPINFO( LHCb.ProtoParticle.CaloPrsE , -10)" ,
        # "PPINFO( LHCb.ProtoParticle.MuonNShared , -1)",
        # "PPINFO( LHCb.ProtoParticle.VeloCharge , -10)",
        'ID',  # ...
        'Q',  # ...
        'AbsIP',
        'BPVIPCHI2',
        'IPErr',
        'IPPUSig',
        'IPSig',
        'IsSignalDaughter',
        'MuonPIDIsMuon',
        'P',
        'PIDK',
        'PIDp',
        'PIDe',
        'PIDmu',
        'PIDp',
        'PP_InAccHcal',
        'PP_VeloCharge',
        # these PROBNNs will use loki functors, which will essentially read
        # the MC12TuneV2
        'PROBNNe',
        'PROBNNk',
        'PROBNNmu',
        'PROBNNp',
        'PROBNNpi',
        # these PROBNNs are manually tuned within FeatureGenerator
        'PROBNNe_MC12TuneV4',
        'PROBNNk_MC12TuneV4',
        'PROBNNmu_MC12TuneV4',
        'PROBNNp_MC12TuneV4',
        'PROBNNpi_MC12TuneV4',
        'PROBNNghost_MC12TuneV4',
        'PROBNNe_MC15TuneV1',
        'PROBNNk_MC15TuneV1',
        'PROBNNmu_MC15TuneV1',
        'PROBNNp_MC15TuneV1',
        'PROBNNpi_MC15TuneV1',
        'PROBNNghost_MC15TuneV1',
        'PVndof',
        'Signal_TagPart_PT',
        'Signal_TagPart_CHI2NDOF',
        'TRCHI2DOF',
        'TRGHP',
        'TRLH',
        'TRTYPE',
        'DeltaQ',
        'DeltaR',
        'etaDistance',
        'phiDistance',
        'eOverP',
        'minPhiDistance',
        'countTracks',
        'nPV',
        'Signal_PT',
    ],
]
