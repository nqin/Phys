###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Configurables import InclusiveTagger, InclusiveTaggerClassifierFactory

from FlavourTagging.ClassicTunings import setTuningProperties

Development = InclusiveTagger(name='InclusiveTagger')
Development.SelectionPipeline = [[
    'PROBNNe',
    'PROBNNghost',
    'PROBNNk',
    'PROBNNmu',
    'PROBNNp',
    'PROBNNpi',
    'P',
    'PT',
    'Q',
    'BPVIP',
    'BPVIPCHI2',
    'SumBDT_ult',
    'PP_VeloCharge',
    'IP_Mother',
    'diff_z',
    'P_proj',
    'cos_diff_phi',
    'diff_eta',
]]

Development.addTool(InclusiveTaggerClassifierFactory, name='IFT_Factory')
Development.ClassifierFactoryName = Development.IFT_Factory.getFullName()
