###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import (
    OSVtxChTagger,
    TaggerVertexChargeTool,
    NNetTool_MLP,
    TaggingUtils,
    SVertexOneSeedTool,
)

from FlavourTagging.ClassicTunings import setTuningProperties

# classic tunings
Stripping21 = TaggerVertexChargeTool(name='OSVtxChTagger_Stripping21')
Stripping21.addTool(NNetTool_MLP)
Stripping21.addTool(SVertexOneSeedTool)
setTuningProperties(Stripping21, 'Stripping21')

Stripping23 = TaggerVertexChargeTool(name='OSVtxChTagger_Stripping23')
Stripping23.addTool(NNetTool_MLP)
Stripping23.addTool(SVertexOneSeedTool)
setTuningProperties(Stripping23, 'Stripping23')

# classic tunings
Development = OSVtxChTagger(name='OSVtxChTagger_Development')
