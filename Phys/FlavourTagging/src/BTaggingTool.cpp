/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "BTaggingTool.h"

#undef DEBUG_CLONES
// uncomment line below to fill clone killing debug counters
//#define DEBUG_CLONES

//--------------------------------------------------------------------------
// Implementation file for class : BTaggingTool
//
//  First Author: Marco Musy
//  maintainers: Miriam Calvo, Marc Grabalosa, Stefania Vecchi
//--------------------------------------------------------------------------

using namespace LHCb;
using namespace Gaudi::Units;

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( BTaggingTool )

//=========================================================================
BTaggingTool::BTaggingTool( const std::string& type, const std::string& name, const IInterface* parent )
    : GaudiTool( type, name, parent ) {
  declareInterface<IBTaggingTool>( this );
}

//==========================================================================
StatusCode BTaggingTool::initialize() {
  const StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  // retrieve/instantiate taggers and establish call order
  // 1.) loop over names given in the CallOrder vector
  for ( auto enabledTaggerName : m_enabledTaggersCallOrder ) {
    // check if name is in the map
    auto enabledTaggerAndTool = m_enabledTaggersMap.find( enabledTaggerName );
    if ( enabledTaggerAndTool != m_enabledTaggersMap.end() ) {
      // get TaggerType
      auto taggerType = Tagger::TaggerTypeToType( enabledTaggerAndTool->first );

      // if TaggerType is invalid, throw warning and erase from map
      if ( taggerType != Tagger::TaggerType::unknown && taggerType != Tagger::TaggerType::none ) {
        m_activeTaggers.push_back( {taggerType, tool<ITagger>( enabledTaggerAndTool->second, this )} );
      } else {
        warning() << "TaggerType " << enabledTaggerName << " in CallOrder "
                  << "is none or unknown (according to Tagger.xml)." << endmsg;
      }
      m_enabledTaggersMap.erase( enabledTaggerName );
    } else {
      warning() << "TaggerType " << enabledTaggerName << " in CallOrder "
                << "could not be found in EnabledTaggersMap" << endmsg;
    }
  }

  // 2.) loop over remaining taggers in enabledTaggersMap
  for ( auto enabledTaggerAndTool : m_enabledTaggersMap ) {
    auto taggerType = Tagger::TaggerTypeToType( enabledTaggerAndTool.first );
    // if TaggerType is invalid, throw warning and erase from map
    if ( taggerType != Tagger::TaggerType::unknown && taggerType != Tagger::TaggerType::none ) {
      m_activeTaggers.push_back( {taggerType, tool<ITagger>( enabledTaggerAndTool.second, this )} );
    } else {
      warning() << "TaggerType " << enabledTaggerAndTool.first << " in "
                << "EnabledTaggersMap is none or unknown "
                << "(according to Tagger.xml)." << endmsg;
    }
  }

  // get TaggingUtils
  m_taggingUtils = tool<ITaggingUtils>( "TaggingUtils", this );

  // get ParticleDescendants
  m_partDescends = m_taggingUtils->getParticleDescendants();

  // get PV Refitter if needed
  if ( m_usePVRefit ) m_pvReFitter = m_taggingUtils->getPVReFitter();

  m_parentDVA = Gaudi::Utils::getIDVAlgorithm( contextSvc(), this );
  if ( m_parentDVA == nullptr ) { return Error( "Couldn't get parent DVAlgorithm", StatusCode::FAILURE ); }

  return sc;
}

//==========================================================================
StatusCode BTaggingTool::tag( FlavourTag& flavTag, const Particle* sigPart, const RecVertex* assocVtx,
                              Particle::ConstVector& tagParts ) {
  if ( sigPart == nullptr ) return Error( "No B candidate to tag!" );

  // clear extra info and set overall tag decision to 0
  m_extraInfoToClear.clear();
  flavTag.setDecision( FlavourTag::none );
  flavTag.setTaggedB( sigPart );

  // Load all particles from tagging location and get all primary vertices
  const Particle::Range  allTagParts = get<Particle::Range>( m_tagPartLoc + "/Particles" );
  const RecVertex::Range allVtxs     = get<RecVertex::Range>( RecVertexLocation::Primary );

  const int nAllVtxs                                        = allVtxs.size();
  FeatureGeneratorCache::getInstance().currentOtherVertices = &allVtxs;

  // get refitted associated PV and remaining pileup vertices after refitting
  RecVertex                    assocVtxRefit( 0 ); // 0 here is the key of the vertex
  const RecVertex::ConstVector puVtxs = choosePrimary( sigPart, allVtxs, assocVtx, assocVtxRefit );
  if ( assocVtx == nullptr ) {
    clearExtraInfo();
    return Error( "No associated vertex could be found! Skip." );
  }

  FeatureGeneratorCache::getInstance().currentPileUpVertices = puVtxs;

  // if no tagging particles have been provided, preselect them from common
  // tagging location
  if ( tagParts.empty() ) tagParts = chooseCandidates( sigPart, allTagParts, puVtxs );
  // return the untagged event, if no tagging particles are available
  if ( tagParts.empty() ) return StatusCode::SUCCESS;

  // Loop over activeTaggers and produce tagger objects
  std::vector<Tagger> taggers;
  taggers.reserve( m_activeTaggers.size() );
  for ( auto activeTagger : m_activeTaggers ) {
    auto tagger = activeTagger.second->tag( sigPart, assocVtx, nAllVtxs, tagParts );
    tagger.setType( activeTagger.first );
    taggers.push_back( tagger );
  }

  // fill flavourTag object
  for ( auto tagger : taggers ) { flavTag.addTagger( tagger ); }

  // //----------------------------------------------------------------------
  // // Now combine the individual tagger decisions into one final B flavour
  // // tagging decision. There are different methods that can be used.
  // //  - CombineTaggersProbability.cpp  --> combines according individual
  // //    probabilities (default)
  // //  - CombineTaggersNN.cpp ---> combines using a NNEt that used individual
  // //    probabilities
  // //  - CombineTaggersOSTDR.cpp
  // //  - CombineTaggersTDR.cpp
  // //  - CombineTaggersPID.cpp --> based on the particle type

  // // sigPart is the signal B from selection
  // bool isBd = sigPart->particleID().hasDown();
  // bool isBs = sigPart->particleID().hasStrange();
  // bool isBu = sigPart->particleID().hasUp();

  // ///ForceSignalID
  // if      (m_ForceSignalID=="Bd") { isBd = true;  isBu = false; isBs = false; }
  // else if (m_ForceSignalID=="Bu") { isBd = false; isBu = true;  isBs = false; }
  // else if (m_ForceSignalID=="Bs") { isBd = false; isBu = false; isBs = true;  }

  // int signalType =0;
  // if(isBu || isBd ) signalType=1;
  // else if(isBs) signalType=2;

  // m_combine->combineTaggers(flavTag, taggers, signalType,
  //                           m_CombineWithNNetTagger,
  //                           m_CombineWithCharmTagger);

  //----------------------------------------------------------------------------
  // Clean up and erase extra info added to tagging particles
  for ( auto& tagPart : tagParts ) {
    auto tagPartMutable = const_cast<Particle*>( tagPart );
    if ( tagPart->hasInfo( LHCb::Particle::additionalInfo::FlavourTaggingIPPUs ) ) {
      tagPartMutable->eraseInfo( LHCb::Particle::additionalInfo::FlavourTaggingIPPUs );
    }
    if ( tagPart->hasInfo( LHCb::Particle::additionalInfo::FlavourTaggingTaggerID ) ) {
      tagPartMutable->eraseInfo( LHCb::Particle::additionalInfo::FlavourTaggingTaggerID );
    }
  }

  clearExtraInfo();

  return StatusCode::SUCCESS;
}
//==========================================================================
StatusCode BTaggingTool::tag( FlavourTag& flavTag, const Particle* sigPart ) {
  Particle::ConstVector tagPartsEmpty( 0 );
  return tag( flavTag, sigPart, nullptr, tagPartsEmpty );
}
//==========================================================================
StatusCode BTaggingTool::tag( FlavourTag& flavTag, const Particle* sigPart, const RecVertex* assocVtx ) {
  Particle::ConstVector tagPartsEmpty( 0 );
  return tag( flavTag, sigPart, assocVtx, tagPartsEmpty );
}

//==========================================================================
std::vector<std::string> BTaggingTool::activeTaggerTypeNames() const {
  std::vector<std::string> activeTaggerTypeNames;
  std::transform( m_activeTaggers.begin(), m_activeTaggers.end(), std::back_inserter( activeTaggerTypeNames ),
                  []( const auto& pair ) { return Tagger::TaggerTypeToString( pair.first ); } );
  return activeTaggerTypeNames;
}

//==========================================================================
std::vector<LHCb::Tagger::TaggerType> BTaggingTool::activeTaggerTypes() const {
  std::vector<LHCb::Tagger::TaggerType> activeTaggerTypes;
  std::transform( m_activeTaggers.begin(), m_activeTaggers.end(), std::back_inserter( activeTaggerTypes ),
                  []( const auto& pair ) { return pair.first; } );
  return activeTaggerTypes;
}

//==========================================================================
std::vector<std::string> BTaggingTool::featureNames( LHCb::Tagger::TaggerType taggerType ) const {
  auto activeTagger = getActiveTagger( taggerType );
  if ( activeTagger != nullptr ) {
    return activeTagger->featureNames();
  } else {
    return {};
  }
}

//==========================================================================
std::vector<double> BTaggingTool::featureValues( LHCb::Tagger::TaggerType taggerType ) const {
  auto activeTagger = getActiveTagger( taggerType );
  if ( activeTagger != nullptr ) {
    return activeTagger->featureValues();
  } else {
    return {};
  }
}

//==========================================================================
std::vector<std::string> BTaggingTool::featureNamesTagParts( LHCb::Tagger::TaggerType taggerType ) const {
  auto activeTagger = getActiveTagger( taggerType );
  if ( activeTagger != nullptr ) {
    return activeTagger->featureNamesTagParts();
  } else {
    return {};
  }
}

//==========================================================================
std::vector<std::vector<double>> BTaggingTool::featureValuesTagParts( LHCb::Tagger::TaggerType taggerType ) const {
  auto activeTagger = getActiveTagger( taggerType );
  if ( activeTagger != nullptr ) {
    return activeTagger->featureValuesTagParts();
  } else {
    return {{}};
  }
}

///=========================================================================
const RecVertex::ConstVector BTaggingTool::choosePrimary( const Particle* sigPart, const RecVertex::Range& verts,
                                                          const RecVertex*& assocVtx, RecVertex& assocVtxRefit ) {
  bool                   hasRefitFailed = false;
  RecVertex::ConstVector puVtxs( 0 ); // will contain all the other primary vtx's
  if ( assocVtx ) {                   // PV was given by the user
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Will use the PV given by the user." << endmsg;
    if ( m_usePVRefit ) { //----------------------------- Refit PV without B tracks
      RecVertex        newPV( *assocVtx );
      Particle         newPart( *sigPart );
      const StatusCode sc = m_pvReFitter->remove( &newPart, &newPV );
      if ( !sc ) {
        Error( "ReFitter fails!" ).ignore();
        hasRefitFailed = true;
      } // Should we stop the execution?
      if ( msgLevel( MSG::DEBUG ) ) debug() << " Refitted PV " << endmsg;
      assocVtxRefit = newPV;
    }
  } else if ( m_assocPVCriterion == "bestPV" ) { // choose bestPV according IRelatedPVFinder
    assocVtx = (const RecVertex*)m_parentDVA->bestVertex( sigPart );
    if ( assocVtx ) {
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "Will use the bestPV criteria found z=" << assocVtx->position().z() << endmsg;
    } else {
      Error( "No bextPV vertex found! Skip." ).ignore();
      return puVtxs;
    }

    if ( m_usePVRefit ) { //----------------------------- Refit PV without B tracks
      RecVertex        newPV( *assocVtx );
      Particle         newPart( *sigPart );
      const StatusCode sc = m_pvReFitter->remove( &newPart, &newPV );
      if ( !sc ) {
        Error( "ReFitter fails!" ).ignore();
        hasRefitFailed = true;
      } // Should we stop the execution?
      if ( msgLevel( MSG::DEBUG ) ) debug() << " Refitted PV " << endmsg;
      assocVtxRefit = newPV;
    }
  } else {
    if ( msgLevel( MSG::DEBUG ) )
      debug() << "Will NOT use the bestPV criteria. Loop over the " << verts.size() << " vertices" << endmsg;
    double kdmin = 1000000;
    // -------------------------------------------------- Loop over the PV-vertices
    for ( RecVertex::Range::const_iterator iv = verts.begin(); iv != verts.end(); ++iv ) {
      RecVertex newPV( **iv );
      double    var, ip, iperr;
      if ( m_usePVRefit ) { //----------------------------- Refit PV without B tracks
        Particle         newPart( *sigPart );
        const StatusCode sc = m_pvReFitter->remove( &newPart, &newPV );
        if ( msgLevel( MSG::DEBUG ) ) debug() << " Refitted PV " << endmsg;
        if ( !sc ) {
          Error( "ReFitter fails!" ).ignore();
          continue;
        }
        m_taggingUtils->calcIP( sigPart, &newPV, ip, iperr ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        var = fabs( ip );
      } else {
        m_taggingUtils->calcIP( sigPart, *iv, ip, iperr ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      }
      if ( m_assocPVCriterion == "PVbyIP" ) { // cheated sel needs this
        if ( msgLevel( MSG::DEBUG ) ) debug() << "Will use the PVbyIP criteria " << endmsg;
        var = fabs( ip );
      } else if ( m_assocPVCriterion == "PVbyIPs" ) {
        if ( msgLevel( MSG::DEBUG ) ) debug() << "Will use the PVbyIPs criteria " << endmsg;
        if ( !iperr ) {
          Error( "IPerror zero or nan, skip vertex" ).ignore();
          continue;
        }
        var = fabs( ip / iperr );
      } else {
        Error( "Invalid option ChoosePVCriterium: " + m_assocPVCriterion ).ignore();
        return puVtxs;
      }
      if ( var < kdmin ) {
        kdmin    = var;
        assocVtx = ( *iv );
        if ( m_usePVRefit ) assocVtxRefit = newPV;
      }
      if ( !assocVtx ) {
        Error( "No Reconstructed Vertex! Skip. "
               "(If using CheatedSelection, set ChoosePVCriterium to PVbyIP !)" )
            .ignore();
        return puVtxs;
      }
    }
  } // else bestPV

  // build a vector of pileup vertices --------------------------
  double min_chi2PV = 1000;
  double the_chi2PV = 1000;
  int    nPV        = 0;

  if ( !assocVtx ) {
    Error( "No Reconstructed Vertex! -> Skip" ).ignore();
    return puVtxs;
  }

  const double c00 = assocVtx->covMatrix()( 0, 0 );
  const double c11 = assocVtx->covMatrix()( 1, 1 );
  const double c22 = assocVtx->covMatrix()( 2, 2 );

  for ( RecVertex::Range::const_iterator jv = verts.begin(); jv != verts.end(); jv++ ) {
    const double dx = assocVtx->position().x() - ( *jv )->position().x();
    const double dy = assocVtx->position().y() - ( *jv )->position().y();
    const double dz = assocVtx->position().z() - ( *jv )->position().z();

    const double chi2PV = ( ( fabs( c00 ) > 0 ? dx * dx / c00 : 9e9 ) + ( fabs( c11 ) > 0 ? dy * dy / c11 : 9e9 ) +
                            ( fabs( c22 ) > 0 ? dz * dz / c22 : 9e9 ) );

    if ( chi2PV < min_chi2PV ) min_chi2PV = chi2PV;

    if ( chi2PV < 3 ) {
      the_chi2PV = chi2PV;
      nPV++;
      continue;

    } else {
      puVtxs.push_back( *jv );
    }
  }

  if ( ( fabs( the_chi2PV ) > 0 && fabs( min_chi2PV / the_chi2PV - 1. ) > 1.e-5 ) || nPV > 1 ) {
    puVtxs.clear();
    for ( RecVertex::Range::const_iterator jv = verts.begin(); jv != verts.end(); jv++ ) {
      const double dxx = assocVtx->position().x() - ( *jv )->position().x();
      const double dyy = assocVtx->position().y() - ( *jv )->position().y();
      const double dzz = assocVtx->position().z() - ( *jv )->position().z();
      const double chi2PV =
          ( ( fabs( c00 ) > 0 ? dxx * dxx / c00 : 9e9 ) + ( fabs( c11 ) > 0 ? dyy * dyy / c11 : 9e9 ) +
            ( fabs( c22 ) > 0 ? dzz * dzz / c22 : 9e9 ) );

      if ( fabs( chi2PV / min_chi2PV - 1. ) < 1.e-5 )
        continue; // this is the PV
      else
        puVtxs.push_back( *jv );
    }
  }

  // UseReFitPV means that it will use the refitted pV for the ip calculation
  // of taggers and SV building. Do not move this line above puVtxs building
  if ( not m_vetoFailedPVRefits ) { hasRefitFailed = false; }

  if ( m_usePVRefit and ( not hasRefitFailed ) ) assocVtx = ( &assocVtxRefit );
  if ( m_usePVRefit and hasRefitFailed ) assocVtx = NULL;

  if ( !assocVtx ) {
    Error( "No Reconstructed Vertex!! Skip." ).ignore();
    return puVtxs;
  }

  return puVtxs;
}

//=============================================================================
const Particle::ConstVector BTaggingTool::chooseCandidates( const Particle* sigPart, const Particle::Range& parts,
                                                            const RecVertex::ConstVector& PileUpVtx ) {
  Particle::ConstVector tagParts;
  tagParts.reserve( 32 );
  Particle::ConstVector axdaugh = m_partDescends->descendants( sigPart );
  axdaugh.push_back( sigPart );
  std::vector<const LHCb::Particle*> clones;
  for ( Particle::Range::const_iterator ip = parts.begin(); parts.end() != ip; ++ip ) {
    const LHCb::Particle* p     = *ip;
    const ProtoParticle*  proto = p->proto();
    if ( !proto || !proto->track() ) continue;
    if ( 0 == p->charge() ) continue;
    // for now, only allow long tracks
    const bool trackTypeOK = ( proto->track()->type() == LHCb::Track::Types::Long ) ||
                             ( proto->track()->type() == LHCb::Track::Types::Upstream );
    if ( !trackTypeOK ) continue;
    if ( p->p() / GeV < m_cutTagPart_MinP || p->p() / GeV > m_cutTagPart_MaxP ) continue;
    if ( p->pt() / GeV > m_cutTagPart_MaxPT ) continue;
    // exclude tracks too close to the beam line
    if ( p->momentum().theta() < m_cutTagPart_MinTheta ) continue;
    if ( proto->track()->ghostProbability() > m_cutTagPart_MaxGhostProb ) continue;
    // exclude "trivial" clones: particles with same protoparticle or same
    // underlying track
    //
    // FIXME: this uses the first such track for now, since they have the same
    // hit content, this should be ok...
    using TaggingHelpers::isSameTrack;
    using TaggingHelpers::SameTrackStatus;
    using TaggingHelpers::toString;
    SameTrackStatus isSame = TaggingHelpers::DifferentParticles;
    clones.clear();
    BOOST_FOREACH ( const LHCb::Particle* q, tagParts ) {
      isSame = isSameTrack( *p, *q );
      if ( !isSame ) continue;
      // only skip all the rest if actually same track
      if ( isSame >= TaggingHelpers::SameTrack ) break;
      // otherwise, we may need some form of clone killing, because tracks
      // may be slightly different and we want to pick the "best" one, so
      // save the other contenders for "best track"
      clones.push_back( q );
    }
    if ( isSame ) {
      // if it's actually the same underlying track object, we can throw away
      // this candidate because it is already in tagParts
      if ( isSame >= TaggingHelpers::SameTrack ) continue;
    }
    // exclude pre-flagged clones we did not catch ourselves
    if ( proto->track()->hasInfo( LHCb::Track::AdditionalInfo::CloneDist ) ) {
      if ( proto->track()->info( LHCb::Track::AdditionalInfo::CloneDist, 999999. ) < m_cutTagPart_MinCloneDist ) {
        continue;
      }
    }

    // exclude signal tracks themselves
    double distphi( 0 );
    if ( m_taggingUtils->isInTree( p, axdaugh, distphi ) ) continue;
    // exclude tracks too close to the signal
    if ( distphi < m_cutTagPart_MinDistPhi ) continue;

    // CRJ : This is not really allowed -- SV reintroduced for Local FT use
    Particle* c = const_cast<Particle*>( p );
    if ( c->hasInfo( LHCb::Particle::additionalInfo::FlavourTaggingIPPUs ) ) {
      Error( "FlavourTaggingIPPUs info already set: erasing it" )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      c->eraseInfo( LHCb::Particle::additionalInfo::FlavourTaggingIPPUs );
    }
    if ( c->hasInfo( LHCb::Particle::additionalInfo::FlavourTaggingTaggerID ) ) {
      Error( "FlavourTaggingTaggerID info already set: erasing it" )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      c->eraseInfo( LHCb::Particle::additionalInfo::FlavourTaggingTaggerID );
    }

    // calculate the min IP wrt all pileup vtxs
    double ippu( 0 ), ippuerr( 0 );
    m_taggingUtils->calcIP( p, PileUpVtx, ippu, ippuerr ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    // eliminate from tagParts all parts coming from a pileup vtx
    if ( ippuerr ) {
      if ( ippu / ippuerr < m_cutTagPart_MinIPPU ) continue; // preselection cuts

      c->addInfo( LHCb::Particle::additionalInfo::FlavourTaggingIPPUs, ippu / ippuerr );
      m_extraInfoToClear.push_back( c );
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << "particle p=" << p->p() << " ippu_sig " << ippu / ippuerr << endmsg;
    } else {
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "particle p=" << p->p() << " ippu NAN ****" << endmsg; // happens only when there is 1 PV
    }

    // ok, if p is a potential clone, we need to find the "best" track and keep
    // only that one
    if ( clones.empty() ) {
      // no clone, so just store tagger candidate
      tagParts.push_back( p );
    } else {
      // complete list of clones
      clones.push_back( p );
      // functor to sort by quality (want to keep "best" track)
      struct dummy {
        static bool byIncreasingQual( const LHCb::Particle* p, const LHCb::Particle* q ) {
          if ( p->proto() && q->proto() ) {
            if ( p->proto()->track() && q->proto()->track() ) {
              const LHCb::Track& t1 = *p->proto()->track();
              const LHCb::Track& t2 = *q->proto()->track();
              // prefer tracks which have more subdetectors in
              // where TT counts as half a subdetector
              const unsigned nSub1 = ( t1.hasVelo() ? 2 : 0 ) + ( t1.hasTT() ? 1 : 0 ) + ( t1.hasT() ? 2 : 0 );
              const unsigned nSub2 = ( t2.hasVelo() ? 2 : 0 ) + ( t2.hasTT() ? 1 : 0 ) + ( t2.hasT() ? 2 : 0 );
              if ( nSub1 < nSub2 ) return true;
              if ( nSub1 > nSub2 ) return false;
              // if available, prefer lower ghost probability
              const double ghProb1 = t1.ghostProbability();
              const double ghProb2 = t2.ghostProbability();
              if ( -0. <= ghProb1 && ghProb1 <= 1. && -0. <= ghProb2 && ghProb2 <= 1. ) {
                if ( ghProb1 > ghProb2 ) return true;
                if ( ghProb1 < ghProb2 ) return false;
              }
              // prefer longer tracks
              if ( t1.nLHCbIDs() < t2.nLHCbIDs() ) return true;
              if ( t1.nLHCbIDs() > t2.nLHCbIDs() ) return false;
              // for same length tracks, have chi^2/ndf decide
              const double chi1 = t1.chi2() / double( t1.nDoF() );
              const double chi2 = t2.chi2() / double( t2.nDoF() );
              return ( chi1 > chi2 );
            }
          }
          // fall back on a pT comparison (higher is better) as last resort
          return p->pt() < q->pt();
        }
      };
      // sort clones by quality (stable_sort since byIncreasingQual does not
      // impose a total ordering, so use stable_sort to ensure that the
      // resulting order does not depend on sort implementation)
      std::stable_sort( clones.begin(), clones.end(), dummy::byIncreasingQual );
      // remove all potential clones from tagParts
      BOOST_FOREACH ( const LHCb::Particle* q, clones ) {
        // find potential clone in tagParts
        Particle::ConstVector::iterator it = std::find( tagParts.begin(), tagParts.end(), q );
        // if in tagParts, remove
        if ( tagParts.end() != it ) tagParts.erase( it );
      }
      // get rid of the clones

      // make a disjoint set of tracks by removing the worst track which
      // is a clone of another track until the set is clone-free
      for ( std::vector<const LHCb::Particle*>::iterator it = clones.begin(); clones.end() != it; ) {
        bool elim = false;
        for ( std::vector<const LHCb::Particle*>::iterator jt = it + 1; clones.end() != jt; ++jt ) {
          const SameTrackStatus status = isSameTrack( *( *it ), *( *jt ) );
          if ( status ) {
            // it is a redundant track, remove it from clones and
            // start over
            if ( status == TaggingHelpers::ConvertedGamma ) {
              // if we have a converted photon, remove the other leg
              // as well
              jt = clones.erase( jt );
            }
            it   = clones.erase( it );
            elim = true;
            break;
          }
        }
        if ( elim ) continue;
        ++it;
      }

      // insert disjoint set of tracks into tagParts
      if ( !clones.empty() ) tagParts.insert( tagParts.end(), clones.begin(), clones.end() );
    }

    if ( msgLevel( MSG::DEBUG ) )
      debug() << "part ID=" << p->particleID().pid() << " p=" << p->p() / GeV << " pt=" << p->pt() / GeV
              << " PIDm=" << p->proto()->info( ProtoParticle::additionalInfo::CombDLLmu, 0 )
              << " PIDe=" << p->proto()->info( ProtoParticle::additionalInfo::CombDLLe, 0 )
              << " PIDk=" << p->proto()->info( ProtoParticle::additionalInfo::CombDLLk, 0 ) << endmsg;
  }
#ifdef DEBUG_CLONES
  counter( "nCands" ) += tagParts.size();
#endif
  return tagParts;
}

//=========================================================================

void BTaggingTool::clearExtraInfo() {
  for ( std::vector<LHCb::Particle*>::iterator iC = m_extraInfoToClear.begin(); iC != m_extraInfoToClear.end(); ++iC ) {
    if ( ( *iC )->hasInfo( LHCb::Particle::additionalInfo::FlavourTaggingIPPUs ) )
      ( *iC )->eraseInfo( LHCb::Particle::additionalInfo::FlavourTaggingIPPUs );
    if ( ( *iC )->hasInfo( LHCb::Particle::additionalInfo::FlavourTaggingTaggerID ) )
      ( *iC )->eraseInfo( LHCb::Particle::additionalInfo::FlavourTaggingTaggerID );
  }
  m_extraInfoToClear.clear();
}

//=========================================================================
