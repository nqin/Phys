/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// local
#include "BTagging.h"

// from LHCb
#include "Kernel/IBTaggingTool.h"

//-----------------------------------------------------------------------
// Implementation file for class : BTagging
//
// Author: Marco Musy
//-----------------------------------------------------------------------

using namespace LHCb;
using namespace Gaudi::Units;

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( BTagging )

//=======================================================================
// Main execution
//=======================================================================
StatusCode BTagging::execute() {
  for ( std::vector<std::string>::const_iterator iLoc = inputLocations().begin(); iLoc != inputLocations().end();
        ++iLoc ) {
    performTagging( *iLoc );
  }
  setFilterPassed( true );
  return StatusCode::SUCCESS;
}

//=======================================================================

void BTagging::performTagging( const std::string& location ) {

  const std::string particlesLocation = location + "/Particles";

  const Particle::Range parts = getIfExists<Particle::Range>( particlesLocation );
  if ( parts.empty() ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "No particles found at " << particlesLocation << endmsg;
    return;
  }

  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Will tag " << parts.size() << " B hypos!" << endmsg;

  // Make new FT object container
  FlavourTags* tags = new FlavourTags;

  //-------------- loop on signal B candidates from selection
  for ( Particle::Range::const_iterator icandB = parts.begin(); icandB != parts.end(); ++icandB ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Found Particle of type " << ( *icandB )->particleID() << endmsg;
    if ( ( *icandB )->particleID().hasBottom() ) {
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "About to tag candidate B of mass=" << ( *icandB )->momentum().M() / GeV << " GeV" << endmsg;

      FlavourTag* theTag = new FlavourTag;

      //--------------------- TAG IT ---------------------
      // use tool for tagging by just specifing the signal B
      const StatusCode sc = flavourTagging()->tag( *theTag, *icandB );

      // use tool for tagging if you want to specify the Primary Vtx
      // const StatusCode sc = flavourTagging()->tag( *theTag, *icandB, PVertex );

      // use tool for tagging if you want to specify a list of particles
      // const StatusCode sc = flavourTagging()->tag( *theTag, *icandB, PVertex, vtags );
      //--------------------------------------------------
      if ( sc.isFailure() ) {
        Error( "Tagging Tool returned error." ).ignore();
        delete theTag;
        continue;
      } else {
        tags->insert( theTag );
      }

      //--- PRINTOUTS ---
      // print the information in theTag
      if ( msgLevel( MSG::DEBUG ) ) {
        const int tagdecision = theTag->decision();
        if ( tagdecision ) debug() << "Flavour guessed: " << ( tagdecision > 0 ? "b" : "bbar" ) << endmsg;
        debug() << "estimated omega= " << theTag->omega() << endmsg;
        const Particle* tagB = theTag->taggedB();
        if ( tagB ) debug() << "taggedB p=" << tagB->p() / GeV << endmsg;

        /// print Taggers information
        const std::vector<Tagger>& mytaggers = theTag->taggers();
        for ( std::vector<Tagger>::const_iterator itag = mytaggers.begin(); itag != mytaggers.end(); ++itag ) {
          std::string tts = Tagger::TaggerTypeToString( itag->type() );

          debug() << "--> tagger type: " << tts << endmsg;
          debug() << "    decision = " << ( itag->decision() > 0 ? "b" : "bbar" ) << endmsg;
          debug() << "    omega    = " << itag->omega() << endmsg;

          if ( msgLevel( MSG::VERBOSE ) ) {
            const SmartRefVector<LHCb::Particle>& taggerparts = itag->taggerParts();
            for ( SmartRefVector<LHCb::Particle>::const_iterator kp = taggerparts.begin(); kp != taggerparts.end();
                  ++kp ) {
              verbose() << "    ID:" << std::setw( 4 ) << ( *kp )->particleID().pid() << " p= " << ( *kp )->p() / GeV
                        << endmsg;
            }
          }
        }
      }
    }
  }

  // Output to TES, if tags is not empty
  if ( tags->empty() ) {
    delete tags;
  } else {
    put( tags, location + "/" + m_tagLocation );
  }
}

//==========================================================================
