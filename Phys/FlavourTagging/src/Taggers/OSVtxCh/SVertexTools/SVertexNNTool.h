/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: SVertexNNTool.h,v 1.4 2010-01-18 22:17:04 musy Exp $
#ifndef SVERTEXNNTOOL_H
#define SVERTEXNNTOOL_H 1
// Include files
// from Gaudi
#include "FlavourTagging/ITaggingUtils.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/AlgTool.h"

#include "Kernel/ISecondaryVertexTool.h" // Interface
#include "Kernel/IVertexFit.h"

/** @class SVertexNNTool SVertexNNTool.h SVertexNNTool.h
 *
 *  v1.3 This tool takes as input a primary vertex and a particle vector
 *       and returns a secondary vertex
 *  @author Marco Musy
 *  @date   2004-12-14
 */

class SVertexNNTool : public GaudiTool, virtual public ISecondaryVertexTool {

public:
  /// Standard constructor
  SVertexNNTool( const std::string& type, const std::string& name, const IInterface* parent );
  ~SVertexNNTool(); ///< Destructor
  /// Initialize
  StatusCode initialize() override;

  //-------------------------------------------------------------
  std::vector<LHCb::Vertex> buildVertex( const LHCb::RecVertex&, const LHCb::Particle::ConstVector& ) override;
  //-------------------------------------------------------------

private:
  ITaggingUtils*    m_util;
  const IVertexFit* fitter;

  double NNseeding( double P1, double P2, double PT1, double PT2, double B0PT, double IP1, double IP2, double JVZ,
                    double JCHI );

  double SIGMOID( double x );
  double ipprob( double x );
  double ptprob( double x );
};
#endif // SVERTEXNNTOOL_H
