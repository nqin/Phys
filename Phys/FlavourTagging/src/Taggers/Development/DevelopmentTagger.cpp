/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DevelopmentTagger.h"

DECLARE_COMPONENT( DevelopmentTagger )

using namespace std;
using namespace LHCb;

DevelopmentTagger::DevelopmentTagger( const string& type, const string& name, const IInterface* parent )
    : GaudiTool( type, name, parent ) {
  declareInterface<ITagger>( this );
}

StatusCode DevelopmentTagger::initialize() {
  auto sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  m_pipeline = make_unique<Pipeline>();
  m_pipeline->setToolProvider( this );
  m_pipeline->setAliases( m_featureAliases );
  m_pipeline->setPipeline( m_selectionPipeline );

  return StatusCode::SUCCESS;
}

Tagger DevelopmentTagger::tag( const Particle* sigPart, const RecVertex* assocVtx, const int,
                               Particle::ConstVector& /*tagParts*/ ) {
  Tagger tagObject;

  m_pipeline->setReconstructionVertex( assocVtx );
  m_pipeline->setSignalCandidate( sigPart );

  // apply selection to calculate features and fill cache
  const Particle::Range              allTagParts = get<Particle::Range>( m_iFTTagPartsLocation );
  std::vector<const LHCb::Particle*> tagParts2;
  LoKi::Types::Cut                   cuts = LoKi::Particles::IsAParticleInTree( sigPart ) ||
                          LoKi::Particles::HasProtosInTree( sigPart ) || LoKi::Particles::HasTracksInTree( sigPart );
  cuts = !cuts;
  //
  std::copy_if( allTagParts.begin(), allTagParts.end(), std::back_inserter( tagParts2 ), std::cref( cuts ) );

  m_pipeline->applySelection( tagParts2 );

  return tagObject;
}

Tagger DevelopmentTagger::tag( const Particle* sigPart, const RecVertex* assocVtx, RecVertex::ConstVector& puVtxs,
                               Particle::ConstVector& tagParts ) {
  m_pipeline->setPileUpVertices( puVtxs );
  return tag( sigPart, assocVtx, puVtxs.size(), tagParts );
}

vector<string> DevelopmentTagger::featureNames() const { return m_pipeline->mergedFeatureNames(); }

vector<double> DevelopmentTagger::featureValues() const { return m_pipeline->mergedFeatures(); }

vector<string> DevelopmentTagger::featureNamesTagParts() const { return m_pipeline->mergedFeatureNames(); }

vector<vector<double>> DevelopmentTagger::featureValuesTagParts() const { return m_pipeline->mergedFeatureMatrix(); }
