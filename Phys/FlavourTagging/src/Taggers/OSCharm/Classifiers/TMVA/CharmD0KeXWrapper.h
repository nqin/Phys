/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CHARMD0KEXWRAPPER_H
#define CHARMD0KEXWRAPPER_H 1

// Include files

// local
#include "src/TMVAWrapper.h"

namespace MyD0KeXSpace {
  class ReadBDT;
  class PurityTable;
} // namespace MyD0KeXSpace

/** @class CharmD0KeXWrapper CharmD0KeXWrapper.h
 *
 *  Wrapper for D0 -> KeX classifier
 *
 *  @author Jack Timpson Wimberley
 *  @date   2014-02-18
 */
class CharmD0KeXWrapper : public TMVAWrapper {
public:
  CharmD0KeXWrapper( std::vector<std::string>& );
  ~CharmD0KeXWrapper();
  double GetMvaValue( std::vector<double> const& ) override;

protected:
private:
  MyD0KeXSpace::ReadBDT*     mcreader;
  MyD0KeXSpace::PurityTable* purtable;
};
#endif // CHARMD0KEXWRAPPER_H
