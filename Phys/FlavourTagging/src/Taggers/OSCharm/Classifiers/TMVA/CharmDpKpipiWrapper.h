/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CHARMDPKPIPIWRAPPER_H
#define CHARMDPKPIPIWRAPPER_H 1

// Include files

// local
#include "src/TMVAWrapper.h"

namespace MyDpKpipiSpace {
  class ReadBDT;
  class PurityTable;
} // namespace MyDpKpipiSpace

/** @class CharmDpKpipiWrapper CharmDpKpipiWrapper.h
 *
 *  Wrapper for Dp -> Kpipi classifier
 *
 *  @author Jack Timpson Wimberley
 *  @date   2014-02-18
 */
class CharmDpKpipiWrapper : public TMVAWrapper {
public:
  CharmDpKpipiWrapper( std::vector<std::string>& );
  ~CharmDpKpipiWrapper();
  double GetMvaValue( std::vector<double> const& ) override;

protected:
private:
  MyDpKpipiSpace::ReadBDT*     mcreader;
  MyDpKpipiSpace::PurityTable* purtable;
};
#endif // CHARMDPKPIPIWRAPPER_H
