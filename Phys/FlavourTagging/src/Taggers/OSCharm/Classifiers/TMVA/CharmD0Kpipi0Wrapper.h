/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CHARMD0KPIPI0WRAPPER_H
#define CHARMD0KPIPI0WRAPPER_H 1

// Include files

// local
#include "src/TMVAWrapper.h"

namespace MyD0Kpipi0Space {
  class ReadBDT;
  class PurityTable;
} // namespace MyD0Kpipi0Space

/** @class CharmD0Kpipi0Wrapper CharmD0Kpipi0Wrapper.h
 *
 *  Wrapper for D0 -> Kpipi0 classifier
 *
 *  @author Jack Timpson Wimberley
 *  @date   2014-02-18
 */
class CharmD0Kpipi0Wrapper : public TMVAWrapper {
public:
  CharmD0Kpipi0Wrapper( std::vector<std::string>& );
  ~CharmD0Kpipi0Wrapper();
  double GetMvaValue( std::vector<double> const& ) override;

protected:
private:
  MyD0Kpipi0Space::ReadBDT*     mcreader;
  MyD0Kpipi0Space::PurityTable* purtable;
};
#endif // CHARMD0KPIPI0WRAPPER_H
