/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: TaggerElectronTool.h,v 1.13 2010-06-17 17:46:03 mgrabalo Exp $
#ifndef PHYS_PHYS_FLAVOURTAGGING_TAGGERELECTRONTOOL_H
#define PHYS_PHYS_FLAVOURTAGGING_TAGGERELECTRONTOOL_H 1

// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/AlgTool.h"
// from Event
#include "CaloUtils/ICaloElectron.h"
#include "Kernel/ITagger.h"
// from ROOT
#include <TROOT.h>

// from local
#include "FlavourTagging/ITaggingUtils.h"
#include "Kernel/IParticleDescendants.h"
#include "src/INNetTool.h"

class ITaggingClassifier;
class ITaggingClassifierFactory;

/** @class TaggerElectronTool TaggerElectronTool.h
 *
 *  Tool to tag the B flavour with a Electron Tagger
 *
 *  @author Marco Musy
 *  @date   30/06/2005
 */

class TaggerElectronTool : public GaudiTool, virtual public ITagger {

public:
  /// Standard constructor
  TaggerElectronTool( const std::string& type, const std::string& name, const IInterface* parent );
  virtual ~TaggerElectronTool();    ///< Destructor
  StatusCode initialize() override; ///<  initialization
  StatusCode finalize() override;

  LHCb::Tagger::TaggerType taggerType() const override { return LHCb::Tagger::TaggerType::OS_Electron; }

  //-------------------------------------------------------------
  using ITagger::tag;
  LHCb::Tagger tag( const LHCb::Particle*, const LHCb::RecVertex*, const int, LHCb::Particle::ConstVector& ) override;

private:
  std::unique_ptr<ITaggingClassifier> m_classifier        = nullptr;
  ITaggingClassifierFactory*          m_classifierFactory = nullptr;

  Gaudi::Property<std::string> m_classifierFactoryName{this, "ClassifierFactoryName", "OSElectronClassifierFactory",
                                                       "Name of the factory that creates the classifier."};

  ITaggingUtils*        m_util;
  IParticleDescendants* m_descend;
  ICaloElectron*        m_electron;

  std::string m_CombinationTechnique, m_NeuralNetName;

  // properties
  double m_Pt_cut_ele;
  double m_P_cut_ele;
  double m_IPs_cut_ele;
  double m_VeloChMin;
  double m_VeloChMax;
  double m_EoverP, m_EoverPmax, m_lcs_cut_ele;
  double m_AverageOmega;
  double m_ghost_cut_ele;
  double m_PIDe_cut;
  double m_ipPU_cut_ele;
  double m_distPhi_cut_ele;
  double m_ProbMin_ele;
  double m_P0_Cal_ele;
  double m_P1_Cal_ele;
  double m_Eta_Cal_ele;

  double m_ghostProb_ele;
  double m_PIDNNk_cut_ele;
  double m_PIDNNp_cut_ele;
  double m_PIDNNpi_cut_ele;
  double m_PIDNNe_cut_ele;
  double m_PIDNNepi_cut_ele;

  int    m_isMonteCarlo;
  double m_P0e, m_P1e, m_P2e, m_P3e;
};

//===============================================================//
#endif // PHYS_PHYS_FLAVOURTAGGING_TAGGERELECTRONTOOL_H
