/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "NEWKaonOpposite_NN1_reco14.h"

// hack, otherwise: redefinitions...
//
namespace MyMCSpace_reco14 {
#include "weights/NN1_OSK_reco14.dat/TMVAClassification_MLPBNN.class.C"
}

MCReaderCompileWrapper_reco14::MCReaderCompileWrapper_reco14( std::vector<std::string>& names )
    : mcreader_reco14( new MyMCSpace_reco14::ReadMLPBNN( names ) ) {}

MCReaderCompileWrapper_reco14::~MCReaderCompileWrapper_reco14() { delete mcreader_reco14; }

double MCReaderCompileWrapper_reco14::GetMvaValue( std::vector<double> const& values ) {
  return mcreader_reco14->GetMvaValue( values );
}
