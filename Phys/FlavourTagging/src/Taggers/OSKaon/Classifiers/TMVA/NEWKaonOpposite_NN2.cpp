/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "NEWKaonOpposite_NN2.h"

// hack, otherwise: redefinitions...
//
namespace MyDataSpace {
#include "weights/NN2_OSK.dat/TMVAClassification_MLPBNN.class.C"
}

DataReaderCompileWrapper::DataReaderCompileWrapper( std::vector<std::string>& names )
    : datareader( new MyDataSpace::ReadMLPBNN( names ) ) {}

DataReaderCompileWrapper::~DataReaderCompileWrapper() { delete datareader; }

double DataReaderCompileWrapper::GetMvaValue( std::vector<double> const& values ) {
  return datareader->GetMvaValue( values );
}
