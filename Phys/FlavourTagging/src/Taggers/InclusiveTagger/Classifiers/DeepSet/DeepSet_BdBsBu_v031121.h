/*****************************************************************************\
* (c) Copyright 2020-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include <algorithm>
#include <cmath>
#include <fstream>
#include <iostream>
#include <memory>
#include <numeric>
#include <string>
#include <vector>

#include "simplenn/Dense.h"
#include <nlohmann/json.hpp>

namespace DeepSetClassifier {
  template <typename float_type = float>
  class DeepSet_BdBsBu_v031121 : public TaggingClassifierSimpleNN {
  public:
    constexpr static std::size_t in_feature_dim = 18;
    constexpr static std::size_t lat_space_dim  = 18;
    constexpr static std::size_t summed_out_dim = 20;
    constexpr static std::size_t final_out_dim  = 1;

    DeepSet_BdBsBu_v031121( const std::string& filename )
        : layers( [&]() {
          using json = nlohmann::json;
          std::ifstream f{filename};
          std::string   file_content{std::istreambuf_iterator<char>{f}, {}};
          auto          params     = json::parse( file_content );
          auto          weights_l1 = params["weight1"].get<std::vector<float_type>>();
          auto          weights_l2 = params["weight2"].get<std::vector<float_type>>();
          auto          weights_l3 = params["weight3"].get<std::vector<float_type>>();
          auto          weights_l4 = params["weight4"].get<std::vector<float_type>>();
          auto          biases_l1  = params["bias1"].get<std::vector<float_type>>();
          auto          biases_l2  = params["bias2"].get<std::vector<float_type>>();
          auto          biases_l3  = params["bias3"].get<std::vector<float_type>>();
          auto          biases_l4  = params["bias4"].get<std::vector<float_type>>();
          auto          scales     = params["scales"].get<std::vector<float_type>>();
          auto          offsets    = params["offsets"].get<std::vector<float_type>>();

          return std::tuple{l1_t{std::array<simpleNN::procs::ReLU, 18>{},
                                 std::array<simpleNN::procs::linear<float_type>, 18>{
                                     simpleNN::procs::linear<float_type>( 1.0 / scales[0], -offsets[0] / scales[0] ),
                                     simpleNN::procs::linear<float_type>( 1.0 / scales[1], -offsets[1] / scales[1] ),
                                     simpleNN::procs::linear<float_type>( 1.0 / scales[2], -offsets[2] / scales[2] ),
                                     simpleNN::procs::linear<float_type>( 1.0 / scales[3], -offsets[3] / scales[3] ),
                                     simpleNN::procs::linear<float_type>( 1.0 / scales[4], -offsets[4] / scales[4] ),
                                     simpleNN::procs::linear<float_type>( 1.0 / scales[5], -offsets[5] / scales[5] ),
                                     simpleNN::procs::linear<float_type>( 1.0 / scales[6], -offsets[6] / scales[6] ),
                                     simpleNN::procs::linear<float_type>( 1.0 / scales[7], -offsets[7] / scales[7] ),
                                     simpleNN::procs::linear<float_type>( 1.0 / scales[8], -offsets[8] / scales[8] ),
                                     simpleNN::procs::linear<float_type>( 1.0 / scales[9], -offsets[9] / scales[9] ),
                                     simpleNN::procs::linear<float_type>( 1.0 / scales[10], -offsets[10] / scales[10] ),
                                     simpleNN::procs::linear<float_type>( 1.0 / scales[11], -offsets[11] / scales[11] ),
                                     simpleNN::procs::linear<float_type>( 1.0 / scales[12], -offsets[12] / scales[12] ),
                                     simpleNN::procs::linear<float_type>( 1.0 / scales[13], -offsets[13] / scales[13] ),
                                     simpleNN::procs::linear<float_type>( 1.0 / scales[14], -offsets[14] / scales[14] ),
                                     simpleNN::procs::linear<float_type>( 1.0 / scales[15], -offsets[15] / scales[15] ),
                                     simpleNN::procs::linear<float_type>( 1.0 / scales[16], -offsets[16] / scales[16] ),
                                     simpleNN::procs::linear<float_type>( 1.0 / scales[17], -offsets[17] / scales[17] ),
                                 },
                                 biases_l1, weights_l1},
                            l2_t{std::array<simpleNN::procs::ReLU, 18>{}, std::array<simpleNN::procs::id, 18>{},
                                 biases_l2, weights_l2},
                            l3_t{std::array<simpleNN::procs::ReLU, 20>{}, std::array<simpleNN::procs::id, 18>{},
                                 biases_l3, weights_l3},
                            l4_t{std::array<simpleNN::procs::sigmoid, 1>{}, std::array<simpleNN::procs::id, 20>{},
                                 biases_l4, weights_l4}};
        }() ) {}

    template <typename input_type>
    float_type predict( const std::vector<std::vector<input_type>>& input_0 ) const {
      std::vector<std::array<float_type, in_feature_dim>> converted_0( input_0.size() );
      for ( std::size_t i = 0; i < input_0.size(); ++i ) {
        std::copy( input_0[i].begin(), input_0[i].end(), converted_0[i].begin() );
      }
      return predict( converted_0 );
    }

    float_type predict( const std::vector<std::array<float_type, in_feature_dim>>& input_0 ) const {
      std::array<float, in_feature_dim> after_l1;
      std::array<float, lat_space_dim>  after_l2;
      std::array<float, summed_out_dim> after_l3;
      std::array<float, final_out_dim>  output;

      // need to explicitly initialize that one
      std::array<float, lat_space_dim> after_l2_sum;
      after_l2_sum.fill( 0 );

      for ( auto const& track : input_0 ) {
        std::get<0>( layers )( after_l1, track );
        std::get<1>( layers )( after_l2, after_l1 );
        for ( uint32_t i = 0; i < after_l2.size(); ++i ) { after_l2_sum[i] += after_l2[i]; }
      }
      std::get<2>( layers )( after_l3, after_l2_sum );
      std::get<3>( layers )( output, after_l3 );
      return output[0];
    }

    using l1_t = simpleNN::Dense<std::array<simpleNN::procs::ReLU, in_feature_dim>,
                                 std::array<simpleNN::procs::linear<float_type>, in_feature_dim>, float_type>;
    using l2_t = simpleNN::Dense<std::array<simpleNN::procs::ReLU, lat_space_dim>,
                                 std::array<simpleNN::procs::id, in_feature_dim>, float_type>;
    using l3_t = simpleNN::Dense<std::array<simpleNN::procs::ReLU, summed_out_dim>,
                                 std::array<simpleNN::procs::id, lat_space_dim>, float_type>;
    using l4_t = simpleNN::Dense<std::array<simpleNN::procs::sigmoid, final_out_dim>,
                                 std::array<simpleNN::procs::id, summed_out_dim>, float_type>;

  private:
    std::tuple<l1_t, l2_t, l3_t, l4_t> layers;
  };

} // namespace DeepSetClassifier
