/*****************************************************************************\
* (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/////////////////////////////////////////////////
//      AUTO-GENERATED CODE, DO NOT EDIT       //
// gitlab: gitlab.cern.ch/vjevtic/rnngenerator //
// simplenn: gitlab.cern.ch/mschille/simplenn  //
/////////////////////////////////////////////////

#ifndef JSONREADER_H
#define JSONREADER_H

#include <iostream>
#include <iterator>
#include <string>
#include <vector>

#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

namespace RNNClassifier {

  class JSONReader {
  public:
    JSONReader( const std::string& archfile );

    template <typename float_type = float>
    std::vector<float_type> load_weights( int layer, const std::string& tensor_name ) const;

    template <typename float_type = float>
    float_type load_feature_scaling( int layer, const std::string& feature, const std::string& param ) const;

  private:
    boost::property_tree::ptree json_root;
  };

} // namespace RNNClassifier

#endif // JSONREADER_H
