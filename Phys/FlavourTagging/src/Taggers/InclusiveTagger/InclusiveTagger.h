/*****************************************************************************\
* (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PHYS_PHYS_FLAVOURTAGGING_INCLUSIVETAGGER_H
#define PHYS_PHYS_FLAVOURTAGGING_INCLUSIVETAGGER_H 1

#include <fstream>
#include <map>
#include <memory>
#include <sstream>
#include <string>
#include <variant>

// from Gaudi
#include "GaudiAlg/GaudiTool.h"

// from LHCb
#include "Event/FlavourTag.h"

// from Phys
#include "Kernel/ITagger.h"

// local
#include "src/Selection/Pipeline.h"

// SimpleNN Classifier factory
#include "Classifiers/InclusiveTaggerClassifierFactory.h"
#include "Classifiers/InclusiveTaggerLibrary.h"
#include "src/Classification/ITaggingClassifierFactory.h"
#include "src/Classification/TaggingClassifierSimpleNN.h"

/**
 * @brief Tags a candidate using the inclusive tagger
 *
 */

class InclusiveTagger : public GaudiTool, virtual public ITagger {
public:
  InclusiveTagger( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override;

  LHCb::Tagger::TaggerType taggerType() const override;

  LHCb::Tagger tag( const LHCb::Particle* sigPart, const LHCb::RecVertex* assocVtx, const int nPUVtxs,
                    LHCb::Particle::ConstVector& ) override;

  LHCb::Tagger tag( const LHCb::Particle* sigPart, const LHCb::RecVertex* assocVtx,
                    LHCb::RecVertex::ConstVector& puVtxs, LHCb::Particle::ConstVector& tagParts ) override;

  std::vector<std::string> featureNames() const override;

  std::vector<double> featureValues() const override;

  std::vector<std::string> featureNamesTagParts() const override;

  std::vector<std::vector<double>> featureValuesTagParts() const override;

private:
  Gaudi::Property<std::vector<std::vector<std::string>>> m_selectionPipeline{this, "SelectionPipeline", {{}}, ""};
  Gaudi::Property<std::map<std::string, std::string>>    m_featureAliases{this, "Aliases", {}, ""};
  Gaudi::Property<std::string>                           m_classifierFactoryName{
      this, "ClassifierFactoryName", "",
      "Name of the InclusiveTaggerClassifierFactory tool, as configured in GaudiPython."};
  Gaudi::Property<int>         m_maxNumTracks{this, "MaxNumTracks", 100,
                                      "Set the maxiumum number of Tracks that "
                                      "will be selected as input for the iFT "
                                      "Neural Network"};
  Gaudi::Property<std::string> m_sortingFeature{this, "SortingFeature", "PT",
                                                "Set the feature used to sort the tagging tracks."};
  Gaudi::Property<std::string> m_classifierVersion{this, "ClassifierVersion", "", ""};
  Gaudi::Property<std::string> m_weightFileOverride{this, "WeightFileOverride", "",
                                                    ""}; // to be set when using unofficial sets of weights
  std::unique_ptr<Pipeline>    m_pipeline;

  // SimpleNN
  ITaggingClassifierFactory* m_classifierFactory = nullptr;
  RNNClassifierType          m_simpleNNClassifier;
};

#endif // PHYS_PHYS_FLAVOURTAGGING_INCLUSIVETAGGER_H
