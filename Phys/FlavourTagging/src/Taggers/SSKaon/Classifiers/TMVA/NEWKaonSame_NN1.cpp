/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "NEWKaonSame_NN1.h"

// hack, otherwise: redefinitions...
//
namespace MyNN1Space {
#include "weights/NN1_SSK.dat/TMVAClassification_MLPBNN.class.C"
}

NN1ReaderCompileWrapper::NN1ReaderCompileWrapper( std::vector<std::string>& names )
    : nn1reader( new MyNN1Space::ReadMLPBNN( names ) ) {}

NN1ReaderCompileWrapper::~NN1ReaderCompileWrapper() { delete nn1reader; }

double NN1ReaderCompileWrapper::GetMvaValue( std::vector<double> const& values ) {
  return nn1reader->GetMvaValue( values );
}
