/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <cmath>
#include <iostream>
#include <string>
#include <vector>

namespace MyNN1Space {
  class ReadMLPBNN;
}

struct NN1ReaderCompileWrapper {

  NN1ReaderCompileWrapper( std::vector<std::string>& );

  ~NN1ReaderCompileWrapper();

  double GetMvaValue( std::vector<double> const& );

private:
  MyNN1Space::ReadMLPBNN* nn1reader;
};
