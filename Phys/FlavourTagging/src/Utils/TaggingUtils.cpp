/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "Kernel/GetIDVAlgorithm.h"
#include "Kernel/IDVAlgorithm.h"
#include "Kernel/IDistanceCalculator.h"
#include "Kernel/ILifetimeFitter.h"
#include "Kernel/IPVReFitter.h"
#include "Kernel/IParticleDescendants.h"
#include "Kernel/IVertexFit.h"
#include <limits>

#include "TaggingHelpers.h"
#include "TaggingUtils.h"

#include "LoKi/ParticleProperties.h"
#include "LoKi/VertexCuts.h"
#include "TMath.h"
#include "src/Features/Classifiers/TMVA/IsoBDT_Bs_v1r0.C"

//--------------------------------------------------------------------
// Implementation file for class : TaggingUtils
//
// Author: Marco Musy
//--------------------------------------------------------------------

using namespace LHCb;
using namespace Gaudi::Units;

#include "LoKi/ParticleContextCuts.h"
#include "LoKi/ParticleCuts.h"

using namespace LoKi::Cuts;
using namespace LoKi::Types;
using namespace LoKi::Particles;

using CharmTaggerSpace::CharmMode;

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TaggingUtils )

//====================================================================
TaggingUtils::TaggingUtils( const std::string& type, const std::string& name, const IInterface* parent )
    : GaudiTool( type, name, parent ) {
  declareInterface<ITaggingUtils>( this );
}

TaggingUtils::~TaggingUtils() {}

//=====================================================================
StatusCode TaggingUtils::initialize() {
  StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  m_dva = Gaudi::Utils::getIDVAlgorithm( contextSvc(), this );
  if ( m_dva == nullptr ) {
    fatal() << "Coudn't get parent DVAlgorithm" << endmsg;
    return StatusCode::FAILURE;
  }

  m_PVReFitter = tool<IPVReFitter>( m_algNamePVReFitter, this );
  if ( m_PVReFitter == nullptr ) {
    fatal() << "Unable to retrieve IPVRefitter \'" << m_algNamePVReFitter << "\'" << endmsg;
    return StatusCode::FAILURE;
  }

  m_LifetimeFitter = tool<ILifetimeFitter>( m_algNameLifetimeFitter, this );
  if ( m_LifetimeFitter == nullptr ) {
    fatal() << "Unable to retrieve ILifetimFitter \'" << m_algNameLifetimeFitter << "\'" << endmsg;
    return StatusCode::FAILURE;
  }

  m_VertexFitter = tool<IVertexFit>( m_algNameVertexFitter, this );
  if ( m_VertexFitter == nullptr ) {
    fatal() << "Unable to retrieve IVertexFit \'" << m_algNameVertexFitter << "\'" << endmsg;
    return StatusCode::FAILURE;
  }

  m_DistanceCalculator = tool<IDistanceCalculator>( m_algNameDistanceCalculator, this ); // m_dva->distanceCalculator();
  if ( m_DistanceCalculator == nullptr ) {
    fatal() << "Unable to retrieve the IDistanceCalculator tool " << m_algNameDistanceCalculator << endmsg;
    return StatusCode::FAILURE;
  }

  m_ParticleDescendants = tool<IParticleDescendants>( m_algNameParticleDescendants, this );
  if ( !m_ParticleDescendants ) {
    fatal() << "Unable to retrieve ParticleDescendants tool " << m_algNameParticleDescendants << endmsg;
  }
  m_inNames_SignalTrack = {"track_minIPchi2", "track_pvdis_mu", "tracksvdis_mu",
                           "track_doca_mu",   "track_angle_mu", "track_fc_mu"};

  m_read_BDT_SignalTrack = std::make_unique<ReadBDT_bS>( m_inNames_SignalTrack );
  if ( !m_read_BDT_SignalTrack ) {
    fatal() << "Unable to retrieve ParticleDescendants tool, could not initialize BDT " << m_algNameParticleDescendants
            << endmsg;
    return StatusCode::FAILURE;
  }

  // Particle IDs
  lambda_pid  = LoKi::Particles::pidFromName( "Lambda0" ).abspid();
  pi_pid      = LoKi::Particles::pidFromName( "pi+" ).abspid();
  pi0_pid     = LoKi::Particles::pidFromName( "pi0" ).abspid();
  k_pid       = LoKi::Particles::pidFromName( "K+" ).abspid();
  ks_pid      = 310; // LoKi::Particles::pidFromName("KS0").abspid();
  p_pid       = LoKi::Particles::pidFromName( "p+" ).abspid();
  e_pid       = LoKi::Particles::pidFromName( "e+" ).abspid();
  mu_pid      = LoKi::Particles::pidFromName( "mu+" ).abspid();
  d0_pid      = LoKi::Particles::pidFromName( "D0" ).abspid();
  d_pid       = LoKi::Particles::pidFromName( "D+" ).abspid();
  lambdaC_pid = LoKi::Particles::pidFromName( "Lambda_c+" ).abspid();

  return sc;
}
//==========================================================================
StatusCode TaggingUtils::calcDOCAmin( const Particle* axp, const Particle* p1, const Particle* p2, double& doca,
                                      double& docaerr ) const {
  double           doca1( 0 ), doca2( 0 ), err1( 0 ), err2( 0 );
  const StatusCode sc1 = m_DistanceCalculator->distance( axp, p1, doca1, err1 );
  const StatusCode sc2 = m_DistanceCalculator->distance( axp, p2, doca2, err2 );

  doca = std::min( doca1, doca2 );
  if ( doca == doca1 )
    docaerr = err1;
  else
    docaerr = err2;

  doca    = std::min( doca1, doca2 );
  docaerr = ( doca == doca1 ) ? err1 : err2;

  return StatusCode{sc1 && sc2};
}

//==========================================================================
StatusCode TaggingUtils::calcIP( const Particle* axp, const VertexBase* v, double& ip, double& iperr ) const {
  ip                     = -100.0;
  iperr                  = 0.0;
  int              zsign = 0;
  double           ipC = 0, ipChi2 = 0;
  StatusCode       sc2 = m_DistanceCalculator->distance( axp, v, ipC, ipChi2 );
  Gaudi::XYZVector ipV;
  StatusCode       sc = m_DistanceCalculator->distance( axp, v, ipV );
  // if ( msgLevel(MSG::DEBUG) ) debug()<<"ipS: "<<ipC<<", ipV.R: "<<ipV.R()<<endmsg;
  if ( sc2 && ipChi2 != 0 ) {
    if ( sc ) zsign = ipV.z() > 0 ? 1 : -1;
    ip    = ipC * zsign; // IP with sign
    iperr = ipC / std::sqrt( ipChi2 );
  }
  // if ( msgLevel(MSG::DEBUG) )
  //   debug()<<"IP: "<<ipC<<", "<<ip<<", (sign = "<<zsign<<" )"<<endmsg;
  return sc2;
}

//=========================================================================
StatusCode TaggingUtils::calcIP( const Particle* axp, const RecVertex::ConstVector& PileUpVtx, double& ip,
                                 double& ipe ) const {
  double     ipmin    = std::numeric_limits<double>::max();
  double     ipminerr = 0.0;
  StatusCode sc, lastsc = StatusCode::SUCCESS;

  for ( RecVertex::ConstVector::const_iterator iv = PileUpVtx.begin(); iv != PileUpVtx.end(); ++iv ) {
    double ipx = 0, ipex = 0;
    double ipC = 0, ipChi2 = 0;
    sc = m_DistanceCalculator->distance( axp, *iv, ipC, ipChi2 );
    if ( ipChi2 ) {
      ipx  = ipC;
      ipex = ipC / sqrt( ipChi2 );
    }

    if ( sc ) {
      if ( ipx < ipmin ) {
        ipmin    = ipx;
        ipminerr = ipex;
      }
    } else
      lastsc = sc;
  }
  ip  = ipmin;
  ipe = ipminerr;

  return lastsc;
}

StatusCode TaggingUtils::GetVCHI2NDOF( const Particle* sigp, const Particle* tagp, double& VCHI2NDOF ) const {
  VCHI2NDOF = 0.0;
  LHCb::Vertex Vtx;
  auto         statuscode = m_VertexFitter->fit( Vtx, *sigp, *tagp );
  if ( statuscode ) { VCHI2NDOF = Vtx.chi2() / Vtx.nDoF(); }
  return statuscode;
}

//=========================================================================
int TaggingUtils::countTracks( const LHCb::Particle::ConstVector& vtags ) const noexcept { return vtags.size(); }

//=========================================================================
bool TaggingUtils::qualitySort( const LHCb::Particle* p, const LHCb::Particle* q ) {
  // if true is returned the second input particle is the "better" one
  if ( p->proto() && q->proto() ) {
    if ( p->proto()->track() && q->proto()->track() ) {
      const LHCb::Track& t1 = *p->proto()->track();
      const LHCb::Track& t2 = *q->proto()->track();
      // prefer tracks which have more subdetectors in
      // where TT counts as half a subdetector
      const unsigned nSub1 = ( t1.hasVelo() ? 2 : 0 ) + ( t1.hasTT() ? 1 : 0 ) + ( t1.hasT() ? 2 : 0 );
      const unsigned nSub2 = ( t2.hasVelo() ? 2 : 0 ) + ( t2.hasTT() ? 1 : 0 ) + ( t2.hasT() ? 2 : 0 );
      if ( nSub2 > nSub1 ) return true;
      if ( nSub1 > nSub2 ) return false;
      // if available, prefer lower ghost probability
      const double ghProb1 = t1.ghostProbability();
      const double ghProb2 = t2.ghostProbability();
      if ( -0. <= ghProb1 && ghProb1 <= 1. && -0. <= ghProb2 && ghProb2 <= 1. ) {
        if ( ghProb1 > ghProb2 ) return true;
        if ( ghProb2 > ghProb1 ) return false;
      }
      // prefer longer tracks
      if ( t1.nLHCbIDs() < t2.nLHCbIDs() ) return true;
      if ( t1.nLHCbIDs() > t2.nLHCbIDs() ) return false;
      // for same length tracks, have chi^2/ndf decide
      const double chi1 = t1.chi2() / double( t1.nDoF() );
      const double chi2 = t2.chi2() / double( t2.nDoF() );
      return ( chi1 > chi2 );
    }
  }
  // fall back on a pT comparison (higher is better) as last resort
  return p->pt() < q->pt();
}

//============================================================================
bool TaggingUtils::isInTree( const LHCb::Particle* axp, const LHCb::Particle::ConstVector& sons ) const {

  for ( Particle::ConstVector::const_iterator ip = sons.begin(); ip != sons.end(); ++ip ) {
    using TaggingHelpers::dphi;
    using TaggingHelpers::isSameTrack;
    using TaggingHelpers::SameTrackStatus;
    using TaggingHelpers::toString;

    const SameTrackStatus isSame = isSameTrack( *axp, **ip );
    if ( isSame ) {
      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << " particle is: " << toString( isSame ) << " isInTree part: " << axp->particleID().pid()
                  << " with p=" << axp->p() / Gaudi::Units::GeV << " pt=" << axp->pt() / Gaudi::Units::GeV
                  << " proto_axp,ip=" << axp->proto() << " " << ( *ip )->proto() << endmsg;
      return true;
    }
  }
  return false;
}

//============================================================================
bool TaggingUtils::isInTree( const LHCb::Particle* axp, const LHCb::Particle::ConstVector& sons,
                             double& dist_phi ) const {
  dist_phi = std::numeric_limits<double>::max();
  for ( Particle::ConstVector::const_iterator ip = sons.begin(); ip != sons.end(); ++ip ) {
    using TaggingHelpers::dphi;
    using TaggingHelpers::isSameTrack;
    using TaggingHelpers::SameTrackStatus;
    using TaggingHelpers::toString;
    const double deltaphi = fabs( dphi( axp->momentum().phi(), ( *ip )->momentum().phi() ) );
    if ( dist_phi > deltaphi ) dist_phi = deltaphi;
    const SameTrackStatus isSame = isSameTrack( *axp, **ip );
    if ( isSame ) {
      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << " particle is: " << toString( isSame ) << " isinTree part: " << axp->particleID().pid()
                  << " with p=" << axp->p() / Gaudi::Units::GeV << " pt=" << axp->pt() / Gaudi::Units::GeV
                  << " proto_axp,ip=" << axp->proto() << " " << ( *ip )->proto() << endmsg;
      return true;
    }
  }
  return false;
}

//=============================================================================
LHCb::Particle::ConstVector TaggingUtils::purgeCands( const LHCb::Particle::Range& cands,
                                                      const LHCb::Particle&        BS ) const {
  // remove any charm cand that has descendents in common with the signal B
  LHCb::Particle::ConstVector purgedCands;

  Particle::ConstVector signalDaus = m_ParticleDescendants->descendants( &BS );

  Particle::ConstVector::const_iterator icand;
  for ( icand = cands.begin(); icand != cands.end(); ++icand ) {

    const Particle*       cand            = *icand;
    Particle::ConstVector candDaus        = m_ParticleDescendants->descendants( cand );
    bool                  isUsedForSignal = false;

    Particle::ConstVector::const_iterator icandDau;
    for ( icandDau = candDaus.begin(); icandDau != candDaus.end(); ++icandDau ) {

      const LHCb::ProtoParticle* proto_candDau = ( *icandDau )->proto();

      Particle::ConstVector::const_iterator isignalDau;
      for ( isignalDau = signalDaus.begin(); isignalDau != signalDaus.end(); ++isignalDau ) {
        if ( proto_candDau == ( *isignalDau )->proto() ) isUsedForSignal = true;
        if ( isUsedForSignal ) break;
      }

      if ( isUsedForSignal ) break;
    }

    if ( !isUsedForSignal ) purgedCands.push_back( cand );
  }

  return purgedCands;
}

double TaggingUtils::TPVTAU( const LHCb::Particle* cand, const LHCb::RecVertex* vert ) const {
  double     ct = 0.0, ctErr = 0.0, ctChi2 = 0.0;
  StatusCode sc  = m_LifetimeFitter->fit( *vert, *cand, ct, ctErr, ctChi2 );
  double     tau = ( sc ) ? ct / picosecond : -1.0;
  return tau;
}

double TaggingUtils::TPVDIRA( const LHCb::Particle* cand, const LHCb::RecVertex* vert ) const {
  Fun fDIRA = DIRA( vert );
  return fDIRA( cand );
}

double TaggingUtils::TPVFD( const LHCb::Particle* cand, const LHCb::RecVertex* vert ) const {
  Fun fVD = VD( vert );
  return fVD( cand );
}

double TaggingUtils::TPVFDCHI2( const LHCb::Particle* cand, const LHCb::RecVertex* vert ) const {
  Fun fVDCHI2 = VDCHI2( vert );
  return fVDCHI2( cand );
}

double TaggingUtils::TPVIPCHI2( const LHCb::Particle* cand, const LHCb::RecVertex* vert, const char* id ) const {
  LHCb::VertexBase::ConstVector verts;
  verts.push_back( vert );
  Fun ipPvChi2 = MIPCHI2( verts, this->getDistanceCalculator() );
  if ( id ) {
    Fun mip = MAXTREE( ipPvChi2, id == ABSID, -1 );
    return mip( cand );
  } else {
    return ipPvChi2( cand );
  }
}

bool TaggingUtils::isBestPV( const LHCb::Particle* cand, const RecVertex* vert ) const {
  const RecVertex* bpv  = (const RecVertex*)m_dva->bestVertex( cand );
  VFun             dist = LoKi::Cuts::VVDCHI2( vert );
  return ( dist( bpv ) < 1 );
}

//=============================================================================
CharmMode TaggingUtils::getCharmDecayMode( const LHCb::Particle* cand, int candType ) const {

  CharmMode mode = CharmMode::None;

  const SmartRefVector<Particle>& daus    = cand->daughters();
  int                             numDaus = daus.size();

  switch ( candType ) {

  case 0: // full reco, exclusive

    if ( cand->particleID().abspid() == d0_pid ) {

      switch ( numDaus ) {

      case 2:
        if ( daus[0]->particleID().abspid() == k_pid and daus[1]->particleID().abspid() == pi_pid )
          mode = CharmMode::Dz2kpi;
        break;

      case 3:
        // if (daus[0]->particleID().abspid() == ks_pid and
        //     daus[1]->particleID().abspid() == pi_pid and
        //     daus[2]->particleID().abspid() == pi_pid)
        //   mode = "D0_Kspipi";
        if ( daus[0]->particleID().abspid() == k_pid and daus[1]->particleID().abspid() == pi_pid and
             daus[2]->particleID().abspid() == pi0_pid )
          mode = CharmMode::Dz2kpipiz;
        break;

      case 4:
        if ( daus[0]->particleID().abspid() == k_pid and daus[1]->particleID().abspid() == pi_pid and
             daus[2]->particleID().abspid() == pi_pid and daus[3]->particleID().abspid() == pi_pid )
          mode = CharmMode::Dz2kpipipi;
        break;

      default:
        fatal() << "Invalid daus size: " << numDaus << " for candtype: " << candType << endmsg;
      }

    } else if ( cand->particleID().abspid() == d_pid ) {

      switch ( numDaus ) {

        // case 2:
        //   if (daus[0]->particleID().abspid() == ks_pid and
        //       daus[1]->particleID().abspid() == pi_pid)
        //     mode = "Dp_Kspi";
        //   break;

      case 3:
        if ( daus[0]->particleID().abspid() == k_pid and daus[1]->particleID().abspid() == pi_pid and
             daus[2]->particleID().abspid() == pi_pid )
          mode = CharmMode::Dp2kpipi;
        break;

      default:
        fatal() << "Invalid daus size: " << numDaus << " for candtype: " << candType << endmsg;
      }

    } else {
      fatal() << "Invalid charm type: " << cand->particleID().abspid() << " for candtype: " << candType << endmsg;
    }

    break;

  case 1: // part reco, inclusive

    if ( cand->particleID().abspid() == d0_pid ) {

      switch ( numDaus ) {

      case 2:
        if ( daus[0]->particleID().abspid() == k_pid and daus[1]->particleID().abspid() == pi_pid )
          mode = CharmMode::Dz2kpiX;
        if ( daus[0]->particleID().abspid() == k_pid and daus[1]->particleID().abspid() == e_pid )
          mode = CharmMode::Dz2keX;
        if ( daus[0]->particleID().abspid() == k_pid and daus[1]->particleID().abspid() == mu_pid )
          mode = CharmMode::Dz2kmuX;
        break;

      default:
        fatal() << "Invalid daus size: " << numDaus << " for candtype: " << candType << endmsg;
      }

    } else if ( cand->particleID().abspid() == d_pid ) {

      switch ( numDaus ) {

        // case 2:
        //   if (daus[0]->particleID().abspid() == k_pid and
        //       daus[1]->particleID().abspid() == pi_pid)
        //     mode = "Dp_KpiX";
        //   if (daus[0]->particleID().abspid() == k_pid and
        //       daus[1]->particleID().abspid() == e_pid)
        //     mode = "Dp_KeX";
        //   if (daus[0]->particleID().abspid() == k_pid and
        //       daus[1]->particleID().abspid() == mu_pid)
        //     mode = "Dp_KmuX";
        //   break;

      default:
        fatal() << "Invalid daus size: " << numDaus << " for candtype: " << candType << endmsg;
      }

    } else {
      fatal() << "Invalid charm type: " << cand->particleID().abspid() << " for candtype: " << candType << endmsg;
    }

    break;

  case 2: // dstar reco

    switch ( numDaus ) {

      // case 3:
      //   if (daus[0]->particleID().abspid() == ks_pid and
      //       daus[1]->particleID().abspid() == pi_pid and
      //       daus[2]->particleID().abspid() == pi_pid)
      //     mode = "Dstar_D0_Kspipi";
      //   break;

    default:
      fatal() << "Invalid daus size: " << numDaus << " candtype: " << candType << endmsg;
    }
    break;

  case 3: // lambda reco

    if ( cand->particleID().abspid() == lambdaC_pid ) {

      switch ( numDaus ) {

        // case 2:
        //   if (daus[0]->particleID().abspid() == p_pid and
        //       daus[1]->particleID().abspid() == ks_pid)
        //     mode = "LambdaC_pKs";
        //   if (daus[0]->particleID().abspid() == lambda_pid and
        //       daus[1]->particleID().abspid() == pi_pid)
        //     mode = "LambdaC_LambdaPi";
        //   break;

      case 3:
        if ( daus[0]->particleID().abspid() == p_pid and daus[1]->particleID().abspid() == k_pid and
             daus[2]->particleID().abspid() == pi_pid )
          mode = CharmMode::LambdaC2pkpi;
        break;

      default:
        fatal() << "Invalid daus size: " << numDaus << " for candtype: " << candType << endmsg;
      }

    } else {
      fatal() << "Invalid charm type: " << cand->particleID().abspid() << " for candtype: " << candType << endmsg;
    }
    break;

  default:
    fatal() << "Invalid candtype: " << candType << endmsg;
  }

  return mode;
}

double TaggingUtils::GetSumBDT_ult( const Particle* tagp, const Particle* sigp, Gaudi::XYZPoint PosPV,
                                    Gaudi::XYZPoint PosSV ) const {
  auto sons = m_ParticleDescendants->descendants( sigp );
  sons.push_back( sigp );
  double                             SumBDT_ult_all = 0.0;
  double                             SumBDT_ult     = 0.0;
  LHCb::Particle::Range              allparts;
  std::vector<const LHCb::Particle*> ultparts;
  if ( exist<LHCb::Particle::Range>( m_isolationVariableAllParticlesLocation ) ) {
    allparts = get<LHCb::Particle::Range>( m_isolationVariableAllParticlesLocation );
  } else {
    return -9999;
  }
  LoKi::Types::Cut cuts = LoKi::Particles::IsAParticleInTree( tagp ) || LoKi::Particles::HasProtosInTree( tagp ) ||
                          LoKi::Particles::HasTracksInTree( tagp );
  if ( sigp ) {
    cuts = cuts || LoKi::Particles::HasTracksInTree( sigp ) || LoKi::Particles::IsAParticleInTree( sigp ) ||
           LoKi::Particles::HasProtosInTree( sigp );
  }
  cuts = !cuts;
  //
  std::copy_if( allparts.begin(), allparts.end(), std::back_inserter( ultparts ), std::cref( cuts ) );
  if ( msgLevel( MSG::VERBOSE ) ) {
    verbose() << " Read " << std::size( ultparts ) << " particles from StdAllNoPIDsPions" << endmsg;
  }
  const LHCb::ProtoParticle* proto = tagp->proto();
  if ( !proto ) return -9998;
  const LHCb::Track* track = proto->track();
  if ( !track ) return -9997;
  if ( isInTree( tagp, sons ) ) return -9996;
  for ( const auto& im_ult : ultparts ) {
    const LHCb::Particle*      axp_ult   = im_ult;
    const LHCb::ProtoParticle* proto_ult = axp_ult->proto();
    if ( !proto_ult ) continue;
    const LHCb::Track* track_ult = proto_ult->track();
    if ( !track_ult ) continue;
    if ( isInTree( im_ult, sons ) ) continue; // make sure the particle in habd is not in the signal
    if ( axp_ult == tagp ) continue;          // remove the particle in hand form ult container
    SumBDT_ult = GetIsoBDT( tagp, axp_ult, PosSV, PosPV );
    SumBDT_ult_all += SumBDT_ult;
  }
  if ( msgLevel( MSG::VERBOSE ) ) { verbose() << " BDT sum for tagging particle is:" << SumBDT_ult_all << endmsg; }
  return SumBDT_ult_all;
}

double TaggingUtils::GetIsoBDT( const LHCb::Particle* axp_one, const LHCb::Particle* axp_two,
                                const Gaudi::XYZPoint& PosPV, const Gaudi::XYZPoint& PosSV ) const {
  double             SumBDT          = -100;
  const LHCb::Track* track           = axp_one->proto()->track();
  double             track_minIPchi2 = get_MINIPCHI2( axp_one );
  Gaudi::XYZPoint    pos_track( track->position() );
  Gaudi::XYZVector   mom_track( track->momentum() );
  Gaudi::XYZPoint    pos_SignalTrack( axp_two->proto()->track()->position() );
  Gaudi::XYZVector   mom_SignalTrack( axp_two->proto()->track()->momentum() );
  // Calculate the input of ISO variable :
  Gaudi::XYZPoint vtx_SignalTrack( 0., 0., 0. );
  double          doca_SignalTrack( -1. ), angle_SignalTrack( -1. );
  closest_point_with_doca_and_angle( pos_SignalTrack, mom_SignalTrack, pos_track, mom_track, vtx_SignalTrack,
                                     doca_SignalTrack, angle_SignalTrack );
  double PVdis_SignalTrack_track =
      ( vtx_SignalTrack.z() - PosPV.z() ) / fabs( vtx_SignalTrack.z() - PosPV.z() ) * ( vtx_SignalTrack - PosPV ).R();
  double SVdis_SignalTrack_track =
      ( vtx_SignalTrack.z() - PosSV.z() ) / fabs( vtx_SignalTrack.z() - PosSV.z() ) * ( vtx_SignalTrack - PosSV ).R();
  double fc_mu = isolation_fc( vtx_SignalTrack - PosPV, mom_track, mom_SignalTrack );
  SumBDT       = m_read_BDT_SignalTrack->GetMvaValue( {
      track_minIPchi2,
      PVdis_SignalTrack_track,
      SVdis_SignalTrack_track,
      doca_SignalTrack,
      angle_SignalTrack,
      fc_mu,
  } );
  if ( msgLevel( MSG::VERBOSE ) ) { verbose() << " Add " << SumBDT << " to BDT sum" << endmsg; }
  return SumBDT;
}

void TaggingUtils::closest_point_with_doca_and_angle( const Gaudi::XYZPoint& o1, const Gaudi::XYZVector& p1,
                                                      const Gaudi::XYZPoint& o2, const Gaudi::XYZVector& p2,
                                                      Gaudi::XYZPoint& vtx, double& doca, double& angle ) const {
  // Gaudi::XYZPoint rv;
  Gaudi::XYZPoint close;
  Gaudi::XYZPoint close_mu;
  bool            fail = false;
  vtx                  = closest_point( o1, p1, o2, p2, close, close_mu, fail );
  if ( fail ) {
    doca  = -1.;
    angle = -1.;
  } else {
    doca  = ( close - close_mu ).R();
    angle = enclosed_angle( p1, p2 );
  }
}

Gaudi::XYZPoint TaggingUtils::closest_point( const Gaudi::XYZPoint& o, const Gaudi::XYZVector& p,
                                             const Gaudi::XYZPoint& o_mu, const Gaudi::XYZVector& p_mu,
                                             Gaudi::XYZPoint& close1, Gaudi::XYZPoint& close2, bool& fail ) const {
  Gaudi::XYZVector v0( o - o_mu );
  Gaudi::XYZVector v1( p.unit() );
  Gaudi::XYZVector v2( p_mu.unit() );
  fail = false;

  double d02 = v0.Dot( v2 ), d21 = v2.Dot( v1 ), d01 = v0.Dot( v1 ), d22 = v2.Dot( v2 ), d11 = v1.Dot( v1 ),
         denom = d11 * d22 - d21 * d21;
  if ( fabs( denom ) == 0. ) {
    close1 = Gaudi::XYZPoint( 0., 0., 0. );
    close2 = Gaudi::XYZPoint( 0., 0., 0. );
    fail   = true;
  } else {
    double numer = d02 * d21 - d01 * d22;
    double mu1   = numer / denom;
    double mu2   = ( d02 + d21 * mu1 ) / d22;
    close1       = o + v1 * mu1;
    close2       = o_mu + v2 * mu2;
  }
  return close1 + ( close2 - close1 ) * 0.5;
}

double TaggingUtils::get_MINIPCHI2( const LHCb::Particle* p ) const {
  double                 minchi2 = -1;
  const RecVertex::Range PV      = m_dva->primaryVertices();
  for ( const auto& pv : PV ) {
    double     ip, chi2;
    RecVertex  newPV( *pv );
    StatusCode sc = m_PVReFitter->remove( p, &newPV );
    if ( sc.isFailure() ) {
      Warning( "ReFitter fails!" ).ignore();
      continue;
    }
    auto* newPVPtr = static_cast<LHCb::VertexBase*>( &newPV );
    sc             = m_DistanceCalculator->distance( p, newPVPtr, ip, chi2 );
    if ( sc.isFailure() ) {
      Warning( "DistanceCalculator fails!" ).ignore();
      continue;
    }
    if ( chi2 < minchi2 || minchi2 < 0.0 ) { minchi2 = chi2; }
  }
  return minchi2;
}

double TaggingUtils::isolation_fc( const Gaudi::XYZVector& vertex, const Gaudi::XYZVector& p,
                                   const Gaudi::XYZVector& p_mu ) const {
  // See LHCb-INT-2010-011
  double           pt = p.Rho() + p_mu.Rho();
  Gaudi::XYZVector ptot( p + p_mu );
  double           num = ptot.R() * sin( enclosed_angle( vertex, ptot ) );
  return num / ( num + pt );
}

double TaggingUtils::enclosed_angle( const Gaudi::XYZVector& p1, const Gaudi::XYZVector& p2 ) const {
  return acos( p1.Dot( p2 ) / ( p1.R() * p2.R() ) );
}
