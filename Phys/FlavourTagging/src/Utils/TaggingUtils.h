/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PHYS_PHYS_FLAVOURTAGGING_TAGGINGUTILS_H
#define PHYS_PHYS_FLAVOURTAGGING_TAGGINGUTILS_H 1

// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IDVAlgorithm.h"
#include "Kernel/IDistanceCalculator.h"
#include "Kernel/IPVReFitter.h"
// from Event

#include "FlavourTagging/ITaggingUtils.h"

#include "Kernel/IParticleDescendants.h"

#include "LoKi/Operators.h"
#include "LoKi/Particles11.h"
#include "LoKi/Particles8.h"
#include "LoKi/Particles9.h"
#include "LoKi/PhysTypes.h"

/** @class TaggingUtils TaggingUtils.h
 *
 *  Tool to tag the B flavour with a Electron Tagger
 *
 *  @author Marco Musy
 *  @date   30/06/2005
 */

class TaggingUtils : public GaudiTool, virtual public ITaggingUtils {

public:
  /// Standard constructor
  TaggingUtils( const std::string& type, const std::string& name, const IInterface* parent );

  virtual ~TaggingUtils();
  StatusCode initialize() override;

  //----------------------------------------------------------------------------
  // acessors to fitters
  const IPVReFitter*         getPVReFitter() const override { return m_PVReFitter; }
  const ILifetimeFitter*     getLifetimeFitter() const override { return m_LifetimeFitter; }
  const IVertexFit*          getVertexFitter() const override { return m_VertexFitter; }
  const IDistanceCalculator* getDistanceCalculator() const override { return m_DistanceCalculator; }
  IParticleDescendants*      getParticleDescendants() const override { return m_ParticleDescendants; }

  //-------------------------------------------------------------
  StatusCode calcIP( const LHCb::Particle* axp, const LHCb::VertexBase* v, double& ip, double& iperr ) const override;

  StatusCode calcIP( const LHCb::Particle*, const LHCb::RecVertex::ConstVector&, double&, double& ) const override;

  StatusCode GetVCHI2NDOF( const LHCb::Particle* sigp, const LHCb::Particle* tagp, double& VCHI2NDOF ) const override;

  StatusCode calcDOCAmin( const LHCb::Particle* axp, const LHCb::Particle* p1, const LHCb::Particle* p2, double& doca,
                          double& docaerr ) const override;

  int countTracks( const LHCb::Particle::ConstVector& ) const noexcept override;

  static bool qualitySort( const LHCb::Particle* p, const LHCb::Particle* q );

  bool isInTree( const LHCb::Particle*              B0Candidate,
                 const LHCb::Particle::ConstVector& daughterCandidates ) const override;

  bool isInTree( const LHCb::Particle* B0Candidate, const LHCb::Particle::ConstVector& daughterCandidates,
                 double& distPhi ) const override;

  double TPVTAU( const LHCb::Particle*, const LHCb::RecVertex* ) const override;
  double TPVDIRA( const LHCb::Particle* cand, const LHCb::RecVertex* vert ) const override;
  double TPVFD( const LHCb::Particle* cand, const LHCb::RecVertex* vert ) const override;
  double TPVFDCHI2( const LHCb::Particle* cand, const LHCb::RecVertex* vert ) const override;
  double TPVIPCHI2( const LHCb::Particle* cand, const LHCb::RecVertex* vert, const char* id = nullptr ) const override;
  bool   isBestPV( const LHCb::Particle* cand, const LHCb::RecVertex* vert ) const override;

  double GetSumBDT_ult( const LHCb::Particle* tagp, const LHCb::Particle* sigp, Gaudi::XYZPoint PosPV,
                        Gaudi::XYZPoint PosSV ) const override;

  double GetIsoBDT( const LHCb::Particle* axp_one, const LHCb::Particle* axp_two, const Gaudi::XYZPoint& PosPV,
                    const Gaudi::XYZPoint& PosSV ) const override;

  void closest_point_with_doca_and_angle( const Gaudi::XYZPoint& o1, const Gaudi::XYZVector& p1,
                                          const Gaudi::XYZPoint& o2, const Gaudi::XYZVector& p2, Gaudi::XYZPoint& vtx,
                                          double& doca, double& angle ) const override;

  Gaudi::XYZPoint closest_point( const Gaudi::XYZPoint& o, const Gaudi::XYZVector& p, const Gaudi::XYZPoint& o_mu,
                                 const Gaudi::XYZVector& p_mu, Gaudi::XYZPoint& close1, Gaudi::XYZPoint& close2,
                                 bool& fail ) const override;

  double get_MINIPCHI2( const LHCb::Particle* p ) const override;

  /* \brief Isolation "fc" variable as described in LHCb-INT-2010-011
   */
  double isolation_fc( const Gaudi::XYZVector& vertex, const Gaudi::XYZVector& p,
                       const Gaudi::XYZVector& p_mu ) const override;

  // Get enclosed angle between two vectors
  double enclosed_angle( const Gaudi::XYZVector& p1, const Gaudi::XYZVector& p2 ) const override;

  // classify charm decay modes
  CharmTaggerSpace::CharmMode getCharmDecayMode( const LHCb::Particle*, int ) const override;

  // remove any charm cand that has descendents in common with the signal B
  LHCb::Particle::ConstVector purgeCands( const LHCb::Particle::Range& cands, const LHCb::Particle& BS ) const override;

  //-------------------------------------------------------------

private:
  IDVAlgorithm* m_dva = nullptr;

  Gaudi::Property<std::string> m_PVSelCriterion{this, "PVSelCriterion", "PVbyIPs", ""};
  Gaudi::Property<std::string> m_algNamePVReFitter{this, "PVReFitter", "LoKi::PVReFitter:PUBLIC", ""};
  Gaudi::Property<std::string> m_algNameLifetimeFitter{this, "LifetimeFitter", "LoKi::LifetimeFitter:PUBLIC", ""};
  Gaudi::Property<std::string> m_algNameVertexFitter{this, "VertexFitter", "LoKi::VertexFitter:PUBLIC", ""};
  Gaudi::Property<std::string> m_algNameDistanceCalculator{this, "DistanceCalculator",
                                                           "LoKi::DistanceCalculator:PUBLIC", ""};
  Gaudi::Property<std::string> m_algNameParticleDescendants{this, "ParticleDescendants", "ParticleDescendants", ""};
  Gaudi::Property<std::string> m_isolationVariableAllParticlesLocation{
      this, "IsolationAllParticlesLocation", "/Event/Phys/TaggingIFTTracks/Particles",
      "All \"other\" particles will be received from this location to compute "
      "the isolation variable SumBDT_ult."};

  std::vector<std::string>    m_inNames_SignalTrack;
  std::unique_ptr<ReadBDT_bS> m_read_BDT_SignalTrack;

  const IPVReFitter*         m_PVReFitter          = nullptr;
  const ILifetimeFitter*     m_LifetimeFitter      = nullptr;
  const IVertexFit*          m_VertexFitter        = nullptr;
  const IDistanceCalculator* m_DistanceCalculator  = nullptr;
  IParticleDescendants*      m_ParticleDescendants = nullptr; // cannot be const, as
                                                              // descendants() is not a const
                                                              // function.

  unsigned int lambda_pid, pi_pid, pi0_pid, k_pid, ks_pid, p_pid, e_pid, mu_pid, d0_pid, d_pid, lambdaC_pid;
};

//===============================================================//
#endif // PHYS_PHYS_FLAVOURTAGGING_TAGGINGUTILS_H
