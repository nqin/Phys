/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Kernel/DaVinciAlgorithm.h"

/** @class BTagging BTagging.h
 *
 *  Algorithm to tag the B flavour
 *
 *  @author Marco Musy
 *  @date   02/10/2006
 */

class BTagging : public DaVinciAlgorithm {

public:
  /// Standard constructor
  using DaVinciAlgorithm::DaVinciAlgorithm;

  StatusCode execute() override; ///< Algorithm execution

private:
  /// Run the tagging on the given location
  void performTagging( const std::string& location );

private:
  Gaudi::Property<std::string> m_tagLocation{this, "TagOutputLocation", "FlavourTags",
                                             "Output location of the FlavourTag objects"};
};
