/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef COMBINETAGGERSPID_H
#define COMBINETAGGERSPID_H 1
// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/AlgTool.h"

#include "ICombineTaggersTool.h" // Interface

/** @class CombineTaggersPID CombineTaggersPID.h CombineTaggersPID.h
 *
 *  Tool to do the combined tagging (OS and SS)
 *  @author Marc Grabalosa
 *  @date   2008-10-31
 */
class CombineTaggersPID : public GaudiTool, virtual public ICombineTaggersTool {
public:
  /// Standard constructor
  CombineTaggersPID( const std::string& type, const std::string& name, const IInterface* parent );

  ~CombineTaggersPID(); ///< Destructor

  /// Initialize
  StatusCode initialize() override;

  int combineTaggers( LHCb::FlavourTag& theTag, std::vector<LHCb::Tagger*>&, int, bool, bool ) override;

private:
  std::vector<int> m_pid_cats_bs;
  std::vector<int> m_pid_cats_bu;
  std::vector<int> m_index;
};
#endif // COMBINETAGGERSPID_H
