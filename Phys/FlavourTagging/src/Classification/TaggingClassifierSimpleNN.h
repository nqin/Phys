/*****************************************************************************\
* (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PHYS_PHYS_FLAVOURTAGGING_TAGGINGCLASSIFIERSIMPLENN_H
#define PHYS_PHYS_FLAVOURTAGGING_TAGGINGCLASSIFIERSIMPLENN_H 1

#include <string>
#include <vector>

#include "ITaggingClassifier.h"

// Empty base class for all simpleNN classifiers.
// Factories can produce specialized derived classifier types

class TaggingClassifierSimpleNN : public ITaggingClassifier {
public:
  TaggingClassifierSimpleNN() = default;

  double getClassifierValue( const std::vector<double>& ) override {
    // error() << "TaggingClassifierSimpleNN::getClassifierValue is unused.Tagger output will be -9999" << endmsg;
    return -9999.0; /* Unused */
  };
};

#endif // PHYS_PHYS_FLAVOURTAGGING_TAGGINGCLASSIFIER_SIMPLENN_H
