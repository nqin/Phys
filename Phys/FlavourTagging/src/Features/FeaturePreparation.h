/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <cmath>
#include <functional>
#include <map>
#include <string>
#include <unordered_map>
#include <vector>

class FeaturePreparation {
public:
  /** Setup the functions to prepare given feature vectors
   */
  void setFunctions( const std::vector<std::string>& transformations );
  void setFunctions( const std::vector<std::string>&           featureNames,
                     const std::map<std::string, std::string>& functionMap );

  /** Calls all previously defined functions on corresponding features
   */
  std::vector<double> prepareFeatures( std::vector<double> features );

private:
  static const std::unordered_map<std::string, std::function<double( double )>> m_functions;
  std::vector<std::function<double( double )>>                                  m_activeFunctions = {};
};
