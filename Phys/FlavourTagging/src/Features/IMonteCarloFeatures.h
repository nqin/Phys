/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/MCParticle.h"

#include <vector>

static const InterfaceID IID_IMonteCarloFeatures( "IMonteCarloFeatures", 1, 0 );

class IMonteCarloFeatures : virtual public IAlgTool {
public:
  static const InterfaceID&                    interfaceID() { return IID_IMonteCarloFeatures; }
  virtual std::vector<const LHCb::MCParticle*> getMCParticles() = 0;
  virtual const LHCb::MCParticle*              signalParticle() = 0;
  virtual void                                 readParticles()  = 0;
};
