/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/MCParticle.h"
#include "GaudiAlg/GaudiTool.h"
#include "MCInterfaces/IMCDecayFinder.h"

#include "IMonteCarloFeatures.h"

class MonteCarloFeatures : public GaudiTool, virtual public IMonteCarloFeatures {
public:
  MonteCarloFeatures( const std::string& type, const std::string& name, const IInterface* interface );

  virtual StatusCode initialize() override;

  virtual std::vector<const LHCb::MCParticle*> getMCParticles() override;

  virtual const LHCb::MCParticle* signalParticle() override;

  /* Read mc particles from tes
   * @TODO: maybe do this "globally" within the TaggingTool
   */
  virtual void readParticles() override;

private:
  const LHCb::MCParticle*              m_mcSignal    = nullptr;
  std::vector<const LHCb::MCParticle*> m_mcParticles = {};

  IMCDecayFinder* m_mcDecayFinder = nullptr;
};
