/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TESLATOOLS_TESLAMATCHER_H
#define TESLATOOLS_TESLAMATCHER_H 1

//------------------------------------------------------------------------------
// TeslaTools
//------------------------------------------------------------------------------
#include "TeslaTools/ITeslaMatcher.h"
//------------------------------------------------------------------------------
// STL
//------------------------------------------------------------------------------
#include <string>
#include <vector>
//------------------------------------------------------------------------------
// DaVinci
//------------------------------------------------------------------------------
#include <Event/Particle.h>
//------------------------------------------------------------------------------
// Gaudi
//------------------------------------------------------------------------------
#include "GaudiAlg/GaudiTool.h"

#define MATCHING_FUNC( name, original, cand, defaultCand )                                                             \
  const Particle* name( const Particle* original, const Particle* cand, const Particle* defaultCand )

class TeslaMatcher : public GaudiTool, virtual public ITeslaMatcher {
public:
  TeslaMatcher( const std::string& type, const std::string& name, const IInterface* parent );

  ~TeslaMatcher();

  StatusCode initialize() override;
  StatusCode finalize() override;

  StatusCode findBestMatch( const Particle* inputParticle, const Particle*& outputParticle,
                            const std::string& tesLocation ) const override;

protected:
  static MATCHING_FUNC( defaultBestMatching, original, cand, defaultCand );
  static MATCHING_FUNC( t2bBestMatching, original, cand, defaultCand );
  static MATCHING_FUNC( trackBestMatching, original, cand, defaultCand );
  static MATCHING_FUNC( momentumBestMatching, original, cand, defaultCand );

private:
  std::function<const Particle*( const Particle*, const Particle*, const Particle* )> m_bestMatching;

  bool        m_checkPid;
  std::string m_matchingTechnique;
};

#undef MATCHING_FUNC

#endif // TESLATOOLS_TESLAMATCHER_H
