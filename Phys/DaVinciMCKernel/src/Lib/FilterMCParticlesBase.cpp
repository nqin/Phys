/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id$
// Include files

// local
#include "Kernel/FilterMCParticlesBase.h"

//-----------------------------------------------------------------------------
// Implementation file for class : FilterMCParticlesBase
//
// 2007-07-20 : Juan Palacios
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
FilterMCParticlesBase::FilterMCParticlesBase( const std::string& type, const std::string& name,
                                              const IInterface* parent )
    : GaudiTool( type, name, parent ) {
  declareInterface<IFilterMCParticles>( this );
}
//=============================================================================
bool FilterMCParticlesBase::operator()( const LHCb::MCParticle::ConstVector& parts ) const {
  return isSatisfied( parts );
}
//=============================================================================
bool FilterMCParticlesBase::isSatisfied( const LHCb::MCParticle::ConstVector& /*parts*/ ) const { return true; }
//=============================================================================
// Destructor
//=============================================================================
FilterMCParticlesBase::~FilterMCParticlesBase() {}

//=============================================================================
