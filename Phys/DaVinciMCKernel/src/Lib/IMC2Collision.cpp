/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/IInterface.h"
// ============================================================================
// Kernel ?
// ============================================================================
#include "Kernel/IMC2Collision.h"
// ============================================================================
/** @file
 *
 *  Implementation file for class IMC2Collision
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-03-18
 */
// ============================================================================
namespace {
  // ==========================================================================
  /** @var IID_IMC2Collision
   *  unique static identifier of IMC2Collision interface
   *  @see IMC2Collision
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date 2006-03-18
   */
  // ==========================================================================
  const InterfaceID IID_IMC2Collision( "IMC2Collision", 1, 0 );
  // ==========================================================================
} // namespace
// ============================================================================
// Return the unique interface identifier
// ============================================================================
const InterfaceID& IMC2Collision::interfaceID() { return IID_IMC2Collision; }
// ============================================================================
// destructor
// ============================================================================
IMC2Collision::~IMC2Collision() {}
// ============================================================================
// The END
// ============================================================================
