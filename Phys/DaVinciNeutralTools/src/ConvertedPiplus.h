/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id$
#ifndef CONVERTEDPIPLUS_H
#define CONVERTEDPIPLUS_H 1

// Include files
// from DaVinci, this is a specialized GaudiAlgorithm
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/RndmGenerators.h"
#include "Kernel/DaVinciAlgorithm.h"
#include "Kernel/IChangePIDTool.h"

/** @class ConvertedPiplus ConvertedPiplus.h
 *
 * Simple algorithm that takes input container of pions and uses each pion
 * to define the 'parent' four vector in an isotropic decay to two photons.
 * One of the photons is saved, at random, into a container
 * ready to be picked up later by a combiner algorithm. Useful to obtain proxy
 * for combinatorial background in pi0 -> e+ e- gamma decays, where the
 * fake-decayed photon has the correct kinematics to combine with e+e- pair
 * from a real Dalitz decay
 *
 * This algorithm should only be applied to charged pions!
 *
 *  @author Daniel Johnson
 *  @date   2023-02-21
 */
class ConvertedPiplus : public DaVinciAlgorithm {

public:
  /// Standard constructor
  ConvertedPiplus( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~ConvertedPiplus(); ///< Destructor

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

protected:
  IChangePIDTool* m_changePIDTool; ///< Tool that performs most of the work

private:
  StatusCode loopOnPions( const LHCb::Particle::Range& pions );
  int        m_nEvents;     ///< Number of events
  int        m_nAccepted;   ///< Number of events accepted
  int        m_nCandidates; ///< Number of candidates
};
#endif // CONVERTEDPIPLUS_H
