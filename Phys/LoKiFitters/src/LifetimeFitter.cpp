/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/PhysicalConstants.h"
// ============================================================================
// DaVinciKernel
// ============================================================================
#include "Kernel/ILifetimeFitter.h"
// ============================================================================
// local
// ============================================================================
#include "DirectionFitBase.h"
// ============================================================================
/** @file
 *
 *  This file is a part of
 *  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
 *  ``C++ ToolKit for Smart and Friendly Physics Analysis''
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date 2008-02-12
 */
// ============================================================================
namespace LoKi {
  // ==========================================================================
  /** @class LifeTimeFitter
   *  The simple implementation of following major  abstract interfaces:
   *
   *    - ILifetimeFitter
   *
   *  The implementation follows the note by Paul AVERY
   *    "Directly Determining Lifetime Using a 3-D Fit"
   *
   *  The actual algorithm si described in detail for
   *  the base class LoKi::DirectionFitBase
   *
   *  @see LoKi::DirectionFitBase
   *  @see ILifetimeFitter
   *
   *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
   *  @date 2008-02-12
   */
  class LifetimeFitter : public extends<LoKi::DirectionFitBase, ILifetimeFitter> {
  public:
    // ========================================================================
    /** Evaluate the particle  lifetime
     *
     *  @code
     *
     *  // get the tool:
     *  ILifetimeFitter* fitter = ... ;
     *
     *  // get B-candidate:
     *  const LHCb::Particle* myB = ... ;
     *
     *  // get the corresponidng primary vertex:
     *  const LHCb::VertexBase* primary = ... ;
     *
     *  // use the tool:
     *  double lifetime = -1*e+10 * Gaudi::Units::ns ;
     *  double error    = -1 ;
     *  double chi2     = -1 ;
     *  StatusCode sc = fitter -> fit ( primary , myB , lifetime , error , chi2 ) ;
     *  if ( sc.isFailure() ) { ... error here ... }
     *
     *  @endcode
     *
     *  @see ILifetimeFitter
     *
     *  @param primary  the production vertex   (input)
     *  @param particle the particle            (input)
     *  @param lifetime the proper lifetime     (output)
     *  @param error error estgimate for the proper lifetime  (output)
     *  @param chi2 chi2 of the fit            (output)
     */
    StatusCode fit( const LHCb::VertexBase& primary, const LHCb::Particle& particle, double& lifetime, double& error,
                    double& chi2 ) const override;
    // ========================================================================
  public:
    // ========================================================================
    /// the standard initialization of the tool
    StatusCode initialize() override { return LoKi::DirectionFitBase::initialize(); }
    // ========================================================================
    using base_class::base_class;
    // ========================================================================
  };
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
// Evaluate the particle  lifetime
// ============================================================================
StatusCode LoKi::LifetimeFitter::fit( const LHCb::VertexBase& primary, const LHCb::Particle& particle, double& lifetime,
                                      double& error, double& chi2 ) const {
  const LHCb::VertexBase* decay = particle.endVertex();
  if ( NULL == decay ) {
    lifetime = -1.e+10 * Gaudi::Units::nanosecond;
    error    = -1.e+10 * Gaudi::Units::nanosecond;
    chi2     = -1.e+10;
    return _Error( "No valid end-vertex is found", StatusCode{NoEndVertex} );
  }

  // make the actual iterations
  const StatusCode sc = fitConst_( &primary, &particle, decay, lifetime, error, chi2 );
  if ( sc.isFailure() ) { return _Warning( "Error from LoKi::DirectionFitBase", sc, 0 ); }

  // convert c*tau into time
  lifetime /= Gaudi::Units::c_light;
  error /= Gaudi::Units::c_light;

  return StatusCode::SUCCESS;
}
// ============================================================================
/// Declaration of the Tool Factory
DECLARE_COMPONENT( LoKi::LifetimeFitter )
// ============================================================================
// The END
// ============================================================================
