/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: DaVinciKernelDict.h,v 1.20 2010-06-22 07:18:11 jpalac Exp $
// ============================================================================
#ifndef DICT_DAVINCIKERNELDICT_H
#define DICT_DAVINCIKERNELDICT_H 1
// ============================================================================
// Include files
// ============================================================================
/** @file DaVinciKernelDict
 *
 *
 *  @author Juan PALACIOS
 *  @date   2006-10-02
 */
// ============================================================================
// DaVinciKernel
// ============================================================================
#include "Kernel/DaVinciAlgorithm.h"
#include "Kernel/DaVinciHistoAlgorithm.h"
#include "Kernel/DaVinciTupleAlgorithm.h"
#include "Kernel/GetTESLocations.h"
#include "Kernel/ParticleFilters.h"
#include "Kernel/TransporterFunctions.h"
// ============================================================================
// The END
// ============================================================================
#endif // DICT_DAVINCIKERNELDICT_H
