/*****************************************************************************\
* (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_PARTICLES49_H
#  define LOKI_PARTICLES49_H 1
// ============================================================================
// Include files
// ============================================================================
// LoKiPhys
// ============================================================================
#  include "LoKi/PhysTypes.h"
// ============================================================================
#  include "DetDesc/DetectorElement.h"
#  include "Event/STCluster.h"
#  include "Event/State.h"
#  include "Event/VeloCluster.h"
#  include "TrackInterfaces/ITrackStateProvider.h"
#  include "TrackInterfaces/ITrackT0Tool.h"
// ============================================================================
// GaudiKernel
// ============================================================================
#  include "GaudiKernel/Kernel.h"
// ============================================================================
// LoKiCore
// ============================================================================
#  include "LoKi/Interface.h"
// ============================================================================
namespace LHCb {
  class State;
}
// ============================================================================
/** @file
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-02-23
 */
// ============================================================================
class DeSTDetector;
namespace LoKi {
  // ==========================================================================
  namespace Particles {
    // ========================================================================
    /** @class TrackNumVeloClusters
     *  The function which obtains the number of velo clusters belonging to the
     *  track
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *
     *  @author Hendrik JAGE hendrik.jage@cern.ch
     *  @date 2021-03-16
     */
    class GAUDI_API TrackNumVeloClusters final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// Default Constructor
      TrackNumVeloClusters() : AuxFunBase{std::tie()} {}
      /// MANDATORY: clone method ("vitual constructor")
      TrackNumVeloClusters* clone() const override { return new TrackNumVeloClusters( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
    };
    // ========================================================================
    /** @class TrackNumVeloRClusters
     *  The function which obtains the number of velo R clusters belonging to
     *  the track
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *
     *  @author Hendrik JAGE hendrik.jage@cern.ch
     *  @date 2023-06-13
     */
    class GAUDI_API TrackNumVeloRClusters final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// Default Constructor
      TrackNumVeloRClusters() : AuxFunBase{std::tie()} {}
      /// MANDATORY: clone method ("vitual constructor")
      TrackNumVeloRClusters* clone() const override { return new TrackNumVeloRClusters( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
    };
    // ========================================================================
    /** @class TrackNumVeloPhiClusters
     *  The function which obtains the number of velo Phi clusters belonging to
     *  the track
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *
     *  @author Hendrik JAGE hendrik.jage@cern.ch
     *  @date 2023-06-13
     */
    class GAUDI_API TrackNumVeloPhiClusters final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// Default Constructor
      TrackNumVeloPhiClusters() : AuxFunBase{std::tie()} {}
      /// MANDATORY: clone method ("vitual constructor")
      TrackNumVeloPhiClusters* clone() const override { return new TrackNumVeloPhiClusters( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
    };
    // ========================================================================
    /** @class TrackVeloClusterADC
     *  The function which obtains the ADC of a given velo cluster belonging to
     *  the track
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *
     *  @author Hendrik JAGE hendrik.jage@cern.ch
     *  @date 2021-03-16
     */
    class GAUDI_API TrackVeloClusterADC final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /** constructor
       *  @param nCl: VELO track cluster number
       *  @param nSt: Strip number of selected VELO track cluster (optional)
       *              (default = -1: Total cluster adc is returned)
       */
      TrackVeloClusterADC( const int nCl, const int nSt = -1 );
      /// MANDATORY: clone method ("vitual constructor")
      TrackVeloClusterADC* clone() const override { return new TrackVeloClusterADC( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      int m_nCl; // VELO track cluster number
      //
      int m_nSt; // Strip number of selected VELO track cluster (default = -1)
      //
      mutable const LHCb::VeloClusters* m_clusters = nullptr;
      /// the default constructor is disabled
      TrackVeloClusterADC(); // the default constructor is disabled
      // ======================================================================
    };
    // ========================================================================
    /** @class TrackVeloClusterDEDX
     *  The function which obtains the DEDX of a given velo cluster belonging to
     *  the track
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *
     *  @author Hendrik JAGE hendrik.jage@cern.ch
     *  @date 2021-03-16
     */
    class GAUDI_API TrackVeloClusterDEDX final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /** constructor
       *  @param nCl: VELO track cluster number
       *  @param nSt: Strip number of selected VELO track cluster (optional)
       *              (default = -1: Total cluster adc is returned)
       */
      TrackVeloClusterDEDX( const int nCl, const int nSt = -1 );
      /// MANDATORY: clone method ("vitual constructor")
      TrackVeloClusterDEDX* clone() const override { return new TrackVeloClusterDEDX( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      int m_nCl; // VELO track cluster number
      //
      int m_nSt; // Strip number of selected VELO track cluster (default = -1)
      //
      mutable const LHCb::VeloClusters* m_clusters = nullptr;
      /// the default constructor is disabled
      TrackVeloClusterDEDX(); // the default constructor is disabled
      // ======================================================================
    };
    // ======================================================================
    /** @class TrackVeloClusterNumStrips
     *  The function which obtains the number of strips of a given velo cluster
     *  belonging to the track
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *
     *  @author Hendrik JAGE hendrik.jage@cern.ch
     *  @date 2021-03-16
     */
    class GAUDI_API TrackVeloClusterNumStrips final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /** constructor
       *  @param nCl: VELO track cluster number
       */
      TrackVeloClusterNumStrips( const int nCl );
      /// MANDATORY: clone method ("vitual constructor")
      TrackVeloClusterNumStrips* clone() const override { return new TrackVeloClusterNumStrips( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      int m_nCl; // VELO track cluster number
      //
      mutable const LHCb::VeloClusters* m_clusters = nullptr;
      /// the default constructor is disabled
      TrackVeloClusterNumStrips(); // the default constructor is disabled
      // ======================================================================
    };
    // ========================================================================
    /** @class TrackVeloClusterADCMedian
     *  The function which obtains the Median of the velo cluster ADC info
     *  belonging to the track
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *
     *  @author Hendrik JAGE hendrik.jage@cern.ch
     *  @date 2023-06-07
     */
    class GAUDI_API TrackVeloClusterADCMedian final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// Default Constructor
      TrackVeloClusterADCMedian() : AuxFunBase{std::tie()} {}
      /// MANDATORY: clone method ("vitual constructor")
      TrackVeloClusterADCMedian* clone() const override { return new TrackVeloClusterADCMedian( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      mutable const LHCb::VeloClusters* m_clusters = nullptr;
      // ======================================================================
    };
    // ========================================================================
    /** @class TrackVeloRClusterADCMedian
     *  The function which obtains the Median of the R-sensor velo cluster ADC
     *  info belonging to the track
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *
     *  @author Hendrik JAGE hendrik.jage@cern.ch
     *  @date 2023-06-12
     */
    class GAUDI_API TrackVeloRClusterADCMedian final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// Default Constructor
      TrackVeloRClusterADCMedian() : AuxFunBase{std::tie()} {}
      /// MANDATORY: clone method ("vitual constructor")
      TrackVeloRClusterADCMedian* clone() const override { return new TrackVeloRClusterADCMedian( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      mutable const LHCb::VeloClusters* m_clusters = nullptr;
      // ======================================================================
    };
    // ========================================================================
    /** @class TrackVeloPhiClusterADCMedian
     *  The function which obtains the Median of the Phi-sensor velo cluster ADC
     *  info belonging to the track
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *
     *  @author Hendrik JAGE hendrik.jage@cern.ch
     *  @date 2023-06-12
     */
    class GAUDI_API TrackVeloPhiClusterADCMedian final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// Default Constructor
      TrackVeloPhiClusterADCMedian() : AuxFunBase{std::tie()} {}
      /// MANDATORY: clone method ("vitual constructor")
      TrackVeloPhiClusterADCMedian* clone() const override { return new TrackVeloPhiClusterADCMedian( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      mutable const LHCb::VeloClusters* m_clusters = nullptr;
      // ======================================================================
    };
    // ========================================================================
    /** @class TrackVeloClusterDEDXMedian
     *  The function which obtains the Median of the velo cluster DEDX info
     *  belonging to the track
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *
     *  @author Hendrik JAGE hendrik.jage@cern.ch
     *  @date 2021-03-18
     */
    class GAUDI_API TrackVeloClusterDEDXMedian final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// Default Constructor
      TrackVeloClusterDEDXMedian() : AuxFunBase{std::tie()} {}
      /// MANDATORY: clone method ("vitual constructor")
      TrackVeloClusterDEDXMedian* clone() const override { return new TrackVeloClusterDEDXMedian( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      mutable const LHCb::VeloClusters* m_clusters = nullptr;
      // ======================================================================
    };
    // ========================================================================
    /** @class TrackVeloRClusterDEDXMedian
     *  The function which obtains the Median of the R-sensor velo cluster
     *  DEDX info belonging to the track, i.e. incidence angle corrected ADC.
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *  @see TrackVeloRClusterADCMedian
     *
     *  @author Hendrik JAGE hendrik.jage@cern.ch
     *  @date 2023-06-12
     */
    class GAUDI_API TrackVeloRClusterDEDXMedian final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// Default Constructor
      TrackVeloRClusterDEDXMedian() : AuxFunBase{std::tie()} {}
      /// MANDATORY: clone method ("vitual constructor")
      TrackVeloRClusterDEDXMedian* clone() const override { return new TrackVeloRClusterDEDXMedian( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class TrackVeloPhiClusterDEDXMedian
     *  The function which obtains the Median of the Phi-sensor velo cluster
     *  DEDX info belonging to the track, i.e. incidence angle corrected ADC.
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *  @see TrackVeloPhiClusterADCMedian
     *
     *  @author Hendrik JAGE hendrik.jage@cern.ch
     *  @date 2023-06-12
     */
    class GAUDI_API TrackVeloPhiClusterDEDXMedian final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// Default Constructor
      TrackVeloPhiClusterDEDXMedian() : AuxFunBase{std::tie()} {}
      /// MANDATORY: clone method ("vitual constructor")
      TrackVeloPhiClusterDEDXMedian* clone() const override { return new TrackVeloPhiClusterDEDXMedian( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class TrackVeloClusterNumOverflows
     *  The function which obtains the number of saturated strips in the velo
     *  clusters of the track. Only clusters up to a given number of strips
     *  are considered.
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *
     *  @author Hendrik JAGE hendrik.jage@cern.ch
     *  @date 2021-03-16
     */
    class GAUDI_API TrackVeloClusterNumOverflows final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /** constructor
       *  @param nMax: Maximum strip-number of clusters to be still considered.
       *               By default only 1-, 2- or 3-strip clusters are used.
       */
      TrackVeloClusterNumOverflows( const int nMax = 3 );
      /// MANDATORY: clone method ("vitual constructor")
      TrackVeloClusterNumOverflows* clone() const override { return new TrackVeloClusterNumOverflows( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      mutable const LHCb::VeloClusters* m_clusters = nullptr;
      // ======================================================================
      int                m_nMax;
      const unsigned int m_threshold = 127;
      // ======================================================================
    };
    // ========================================================================
    /** @class TrackVeloRClusterNumOverflows
     *  The function which obtains the number of saturated strips in the velo
     *  R-sensor clusters of the track. Only clusters up to a given number of
     *  strips are considered.
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *
     *  @author Hendrik JAGE hendrik.jage@cern.ch
     *  @date 2023-06-15
     */
    class GAUDI_API TrackVeloRClusterNumOverflows final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /** constructor
       *  @param nMax: Maximum strip-number of clusters to be still considered.
       *               By default only 1-, 2- or 3-strip clusters are used.
       */
      TrackVeloRClusterNumOverflows( const int nMax = 3 );
      /// MANDATORY: clone method ("vitual constructor")
      TrackVeloRClusterNumOverflows* clone() const override { return new TrackVeloRClusterNumOverflows( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      mutable const LHCb::VeloClusters* m_clusters = nullptr;
      // ======================================================================
      int                m_nMax;
      const unsigned int m_threshold = 127;
      // ======================================================================
    };
    // ========================================================================
    /** @class TrackVeloPhiClusterNumOverflows
     *  The function which obtains the number of saturated strips in the velo
     *  Phi-sensor clusters of the track. Only clusters up to a given number of
     *  strips are considered.
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *
     *  @author Hendrik JAGE hendrik.jage@cern.ch
     *  @date 2023-06-15
     */
    class GAUDI_API TrackVeloPhiClusterNumOverflows final
        : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /** constructor
       *  @param nMax: Maximum strip-number of clusters to be still considered.
       *               By default only 1-, 2- or 3-strip clusters are used.
       */
      TrackVeloPhiClusterNumOverflows( const int nMax = 3 );
      /// MANDATORY: clone method ("vitual constructor")
      TrackVeloPhiClusterNumOverflows* clone() const override { return new TrackVeloPhiClusterNumOverflows( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      mutable const LHCb::VeloClusters* m_clusters = nullptr;
      // ======================================================================
      int                m_nMax;
      const unsigned int m_threshold = 127;
      // ======================================================================
    };
    // ========================================================================
    /** @class TrackFracVeloClusterOverflows
     *  The function which obtains the fraction of saturated strips in the velo
     *  per cluster of the track. Only clusters up to a given number of strips
     *  are considered.
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *
     *  @author Hendrik JAGE hendrik.jage@cern.ch
     *  @date 2021-03-16
     */
    class GAUDI_API TrackFracVeloClusterOverflows final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /** constructor
       *  @param nMax: Maximum strip-number of clusters to be still considered.
       *               By default only 1-, 2- or 3-strip clusters are used.
       */
      TrackFracVeloClusterOverflows( const int nMax = 3 );
      /// MANDATORY: clone method ("vitual constructor")
      TrackFracVeloClusterOverflows* clone() const override { return new TrackFracVeloClusterOverflows( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      mutable const LHCb::VeloClusters* m_clusters = nullptr;
      // ======================================================================
      int                m_nMax;
      const unsigned int m_threshold = 127;
      // ======================================================================
    };
    // ========================================================================
    /** @class TrackNumTTClusters
     *  The function which obtains the number of tt clusters belonging to the
     *  track
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *
     *  @author Hendrik JAGE hendrik.jage@cern.ch
     *  @date 2023-06-07
     */
    class GAUDI_API TrackNumTTClusters final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// Default Constructor
      TrackNumTTClusters() : AuxFunBase{std::tie()} {}
      /// MANDATORY: clone method ("vitual constructor")
      TrackNumTTClusters* clone() const override { return new TrackNumTTClusters( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
    };
    // ======================================================================
    /** @class TrackTTCosTheta
     *  The function which obtains the cosine of the theta angle, for the track
     *  state closest to z = 2485 mm, which is in the middle of the TT
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *
     *  @author Hendrik JAGE hendrik.jage@cern.ch
     *  @date 2023-06-07
     */
    class GAUDI_API TrackTTCosTheta final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /** constructor
       *  @param nCl: VELO track cluster number
       */
      /// Constructor
      TrackTTCosTheta();
      /// MANDATORY:  virtual destructor
      virtual ~TrackTTCosTheta();
      /// MANDATORY: clone method ("vitual constructor")
      TrackTTCosTheta* clone() const override { return new TrackTTCosTheta( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      // TrackStateProvider interface
      LoKi::Interface<ITrackStateProvider> m_extrapolator;
      std::string                          m_tool_name     = "TrackStateProvider";
      const double                         m_tt_z_position = 2485;
    };
    // ========================================================================
    /** @class TrackTTClusterADCMedian
     *  The function which obtains the Median of the TT cluster ADC info
     *  belonging to the track
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *
     *  @author Hendrik JAGE hendrik.jage@cern.ch
     *  @date 2023-06-07
     */
    class GAUDI_API TrackTTClusterADCMedian final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// Default Constructor
      TrackTTClusterADCMedian() : AuxFunBase{std::tie()} {}
      /// MANDATORY: clone method ("vitual constructor")
      TrackTTClusterADCMedian* clone() const override { return new TrackTTClusterADCMedian( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      mutable const LHCb::STClusters* m_clusters = nullptr;
      // ======================================================================
    };
    // ========================================================================
    /** @class TrackTTClusterDEDXMedian
     *  The function which obtains the Median of the TT cluster DEDX info
     *  belonging to the track, i.e. ADC correct for the incident angle
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *  @see TrackTTClusterADCMedian
     *  @see TrackTTCosTheta
     *
     *  @author Hendrik JAGE hendrik.jage@cern.ch
     *  @date 2023-06-12
     */
    class GAUDI_API TrackTTClusterDEDXMedian final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// Default Constructor
      TrackTTClusterDEDXMedian() : AuxFunBase{std::tie()} {}
      /// MANDATORY: clone method ("vitual constructor")
      TrackTTClusterDEDXMedian* clone() const override { return new TrackTTClusterDEDXMedian( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class TrackTTClusterNumOverflows
     *  The function which obtains the number of saturated strips in the TT
     *  clusters of the track. Only clusters up to a given number of strips
     *  are considered.
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *
     *  @author Hendrik JAGE hendrik.jage@cern.ch
     *  @date 2023-06-13
     */
    class GAUDI_API TrackTTClusterNumOverflows final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /** constructor
       *  @param nMax: Maximum strip-number of clusters to be still considered.
       *               By default only 1-, 2- or 3-strip clusters are used.
       */
      TrackTTClusterNumOverflows( const int nMax = 3 );
      /// MANDATORY: clone method ("vitual constructor")
      TrackTTClusterNumOverflows* clone() const override { return new TrackTTClusterNumOverflows( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      mutable const LHCb::STClusters* m_clusters = nullptr;
      // ======================================================================
      int                m_nMax;
      const unsigned int m_threshold = 127;
      // ======================================================================
    };
    // ======================================================================
    /** @class TrackTCosTheta
     *  The function which obtains the cosine of the theta angle, for the track
     *  state closest to z = 8340 mm, which is in the middle of the T1-3
     *  stations.
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *
     *  @author Hendrik JAGE hendrik.jage@cern.ch
     *  @date 2023-06-07
     */
    class GAUDI_API TrackTCosTheta final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /** constructor
       *  @param nCl: VELO track cluster number
       */
      /// Constructor
      TrackTCosTheta();
      /// MANDATORY:  virtual destructor
      virtual ~TrackTCosTheta();
      /// MANDATORY: clone method ("vitual constructor")
      TrackTCosTheta* clone() const override { return new TrackTCosTheta( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      // TrackStateProvider interface
      LoKi::Interface<ITrackStateProvider> m_extrapolator;
      std::string                          m_tool_name    = "TrackStateProvider";
      const double                         m_t_z_position = 8340;
    };
    // ========================================================================
    /** @class TrackNumITClusters
     *  The function which obtains the number of IT clusters belonging to the
     *  track
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *
     *  @author Hendrik JAGE hendrik.jage@cern.ch
     *  @date 2023-06-07
     */
    class GAUDI_API TrackNumITClusters final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// Default Constructor
      TrackNumITClusters() : AuxFunBase{std::tie()} {}
      /// MANDATORY: clone method ("vitual constructor")
      TrackNumITClusters* clone() const override { return new TrackNumITClusters( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
    };
    // ========================================================================
    /** @class TrackITClusterADCMedian
     *  The function which obtains the Median of the IT cluster ADC info
     *  belonging to the track
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *
     *  @author Hendrik JAGE hendrik.jage@cern.ch
     *  @date 2023-06-07
     */
    class GAUDI_API TrackITClusterADCMedian final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// Default Constructor
      TrackITClusterADCMedian() : AuxFunBase{std::tie()} {}
      /// MANDATORY: clone method ("vitual constructor")
      TrackITClusterADCMedian* clone() const override { return new TrackITClusterADCMedian( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      mutable const LHCb::STClusters* m_clusters = nullptr;
      // ======================================================================
    };
    // ========================================================================
    /** @class TrackITClusterDEDXMedian
     *  The function which obtains the Median of the IT cluster DEDX info
     *  belonging to the track. As the IT sensors have different thicknesses,
     *  they are corrected for besides the incidence angle.
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *  @see TrackTCosTheta
     *
     *  @author Hendrik JAGE hendrik.jage@cern.ch
     *  @date 2023-06-09
     */
    class GAUDI_API TrackITClusterDEDXMedian final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      TrackITClusterDEDXMedian();
      /// MANDATORY: clone method ("vitual constructor")
      TrackITClusterDEDXMedian* clone() const override { return new TrackITClusterDEDXMedian( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      mutable const DeSTDetector*     m_ITdet           = nullptr;
      mutable const LHCb::STClusters* m_clusters        = nullptr;
      const double                    m_thres_thickness = 0.35;
      const double                    m_thin_shift      = 2.8;
      const double                    m_thin_scale      = 1.108;
      // ======================================================================
    };
    // ========================================================================
    /** @class TrackITClusterNumOverflows
     *  The function which obtains the number of saturated strips in the IT
     *  clusters of the track. Only clusters up to a given number of strips
     *  are considered.
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *
     *  @author Hendrik JAGE hendrik.jage@cern.ch
     *  @date 2023-06-13
     */
    class GAUDI_API TrackITClusterNumOverflows final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /** constructor
       *  @param nMax: Maximum strip-number of clusters to be still considered.
       *               By default only 1-, 2- or 3-strip clusters are used.
       */
      TrackITClusterNumOverflows( const int nMax = 3 );
      /// MANDATORY: clone method ("vitual constructor")
      TrackITClusterNumOverflows* clone() const override { return new TrackITClusterNumOverflows( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      mutable const LHCb::STClusters* m_clusters = nullptr;
      // ======================================================================
      int                m_nMax;
      const unsigned int m_threshold = 127;
      // ======================================================================
    };
    // ========================================================================
    /** @class TrackNumOTTimes
     *  The function which obtains the number of OT times/hits belonging to the
     *  track
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *
     *  @author Hendrik JAGE hendrik.jage@cern.ch
     *  @date 2023-06-12
     */
    class GAUDI_API TrackNumOTTimes final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// Default Constructor
      TrackNumOTTimes() : AuxFunBase{std::tie()} {}
      /// MANDATORY: clone method ("vitual constructor")
      TrackNumOTTimes* clone() const override { return new TrackNumOTTimes( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
    };
    // ========================================================================
    /** @class TrackTime
     *  The function which obtains the track time via the TrackT0Tool.
     *  For an explanation see CERN-LHCb-DP-2017-001
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *  @see TrackT0Tool
     *
     *  @author Hendrik JAGE hendrik.jage@cern.ch
     *  @date 2023-06-12
     */
    class GAUDI_API TrackTime final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      TrackTime();
      /// MANDATORY:  virtual destructor
      virtual ~TrackTime();
      /// MANDATORY: clone method ("vitual constructor")
      TrackTime* clone() const override { return new TrackTime( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      // TrackT0Tool interface
      LoKi::Interface<ITrackT0Tool> m_trackT0Tool;
      std::string                   m_tool_name = "TrackT0Tool";
      // ======================================================================
    };
    // ========================================================================
  } // namespace Particles
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
// The END
// ============================================================================
#endif // LOKI_PARTICLES49_H
// ============================================================================
