/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_PARTICLES44_H
#define LOKI_PARTICLES44_H 1
// ============================================================================
// Include files
// ============================================================================
// LoKiCore
// ============================================================================
#include "LoKi/Interface.h"
// ============================================================================
// LoKiPhys
// ============================================================================
#include "LoKi/PhysTypes.h"
// ============================================================================
// DaVinciInterfaces
// ============================================================================
#include "Kernel/IParticleValue.h"
// ============================================================================
/** @file
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Sebastian Neubert
 *  @date 2013-08-05
 */
// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace Particles {
    // ========================================================================
    /** @class Filter
     *  Simple adapter which allos to use the tool IParticleValue
     *  as LoKi functor
     *  @see IParticleValue
     *  @see LoKi::Cuts::VALUE
     *  @author Sebastian Neubert
     *  @date 2013-08-05
     */
    class GAUDI_API Value : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// constructor from the tool name
      Value( const std::string& tool );
      /// constructor from the tool
      Value( const IParticleValue* tool );
      /// copy constructor
      Value( const Value& right ) = default;
      /// MANDATORY: virtual destructor
      virtual ~Value();
      /// MANDATORY: clone method ("virtual constructor")
      Value* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// the tool itself
      LoKi::Interface<IParticleValue> m_function; // the tool itself
      // ======================================================================
    };
    // ========================================================================
  } // namespace Particles
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_PARTICLES44_H
