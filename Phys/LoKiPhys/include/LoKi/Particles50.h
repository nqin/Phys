/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_PARTICLES50_H
#  define LOKI_PARTICLES50_H 1
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#  include "GaudiKernel/Kernel.h"
// ============================================================================
// LoKiCore
// ============================================================================
#  include "LoKi/Interface.h"
// ============================================================================
// RecIinterfaces
// ============================================================================
#  include "RecInterfaces/IChargedProtoDedxPIDTool.h"
// ============================================================================
// LoKiPhys
// ============================================================================
#  include "LoKi/PhysTypes.h"
// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace Particles {

    // ========================================================================
    /** @class DedxPID
     *  Wrapper for DedxGlobalPID::IChargedProtoDedxPIDTool to access DedxPID
     *  variables for various tunings
     *  @see LoKi::Cuts::DedxPID
     *  @see DedxGlobalPID::IChargedProtoDedxPIDTool
     *  @see DedxGlobalPID::ChargedProtoDedxPIDTool
     *  @author Hendrik Jage Hendrik.Jage@cern.ch
     *  @date   07.07.2023
     */
    class GAUDI_API DedxPID : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      using Hypothesis = DedxGlobalPID::IChargedProtoDedxPIDTool::Hypothesis;
      using Detector   = DedxGlobalPID::IChargedProtoDedxPIDTool::Detector;
      using Version    = DedxGlobalPID::IChargedProtoDedxPIDTool::Version;
      using Type       = DedxGlobalPID::IChargedProtoDedxPIDTool::Type;
      // ======================================================================
      /** constructor from particle hypothesis, detector type, version & tool
       *  @param hypothesis The PID hypothesis to evaluate for this particle
       *  @param detector   Subdetector dedx info to use
       *  @param version    PDD version to use
       *  @param tool       typename for the tool
       */
      DedxPID( const std::string& hypothesis, const std::string& detector, const std::string& version,
               const std::string& type, const std::string& tool );
      /** constructor from particle hypothesis, detector type, version
       *  @param hypothesis The PID hypothesis to evaluate for this particle
       *  @param detector   Subdetector dedx info to use
       *  @param version    PDD version to use
       */
      DedxPID( const std::string& hypothesis, const std::string& detector, const std::string& version,
               const std::string& type = "" );
      /// mandatory:  virtual destructor
      virtual ~DedxPID();
      /// MANDATORY: clone method ("virtual contructor")
      virtual DedxPID* clone() const override;
      /// MANDATORY: the only one important method
      virtual result_type operator()( argument p ) const override;
      /// OPTIONAL: nice printout
      virtual std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// no  default constructor
      DedxPID(); // no default constructor
      // ======================================================================
    private:
      // ======================================================================
      /// particle hypothesis
      std::string s_hypothesis;
      Hypothesis  m_hypothesis;
      /// detector
      std::string s_detector;
      Detector    m_detector;
      /// version
      std::string s_version;
      Version     m_version;
      /// Type
      std::string s_type;
      Type        m_type;
      /// tool typename
      std::string m_toolname;
      /// DedxPID tool
      LoKi::Interface<DedxGlobalPID::IChargedProtoDedxPIDTool> m_tool;
      /// list of allowed helium names
      const std::vector<std::string> s_HE = {"HELIUM", "HE",     "He++", "He--", "Helium", "He", "he++",
                                             "he--",   "helium", "he",   "Z2",   "z2",     "q2"};
      /// list of allowed gamma conversion names
      const std::vector<std::string> s_GC = {"gammaConversion", "gC", "GC", "e+e-", "pairProduction", "pP"};
      /// list of allowed background names
      const std::vector<std::string> s_Z1 = {"BACKGROUND", "MIPS", "Z1", "BG", "BKG",
                                             "Background", "Mips", "z1", "Bg", "Bkg",
                                             "background", "mips", "q1", "bg", "bkg"};
      /// list of allowed version 1 names
      const std::vector<std::string> s_V1 = {"V1", "v1", "PDDv1", "PDDV1", "pddv1"};
      /// list of allowed version 2 names
      const std::vector<std::string> s_V2 = {"V2", "v2", "PDDv2", "PDDV2", "pddv2"};
      /// list of allowed version 3 names
      const std::vector<std::string> s_V3 = {"V3", "v3", "PDDv3", "PDDV3", "pddv3"};
      /// list of allowed VELO names
      const std::vector<std::string> s_VELO = {"VELO", "Velo", "velo"};
      /// list of allowed VELO R names
      const std::vector<std::string> s_R = {"VELOR", "VeloR", "veloR", "VLR", "R", "r"};
      /// list of allowed VELO Phi names
      const std::vector<std::string> s_PHI = {"VELOPHI", "VeloPHI", "veloPHI", "VELOPhi", "VeloPhi", "veloPhi",
                                              "VLP",     "VLPHI",   "VLPhi",   "PHI",     "Phi",     "phi"};
      /// list of allowed TT names
      const std::vector<std::string> s_TT = {
          "TT",
          "tt",
      };
      /// list of allowed IT names
      const std::vector<std::string> s_IT = {
          "IT",
          "it",
      };
      /// list of allowed IT thin names
      const std::vector<std::string> s_IT1 = {"IT1", "it1", "ITthin", "ITThin", "itthin"};
      /// list of allowed IT thick names
      const std::vector<std::string> s_IT2 = {"IT2", "it2", "ITthick", "ITThick", "itthick"};
      /// list of allowed LL2D name
      const std::vector<std::string> s_LL2D  = {"", // default!
                                               "LL2D", "ll2d", "ll2D", "LL2d", "nCl2dLL"};
      const double                   err_val = -800;
      // ======================================================================
    };
    // ========================================================================
  } // namespace Particles
  // ==========================================================================
} // namespace LoKi
// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace Cuts {
    // ========================================================================
    /** @typedef DedxPID
     *  @code
     *  Fun fun = DedxPID ( "e+" , "MC15TuneFLAT4dV1" ) ;
     *  const LHCb::Particle* p = ... ;
     *  double result = fun ( p ) ;
     *  @endcode
     *  @see LoKi::Particles::DedxPID
     *  @see DedxGlobalPID::IChargedProtoDedxPIDTool
     *  @see DedxGlobalPID::ChargedProtoDedxPIDTool
     *  @author Hendrik Jage Hendrik.Jage@cern.ch
     *  @date   07.07.2023
     */
    typedef LoKi::Particles::DedxPID DedxPID;
    // ========================================================================
  } // namespace Cuts
  // ==========================================================================
} // namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_PARTICLES50_H
// ============================================================================
