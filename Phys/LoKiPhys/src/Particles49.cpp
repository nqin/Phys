/*****************************************************************************\
* (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// STD & STL
// ============================================================================
#include <algorithm>
// ============================================================================
// Event
// ============================================================================
#include "Event/Track.h"
// ============================================================================
// LoKiCore
// ============================================================================
#include "LoKi/Constants.h"
#include "LoKi/Print.h"
// ============================================================================
// LoKiPhys
// ============================================================================
#include "LoKi/Particles49.h"
#include "LoKi/PhysExtract.h"
// ============================================================================
// Detector
// ============================================================================
#include "STDet/DeSTDetector.h"
// ============================================================================
// GSL
// ============================================================================
#include "gsl/gsl_cdf.h"
// ============================================================================
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiAlg/GetAlgs.h"
#include "GaudiKernel/IAlgContextSvc.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/IDetDataSvc.h"
#include "GaudiKernel/IToolSvc.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "GaudiKernel/SmartIF.h"
// ============================================================================
/** @file
 *
 *  Implementation file for functions from namespace  LoKi::Particles
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-02-22
 */
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackNumVeloClusters::result_type LoKi::Particles::TrackNumVeloClusters::
                                                   operator()( LoKi::Particles::TrackNumVeloClusters::argument p ) const {
  //
  const int errVal = -1;
  //
  if ( !p ) {
    Error( "Argument is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( !track ) {
    Error( "Track    is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  int nClustersOnTrack = 0;
  //
  const std::vector<LHCb::LHCbID>& ids = ( track )->lhcbIDs();
  for ( std::vector<LHCb::LHCbID>::const_iterator it = ids.begin(); it != ids.end(); ++it ) {
    if ( it->isVelo() == true ) { nClustersOnTrack++; }
  } // lhcbids
  //
  return nClustersOnTrack; // RETURN
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackNumVeloClusters::fillStream( std::ostream& s ) const {
  return s << "TRVELOCLUSTERS";
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackNumVeloRClusters::result_type LoKi::Particles::TrackNumVeloRClusters::
                                                    operator()( LoKi::Particles::TrackNumVeloRClusters::argument p ) const {
  //
  const int errVal = -1;
  //
  if ( !p ) {
    Error( "Argument is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( !track ) {
    Error( "Track    is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  int nClustersOnTrack = 0;
  //
  const std::vector<LHCb::LHCbID>& ids = ( track )->lhcbIDs();
  for ( std::vector<LHCb::LHCbID>::const_iterator it = ids.begin(); it != ids.end(); ++it ) {
    if ( it->isVeloR() == true ) { nClustersOnTrack++; }
  } // lhcbids
  //
  return nClustersOnTrack; // RETURN
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackNumVeloRClusters::fillStream( std::ostream& s ) const {
  return s << "TRVELORCLUSTERS";
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackNumVeloPhiClusters::result_type LoKi::Particles::TrackNumVeloPhiClusters::
                                                      operator()( LoKi::Particles::TrackNumVeloPhiClusters::argument p ) const {
  //
  const int errVal = -1;
  //
  if ( !p ) {
    Error( "Argument is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( !track ) {
    Error( "Track    is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  int nClustersOnTrack = 0;
  //
  const std::vector<LHCb::LHCbID>& ids = ( track )->lhcbIDs();
  for ( std::vector<LHCb::LHCbID>::const_iterator it = ids.begin(); it != ids.end(); ++it ) {
    if ( it->isVeloPhi() == true ) { nClustersOnTrack++; }
  } // lhcbids
  //
  return nClustersOnTrack; // RETURN
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackNumVeloPhiClusters::fillStream( std::ostream& s ) const {
  return s << "TRVELOPHICLUSTERS";
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackVeloClusterADC::TrackVeloClusterADC( const int nCl, const int nSt )
    : LoKi::AuxFunBase( std::tie( nCl, nSt ) ), m_nCl( nCl ), m_nSt( nSt ) {}
// ============================================================================
LoKi::Particles::TrackVeloClusterADC::result_type LoKi::Particles::TrackVeloClusterADC::
                                                  operator()( LoKi::Particles::TrackVeloClusterADC::argument p ) const {
  //
  const double errVal = -1;
  //
  if ( !p ) {
    Error( "Argument  is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( !track ) {
    Error( "Track     is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  int    nClustersOnTrack = 0;
  double adc              = 0;
  //
  const std::vector<LHCb::LHCbID>& ids = ( track )->lhcbIDs();
  for ( std::vector<LHCb::LHCbID>::const_iterator it = ids.begin(); it != ids.end(); ++it ) {
    if ( it->isVelo() == true ) {
      if ( nClustersOnTrack == m_nCl ) {
        const LHCb::VeloChannelID vcID = ( it )->veloID();
        // Veloclusters
        SmartIF<IDataProviderSvc>        ds( lokiSvc().getObject() );
        SmartDataPtr<LHCb::VeloClusters> data( ds, LHCb::VeloClusterLocation::Default );
        if ( !data ) {
          Warning( "No table at location " + LHCb::VeloClusterLocation::Default ).ignore();
          return errVal;
        }
        m_clusters                 = data;
        LHCb::VeloCluster* cluster = m_clusters->object( vcID );
        if ( !cluster ) {
          Error( "Cluster   is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
          return errVal; // RETURN
        }
        if ( 0 == cluster ) {
          Error( "ClusterID is invalid with ID " + Gaudi::Utils::toString( vcID ) ).ignore();
          return errVal; // RETURN
        }
        // Cluster ADC
        if ( m_nSt == -1 ) {
          adc = cluster->totalCharge();
        }
        // ADC value of selected strip
        else if ( ( 0 <= m_nSt ) && ( m_nSt < (int)cluster->size() ) ) {
          adc = cluster->adcValue( m_nSt );
        }
        //
        break;
      }
      nClustersOnTrack++;
    }
  } // lhcbids
  //
  return adc; // RETURN
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackVeloClusterADC::fillStream( std::ostream& s ) const {
  s << "TRVELOCLUSTERADC(" << m_nCl;
  if ( m_nSt != -1 ) { s << "," << m_nSt; }
  return s << ")";
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackVeloClusterDEDX::TrackVeloClusterDEDX( const int nCl, const int nSt )
    : LoKi::AuxFunBase( std::tie( nCl, nSt ) ), m_nCl( nCl ), m_nSt( nSt ) {}
// ============================================================================
LoKi::Particles::TrackVeloClusterDEDX::result_type LoKi::Particles::TrackVeloClusterDEDX::
                                                   operator()( LoKi::Particles::TrackVeloClusterDEDX::argument p ) const {
  //
  const double errVal = -1;
  //
  if ( !p ) {
    Error( "Argument  is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( !track ) {
    Error( "Track     is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  const TrackVeloClusterADC fun = TrackVeloClusterADC( m_nCl, m_nSt );
  const double              adc = fun( p );
  //
  const double theta        = track->slopes().Theta();
  double       angle_factor = 1;
  if ( cos( theta ) != 0 ) { angle_factor = cos( theta ); }
  return adc * angle_factor; // RETURN
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackVeloClusterDEDX::fillStream( std::ostream& s ) const {
  s << "TRVELOCLUSTERDEDX(" << m_nCl;
  if ( m_nSt != -1 ) { s << "," << m_nSt; }
  return s << ")";
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackVeloClusterNumStrips::TrackVeloClusterNumStrips( const int nCl )
    : LoKi::AuxFunBase( std::tie( nCl ) ), m_nCl( nCl ) {}
// ============================================================================
LoKi::Particles::TrackVeloClusterNumStrips::result_type LoKi::Particles::TrackVeloClusterNumStrips::
                                                        operator()( LoKi::Particles::TrackVeloClusterNumStrips::argument p ) const {
  //
  const int errVal = -1;
  //
  if ( !p ) {
    Error( "Argument  is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( !track ) {
    Error( "Track     is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  int nClustersOnTrack = 0;
  //
  int                              clSize = 0;
  const std::vector<LHCb::LHCbID>& ids = ( track )->lhcbIDs();
  for ( std::vector<LHCb::LHCbID>::const_iterator it = ids.begin(); it != ids.end(); ++it ) {
    if ( it->isVelo() == true ) {
      if ( nClustersOnTrack == m_nCl ) {
        const LHCb::VeloChannelID vcID = ( it )->veloID();
        // Veloclusters
        SmartIF<IDataProviderSvc>        ds( lokiSvc().getObject() );
        SmartDataPtr<LHCb::VeloClusters> data( ds, LHCb::VeloClusterLocation::Default );
        if ( !data ) {
          Warning( "No table at location " + LHCb::VeloClusterLocation::Default ).ignore();
          return errVal;
        }
        m_clusters                 = data;
        LHCb::VeloCluster* cluster = m_clusters->object( vcID );
        if ( !cluster ) {
          Error( "Cluster   is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
          return errVal; // RETURN
        }
        if ( 0 == cluster ) {
          Error( "ClusterID is invalid with ID " + Gaudi::Utils::toString( vcID ) ).ignore();
          return errVal; // RETURN
        }
        //
        clSize = cluster->size();
        break;
      }
      nClustersOnTrack++;
    }
  } // lhcbids
  //
  return clSize; // RETURN
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackVeloClusterNumStrips::fillStream( std::ostream& s ) const {
  s << "TRVELOCLUSTERSTRIPS(" << m_nCl;
  return s << ")";
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackVeloClusterADCMedian::result_type LoKi::Particles::TrackVeloClusterADCMedian::
                                                        operator()( LoKi::Particles::TrackVeloClusterADCMedian::argument p ) const {
  //
  const float errVal     = -2;
  float       ADC_Median = -1;
  //
  if ( !p ) {
    Error( "Argument  is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( !track ) {
    Error( "Track     is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  int                nClustersOnTrack = 0;
  std::vector<float> ADC_vec;
  //
  m_clusters = nullptr;
  //
  const std::vector<LHCb::LHCbID>& ids = ( track )->lhcbIDs();
  for ( std::vector<LHCb::LHCbID>::const_iterator it = ids.begin(); it != ids.end(); ++it ) {
    if ( it->isVelo() == true ) {
      const LHCb::VeloChannelID vcID = ( it )->veloID();
      if ( m_clusters == nullptr ) {
        // Veloclusters
        SmartIF<IDataProviderSvc>        ds( lokiSvc().getObject() );
        SmartDataPtr<LHCb::VeloClusters> data( ds, LHCb::VeloClusterLocation::Default );
        if ( !data ) {
          Warning( "No table at location " + LHCb::VeloClusterLocation::Default ).ignore();
          return errVal;
        }
        m_clusters = data;
      }
      LHCb::VeloCluster* cluster = m_clusters->object( vcID );
      if ( !cluster ) {
        Error( "Cluster   is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
        return errVal; // RETURN
      }
      if ( 0 == cluster ) {
        Error( "ClusterID is invalid with ID " + Gaudi::Utils::toString( vcID ) ).ignore();
        return errVal; // RETURN
      }
      //
      const float ADC = cluster->totalCharge();
      ADC_vec.push_back( ADC );
      nClustersOnTrack++;
    }
  } // lhcbids
  //
  if ( nClustersOnTrack == 0 ) {
    return ADC_Median; // RETURN
  }
  //
  std::sort( ADC_vec.begin(), ADC_vec.end() );
  int n = int( nClustersOnTrack / 2 );
  if ( nClustersOnTrack % 2 == 1 ) // odd
  {
    ADC_Median = ADC_vec[n];
  } else // even
  {
    ADC_Median = 0.5 * ( ADC_vec[n] + ADC_vec[n - 1] );
  }
  return ADC_Median; // RETURN
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackVeloClusterADCMedian::fillStream( std::ostream& s ) const {
  return s << "TRVELOCLUSTERADCMEDIAN";
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackVeloRClusterADCMedian::result_type LoKi::Particles::TrackVeloRClusterADCMedian::
                                                         operator()( LoKi::Particles::TrackVeloRClusterADCMedian::argument p ) const {
  //
  const float errVal     = -2;
  float       ADC_Median = -1;
  //
  if ( !p ) {
    Error( "Argument  is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( !track ) {
    Error( "Track     is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  int                nClustersOnTrack = 0;
  std::vector<float> ADC_vec;
  //
  m_clusters = nullptr;
  //
  const std::vector<LHCb::LHCbID>& ids = ( track )->lhcbIDs();
  for ( std::vector<LHCb::LHCbID>::const_iterator it = ids.begin(); it != ids.end(); ++it ) {
    if ( it->isVeloR() == true ) {
      const LHCb::VeloChannelID vcID = ( it )->veloID();
      if ( m_clusters == nullptr ) {
        // Veloclusters
        SmartIF<IDataProviderSvc>        ds( lokiSvc().getObject() );
        SmartDataPtr<LHCb::VeloClusters> data( ds, LHCb::VeloClusterLocation::Default );
        if ( !data ) {
          Warning( "No table at location " + LHCb::VeloClusterLocation::Default ).ignore();
          return errVal;
        }
        m_clusters = data;
      }
      LHCb::VeloCluster* cluster = m_clusters->object( vcID );
      if ( !cluster ) {
        Error( "Cluster   is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
        return errVal; // RETURN
      }
      if ( 0 == cluster ) {
        Error( "ClusterID is invalid with ID " + Gaudi::Utils::toString( vcID ) ).ignore();
        return errVal; // RETURN
      }
      //
      const float ADC = cluster->totalCharge();
      ADC_vec.push_back( ADC );
      nClustersOnTrack++;
    }
  } // lhcbids
  //
  if ( nClustersOnTrack == 0 ) {
    return ADC_Median; // RETURN
  }
  //
  std::sort( ADC_vec.begin(), ADC_vec.end() );
  int n = int( nClustersOnTrack / 2 );
  if ( nClustersOnTrack % 2 == 1 ) // odd
  {
    ADC_Median = ADC_vec[n];
  } else // even
  {
    ADC_Median = 0.5 * ( ADC_vec[n] + ADC_vec[n - 1] );
  }
  return ADC_Median; // RETURN
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackVeloRClusterADCMedian::fillStream( std::ostream& s ) const {
  return s << "TRVELORCLUSTERADCMEDIAN";
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackVeloPhiClusterADCMedian::result_type LoKi::Particles::TrackVeloPhiClusterADCMedian::
                                                           operator()( LoKi::Particles::TrackVeloPhiClusterADCMedian::argument p ) const {
  //
  const float errVal     = -2;
  float       ADC_Median = -1;
  //
  if ( !p ) {
    Error( "Argument  is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( !track ) {
    Error( "Track     is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  int                nClustersOnTrack = 0;
  std::vector<float> ADC_vec;
  //
  m_clusters = nullptr;
  //
  const std::vector<LHCb::LHCbID>& ids = ( track )->lhcbIDs();
  for ( std::vector<LHCb::LHCbID>::const_iterator it = ids.begin(); it != ids.end(); ++it ) {
    if ( it->isVeloPhi() == true ) {
      const LHCb::VeloChannelID vcID = ( it )->veloID();
      if ( m_clusters == nullptr ) {
        // Veloclusters
        SmartIF<IDataProviderSvc>        ds( lokiSvc().getObject() );
        SmartDataPtr<LHCb::VeloClusters> data( ds, LHCb::VeloClusterLocation::Default );
        if ( !data ) {
          Warning( "No table at location " + LHCb::VeloClusterLocation::Default ).ignore();
          return errVal;
        }
        m_clusters = data;
      }
      LHCb::VeloCluster* cluster = m_clusters->object( vcID );
      if ( !cluster ) {
        Error( "Cluster   is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
        return errVal; // RETURN
      }
      if ( 0 == cluster ) {
        Error( "ClusterID is invalid with ID " + Gaudi::Utils::toString( vcID ) ).ignore();
        return errVal; // RETURN
      }
      //
      const float ADC = cluster->totalCharge();
      ADC_vec.push_back( ADC );
      nClustersOnTrack++;
    }
  } // lhcbids
  //
  if ( nClustersOnTrack == 0 ) {
    return ADC_Median; // RETURN
  }
  //
  std::sort( ADC_vec.begin(), ADC_vec.end() );
  int n = int( nClustersOnTrack / 2 );
  if ( nClustersOnTrack % 2 == 1 ) // odd
  {
    ADC_Median = ADC_vec[n];
  } else // even
  {
    ADC_Median = 0.5 * ( ADC_vec[n] + ADC_vec[n - 1] );
  }
  return ADC_Median; // RETURN
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackVeloPhiClusterADCMedian::fillStream( std::ostream& s ) const {
  return s << "TRVELOPHICLUSTERADCMEDIAN";
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackVeloClusterDEDXMedian::result_type LoKi::Particles::TrackVeloClusterDEDXMedian::
                                                         operator()( LoKi::Particles::TrackVeloClusterDEDXMedian::argument p ) const {
  //
  const double errVal      = -2;
  double       DEDX_Median = -1;
  //
  if ( !p ) {
    Error( "Argument  is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( !track ) {
    Error( "Track     is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  int                 nClustersOnTrack = 0;
  std::vector<double> DEDX_vec;
  //
  m_clusters = nullptr;
  //
  const double theta        = track->slopes().Theta();
  double       angle_factor = 1;
  if ( cos( theta ) != 0 ) { angle_factor = cos( theta ); }
  //
  const std::vector<LHCb::LHCbID>& ids = ( track )->lhcbIDs();
  for ( std::vector<LHCb::LHCbID>::const_iterator it = ids.begin(); it != ids.end(); ++it ) {
    if ( it->isVelo() == true ) {
      const LHCb::VeloChannelID vcID = ( it )->veloID();
      if ( m_clusters == nullptr ) {
        // Veloclusters
        SmartIF<IDataProviderSvc>        ds( lokiSvc().getObject() );
        SmartDataPtr<LHCb::VeloClusters> data( ds, LHCb::VeloClusterLocation::Default );
        if ( !data ) {
          Warning( "No table at location " + LHCb::VeloClusterLocation::Default ).ignore();
          return errVal;
        }
        m_clusters = data;
      }
      LHCb::VeloCluster* cluster = m_clusters->object( vcID );
      if ( !cluster ) {
        Error( "Cluster   is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
        return errVal; // RETURN
      }
      if ( 0 == cluster ) {
        Error( "ClusterID is invalid with ID " + Gaudi::Utils::toString( vcID ) ).ignore();
        return errVal; // RETURN
      }
      //
      double DEDX = cluster->totalCharge() * angle_factor;
      DEDX_vec.push_back( DEDX );
      nClustersOnTrack++;
    }
  } // lhcbids
  //
  if ( nClustersOnTrack == 0 ) {
    return DEDX_Median; // RETURN
  }
  //
  std::sort( DEDX_vec.begin(), DEDX_vec.end() );
  int n = int( nClustersOnTrack / 2 );
  if ( nClustersOnTrack % 2 == 1 ) // odd
  {
    DEDX_Median = DEDX_vec[n];
  } else // even
  {
    DEDX_Median = 0.5 * ( DEDX_vec[n] + DEDX_vec[n - 1] );
  }
  return DEDX_Median; // RETURN
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackVeloClusterDEDXMedian::fillStream( std::ostream& s ) const {
  return s << "TRVELOCLUSTERDEDXMEDIAN";
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackVeloRClusterDEDXMedian::result_type LoKi::Particles::TrackVeloRClusterDEDXMedian::
                                                          operator()( LoKi::Particles::TrackVeloRClusterDEDXMedian::argument p ) const {
  //
  const double errVal      = -2;
  double       DEDX_Median = -1;
  //
  if ( !p ) {
    Error( "Argument  is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( !track ) {
    Error( "Track     is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  const double theta        = track->slopes().Theta();
  double       angle_factor = 1;
  if ( cos( theta ) != 0 ) { angle_factor = cos( theta ); }
  //
  auto         adc_med_fun = TrackVeloRClusterADCMedian();
  const double ADC_Median  = adc_med_fun( p );
  //
  DEDX_Median = ADC_Median * angle_factor;
  //
  return DEDX_Median; // RETURN
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackVeloRClusterDEDXMedian::fillStream( std::ostream& s ) const {
  return s << "TRVELORCLUSTERDEDXMEDIAN";
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackVeloPhiClusterDEDXMedian::result_type LoKi::Particles::TrackVeloPhiClusterDEDXMedian::
                                                            operator()( LoKi::Particles::TrackVeloPhiClusterDEDXMedian::argument p ) const {
  //
  const double errVal      = -2;
  double       DEDX_Median = -1;
  //
  if ( !p ) {
    Error( "Argument  is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( !track ) {
    Error( "Track     is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  const double theta        = track->slopes().Theta();
  double       angle_factor = 1;
  if ( cos( theta ) != 0 ) { angle_factor = cos( theta ); }
  //
  auto         adc_med_fun = TrackVeloPhiClusterADCMedian();
  const double ADC_Median  = adc_med_fun( p );
  //
  DEDX_Median = ADC_Median * angle_factor;
  //
  return DEDX_Median; // RETURN
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackVeloPhiClusterDEDXMedian::fillStream( std::ostream& s ) const {
  return s << "TRVELOPHICLUSTERDEDXMEDIAN";
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Particles::TrackVeloClusterNumOverflows::TrackVeloClusterNumOverflows( const int nMax )
    : LoKi::AuxFunBase( std::tie( nMax ) ), m_nMax( nMax ) {}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackVeloClusterNumOverflows::result_type LoKi::Particles::TrackVeloClusterNumOverflows::
                                                           operator()( LoKi::Particles::TrackVeloClusterNumOverflows::argument p ) const {
  //
  const int errVal = -1;
  //
  if ( !p ) {
    Error( "Argument  is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( !track ) {
    Error( "Track     is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  unsigned int clSize     = 0;
  int          nOverflows = 0;
  //
  m_clusters = nullptr;
  //
  const std::vector<LHCb::LHCbID>& ids = ( track )->lhcbIDs();
  for ( std::vector<LHCb::LHCbID>::const_iterator it = ids.begin(); it != ids.end(); ++it ) {
    if ( it->isVelo() == true ) {
      if ( m_clusters == nullptr ) {
        // Veloclusters
        SmartIF<IDataProviderSvc>        ds( lokiSvc().getObject() );
        SmartDataPtr<LHCb::VeloClusters> data( ds, LHCb::VeloClusterLocation::Default );
        if ( !data ) {
          Warning( "No table at location " + LHCb::VeloClusterLocation::Default ).ignore();
          return errVal;
        }
        m_clusters = data;
      }
      const LHCb::VeloChannelID vcID = ( it )->veloID();
      LHCb::VeloCluster*        cluster = m_clusters->object( vcID );
      if ( !cluster ) {
        Error( "Cluster   is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
        return errVal; // RETURN
      }
      if ( 0 == cluster ) {
        Error( "ClusterID is invalid with ID " + Gaudi::Utils::toString( vcID ) ).ignore();
        return errVal; // RETURN
      }
      //
      clSize = cluster->size();
      //
      if ( (int)clSize <= m_nMax ) {
        for ( unsigned int stp = 0; stp < clSize; stp++ ) {
          if ( cluster->adcValue( stp ) == m_threshold ) { nOverflows++; }
        }
      }
    }
  } // lhcbids
  //
  return nOverflows; // RETURN
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackVeloClusterNumOverflows::fillStream( std::ostream& s ) const {
  s << "TRVELOCLUSTEROVERFLOWS(" << m_nMax;
  return s << ")";
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Particles::TrackVeloRClusterNumOverflows::TrackVeloRClusterNumOverflows( const int nMax )
    : LoKi::AuxFunBase( std::tie( nMax ) ), m_nMax( nMax ) {}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackVeloRClusterNumOverflows::result_type LoKi::Particles::TrackVeloRClusterNumOverflows::
                                                            operator()( LoKi::Particles::TrackVeloRClusterNumOverflows::argument p ) const {
  //
  const int errVal = -1;
  //
  if ( !p ) {
    Error( "Argument  is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( !track ) {
    Error( "Track     is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  unsigned int clSize     = 0;
  int          nOverflows = 0;
  //
  m_clusters = nullptr;
  //
  const std::vector<LHCb::LHCbID>& ids = ( track )->lhcbIDs();
  for ( std::vector<LHCb::LHCbID>::const_iterator it = ids.begin(); it != ids.end(); ++it ) {
    if ( it->isVeloR() == true ) {
      if ( m_clusters == nullptr ) {
        // Veloclusters
        SmartIF<IDataProviderSvc>        ds( lokiSvc().getObject() );
        SmartDataPtr<LHCb::VeloClusters> data( ds, LHCb::VeloClusterLocation::Default );
        if ( !data ) {
          Warning( "No table at location " + LHCb::VeloClusterLocation::Default ).ignore();
          return errVal;
        }
        m_clusters = data;
      }
      const LHCb::VeloChannelID vcID = ( it )->veloID();
      LHCb::VeloCluster*        cluster = m_clusters->object( vcID );
      if ( !cluster ) {
        Error( "Cluster   is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
        return errVal; // RETURN
      }
      if ( 0 == cluster ) {
        Error( "ClusterID is invalid with ID " + Gaudi::Utils::toString( vcID ) ).ignore();
        return errVal; // RETURN
      }
      //
      clSize = cluster->size();
      //
      if ( (int)clSize <= m_nMax ) {
        for ( unsigned int stp = 0; stp < clSize; stp++ ) {
          if ( cluster->adcValue( stp ) == m_threshold ) { nOverflows++; }
        }
      }
    }
  } // lhcbids
  //
  return nOverflows; // RETURN
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackVeloRClusterNumOverflows::fillStream( std::ostream& s ) const {
  s << "TRVELORCLUSTEROVERFLOWS(" << m_nMax;
  return s << ")";
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Particles::TrackVeloPhiClusterNumOverflows::TrackVeloPhiClusterNumOverflows( const int nMax )
    : LoKi::AuxFunBase( std::tie( nMax ) ), m_nMax( nMax ) {}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackVeloPhiClusterNumOverflows::result_type LoKi::Particles::TrackVeloPhiClusterNumOverflows::
                                                              operator()( LoKi::Particles::TrackVeloPhiClusterNumOverflows::argument p ) const {
  //
  const int errVal = -1;
  //
  if ( !p ) {
    Error( "Argument  is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( !track ) {
    Error( "Track     is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  unsigned int clSize     = 0;
  int          nOverflows = 0;
  //
  m_clusters = nullptr;
  //
  const std::vector<LHCb::LHCbID>& ids = ( track )->lhcbIDs();
  for ( std::vector<LHCb::LHCbID>::const_iterator it = ids.begin(); it != ids.end(); ++it ) {
    if ( it->isVeloPhi() == true ) {
      if ( m_clusters == nullptr ) {
        // Veloclusters
        SmartIF<IDataProviderSvc>        ds( lokiSvc().getObject() );
        SmartDataPtr<LHCb::VeloClusters> data( ds, LHCb::VeloClusterLocation::Default );
        if ( !data ) {
          Warning( "No table at location " + LHCb::VeloClusterLocation::Default ).ignore();
          return errVal;
        }
        m_clusters = data;
      }
      const LHCb::VeloChannelID vcID = ( it )->veloID();
      LHCb::VeloCluster*        cluster = m_clusters->object( vcID );
      if ( !cluster ) {
        Error( "Cluster   is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
        return errVal; // RETURN
      }
      if ( 0 == cluster ) {
        Error( "ClusterID is invalid with ID " + Gaudi::Utils::toString( vcID ) ).ignore();
        return errVal; // RETURN
      }
      //
      clSize = cluster->size();
      //
      if ( (int)clSize <= m_nMax ) {
        for ( unsigned int stp = 0; stp < clSize; stp++ ) {
          if ( cluster->adcValue( stp ) == m_threshold ) { nOverflows++; }
        }
      }
    }
  } // lhcbids
  //
  return nOverflows; // RETURN
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackVeloPhiClusterNumOverflows::fillStream( std::ostream& s ) const {
  s << "TRVELOPHICLUSTEROVERFLOWS(" << m_nMax;
  return s << ")";
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackFracVeloClusterOverflows::TrackFracVeloClusterOverflows( const int nMax )
    : LoKi::AuxFunBase( std::tie( nMax ) ), m_nMax( nMax ) {}
// ============================================================================
LoKi::Particles::TrackFracVeloClusterOverflows::result_type LoKi::Particles::TrackFracVeloClusterOverflows::
                                                            operator()( LoKi::Particles::TrackFracVeloClusterOverflows::argument p ) const {
  //
  const int errVal = -1;
  //
  if ( !p ) {
    Error( "Argument  is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( !track ) {
    Error( "Track     is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  unsigned int clSize               = 0;
  int          nOverflows           = 0;
  int          nClusters            = 0;
  double       nOverflowsPerCluster = -0.5;
  //
  m_clusters = nullptr;
  //
  const std::vector<LHCb::LHCbID>& ids = ( track )->lhcbIDs();
  for ( std::vector<LHCb::LHCbID>::const_iterator it = ids.begin(); it != ids.end(); ++it ) {
    if ( it->isVelo() == true ) {
      if ( m_clusters == nullptr ) {
        // Veloclusters
        SmartIF<IDataProviderSvc>        ds( lokiSvc().getObject() );
        SmartDataPtr<LHCb::VeloClusters> data( ds, LHCb::VeloClusterLocation::Default );
        if ( !data ) {
          Warning( "No table at location " + LHCb::VeloClusterLocation::Default ).ignore();
          return errVal;
        }
        m_clusters = data;
      }
      const LHCb::VeloChannelID vcID = ( it )->veloID();
      LHCb::VeloCluster*        cluster = m_clusters->object( vcID );
      if ( !cluster ) {
        Error( "Cluster   is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
        return errVal; // RETURN
      }
      if ( 0 == cluster ) {
        Error( "ClusterID is invalid with ID " + Gaudi::Utils::toString( vcID ) ).ignore();
        return errVal; // RETURN
      }
      //
      clSize = cluster->size();
      //
      if ( (int)clSize <= m_nMax ) {
        nClusters++;
        for ( unsigned int stp = 0; stp < clSize; stp++ ) {
          if ( cluster->adcValue( stp ) == m_threshold ) { nOverflows++; }
        }
      }
    }
  } // lhcbids
  if ( nClusters == 0 ) {
    return nOverflowsPerCluster; // RETURN
  }
  nOverflowsPerCluster = (double)nOverflows / nClusters;
  //
  return nOverflowsPerCluster; // RETURN
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackFracVeloClusterOverflows::fillStream( std::ostream& s ) const {
  s << "TRVELOCLUSTEROVERFLOWFRAC(" << m_nMax;
  return s << ")";
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackNumTTClusters::result_type LoKi::Particles::TrackNumTTClusters::
                                                 operator()( LoKi::Particles::TrackNumTTClusters::argument p ) const {
  //
  const int errVal = -1;
  //
  if ( !p ) {
    Error( "Argument is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( !track ) {
    Error( "Track    is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  int nClustersOnTrack = 0;
  //
  const std::vector<LHCb::LHCbID>& ids = ( track )->lhcbIDs();
  for ( std::vector<LHCb::LHCbID>::const_iterator it = ids.begin(); it != ids.end(); ++it ) {
    if ( it->isTT() == true ) { nClustersOnTrack++; }
  } // lhcbids
  //
  return nClustersOnTrack; // RETURN
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackNumTTClusters::fillStream( std::ostream& s ) const { return s << "TRTTCLUSTERS"; }
// ============================================================================
// constructor with no parameters
// ============================================================================
LoKi::Particles::TrackTTCosTheta::TrackTTCosTheta()
    : LoKi::AuxFunBase{std::tie()}, LoKi::BasicFunctors<const LHCb::Particle*>::Function(), m_extrapolator( nullptr ) {
  // Get the extrapolator (TrackStateProvider)
  const LoKi::ILoKiSvc* svc0 = lokiSvc();
  Assert( nullptr != svc0, "Can't get LoKi service!" );
  LoKi::ILoKiSvc* svc = const_cast<LoKi::ILoKiSvc*>( svc0 );
  // a) First try to use context
  // get the context service:
  SmartIF<IAlgContextSvc> cntx( svc );
  if ( cntx ) {
    // get 'simple' algorithm from the context:
    GaudiAlgorithm* alg = Gaudi::Utils::getGaudiAlg( cntx );
    if ( 0 != alg ) {
      // get the tool from the algorithm
      const ITrackStateProvider* tool = alg->tool<ITrackStateProvider>( m_tool_name, "Extrapolator", alg );
      if ( nullptr != tool ) { m_extrapolator = tool; }
    }
    //
  }
  // b) try to use Tool Service
  if ( !m_extrapolator ) {
    SmartIF<IToolSvc> tsvc( svc );
    if ( tsvc ) {
      const ITrackStateProvider* tool = nullptr;
      StatusCode                 sc   = tsvc->retrieveTool( m_tool_name, tool );
      if ( sc.isSuccess() && 0 != tool ) { m_extrapolator = tool; }
    }
  }
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackTTCosTheta::result_type LoKi::Particles::TrackTTCosTheta::
                                              operator()( LoKi::Particles::TrackTTCosTheta::argument p ) const {
  //
  const int errVal = -1;
  //
  if ( !p ) {
    Error( "Argument is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( !track ) {
    Error( "Track    is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  const LHCb::State* state;
  LHCb::State        extra_state;
  if ( track->hasStateAt( LHCb::State::AtTT ) ) {
    state = track->stateAt( LHCb::State::AtTT );
  } else {
    StatusCode sc = StatusCode::FAILURE;
    if ( m_extrapolator && track->hasTT() ) { sc = m_extrapolator->state( extra_state, *track, m_tt_z_position, 0 ); }
    if ( sc == StatusCode::SUCCESS ) {
      state = &extra_state;
    } else {
      state = &track->closestState( m_tt_z_position );
    }
  }
  const double tt_theta     = state->slopes().Theta();
  double       tt_cos_theta = cos( tt_theta );
  return tt_cos_theta; // RETURN
}
// ============================================================================
// mandatory: virtual destructor
// ============================================================================
LoKi::Particles::TrackTTCosTheta::~TrackTTCosTheta() {
  if ( m_extrapolator && !gaudi() ) { m_extrapolator.reset(); }
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackTTCosTheta::fillStream( std::ostream& s ) const { return s << "TRTTCOSTHETA"; }
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackTTClusterADCMedian::result_type LoKi::Particles::TrackTTClusterADCMedian::
                                                      operator()( LoKi::Particles::TrackTTClusterADCMedian::argument p ) const {
  //
  const double errVal     = -2;
  float        ADC_Median = -1;
  //
  if ( !p ) {
    Error( "Argument  is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( !track ) {
    Error( "Track     is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  int                nClustersOnTrack = 0;
  std::vector<float> ADC_vec;
  //
  m_clusters = nullptr;
  //
  const std::vector<LHCb::LHCbID>& ids = ( track )->lhcbIDs();
  for ( std::vector<LHCb::LHCbID>::const_iterator it = ids.begin(); it != ids.end(); ++it ) {
    if ( it->isTT() == true ) {
      const LHCb::STChannelID stID = ( it )->stID();
      if ( m_clusters == nullptr ) {
        // TTclusters
        SmartIF<IDataProviderSvc>      ds( lokiSvc().getObject() );
        SmartDataPtr<LHCb::STClusters> data( ds, LHCb::STClusterLocation::TTClusters );
        if ( !data ) {
          Warning( "No table at location " + LHCb::STClusterLocation::TTClusters ).ignore();
          return errVal;
        }
        m_clusters = data;
      }
      LHCb::STCluster* cluster = m_clusters->object( stID );
      if ( !cluster ) {
        Error( "Cluster   is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
        return errVal; // RETURN
      }
      if ( 0 == cluster ) {
        Error( "ClusterID is invalid with ID " + Gaudi::Utils::toString( stID ) ).ignore();
        return errVal; // RETURN
      }
      //
      const float ADC = cluster->totalCharge();
      ADC_vec.push_back( ADC );
      nClustersOnTrack++;
    }
  } // lhcbids
  //
  if ( nClustersOnTrack == 0 ) {
    return ADC_Median; // RETURN
  }
  //
  std::sort( ADC_vec.begin(), ADC_vec.end() );
  int n = int( nClustersOnTrack / 2 );
  if ( nClustersOnTrack % 2 == 1 ) // odd
  {
    ADC_Median = ADC_vec[n];
  } else // even
  {
    ADC_Median = 0.5 * ( ADC_vec[n] + ADC_vec[n - 1] );
  }
  return ADC_Median; // RETURN
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackTTClusterADCMedian::fillStream( std::ostream& s ) const {
  return s << "TRTTCLUSTERADCMEDIAN";
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackTTClusterDEDXMedian::result_type LoKi::Particles::TrackTTClusterDEDXMedian::
                                                       operator()( LoKi::Particles::TrackTTClusterDEDXMedian::argument p ) const {
  auto         adc_med_fun = TrackTTClusterADCMedian();
  const double ADC_Median  = adc_med_fun( p );
  if ( ADC_Median < 0 ) { return ADC_Median; }
  //
  auto         angle_fun    = TrackTTCosTheta();
  const double angle_factor = angle_fun( p );
  //
  const double DEDX_Median = ADC_Median * angle_factor;
  return DEDX_Median; // RETURN
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackTTClusterDEDXMedian::fillStream( std::ostream& s ) const {
  return s << "TRTTCLUSTERDEDXMEDIAN";
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Particles::TrackTTClusterNumOverflows::TrackTTClusterNumOverflows( const int nMax )
    : LoKi::AuxFunBase( std::tie( nMax ) ), m_nMax( nMax ) {}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackTTClusterNumOverflows::result_type LoKi::Particles::TrackTTClusterNumOverflows::
                                                         operator()( LoKi::Particles::TrackTTClusterNumOverflows::argument p ) const {
  //
  const double errVal = -1;
  //
  if ( !p ) {
    Error( "Argument  is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( !track ) {
    Error( "Track     is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  m_clusters = nullptr;
  //
  unsigned int clSize     = 0;
  int          nOverflows = 0;
  //
  const std::vector<LHCb::LHCbID>& ids = ( track )->lhcbIDs();
  for ( std::vector<LHCb::LHCbID>::const_iterator it = ids.begin(); it != ids.end(); ++it ) {
    if ( it->isTT() == true ) {
      const LHCb::STChannelID stID = ( it )->stID();
      if ( m_clusters == nullptr ) {
        // TTclusters
        SmartIF<IDataProviderSvc>      ds( lokiSvc().getObject() );
        SmartDataPtr<LHCb::STClusters> data( ds, LHCb::STClusterLocation::TTClusters );
        if ( !data ) {
          Warning( "No table at location " + LHCb::STClusterLocation::TTClusters ).ignore();
          return errVal;
        }
        m_clusters = data;
      }
      LHCb::STCluster* cluster = m_clusters->object( stID );
      if ( !cluster ) {
        Error( "Cluster   is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
        return errVal; // RETURN
      }
      if ( 0 == cluster ) {
        Error( "ClusterID is invalid with ID " + Gaudi::Utils::toString( stID ) ).ignore();
        return errVal; // RETURN
      }
      //
      clSize = cluster->size();
      //
      if ( (int)clSize <= m_nMax ) {
        for ( unsigned int stp = 0; stp < clSize; stp++ ) {
          if ( cluster->adcValue( stp ) == m_threshold ) { nOverflows++; }
        }
      }
    }
  }                  // lhcbids
  return nOverflows; // RETURN
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackTTClusterNumOverflows::fillStream( std::ostream& s ) const {
  s << "TRTTCLUSTEROVERFLOWS(" << m_nMax;
  return s << ")";
}
// ============================================================================
// constructor with no parameters
// ============================================================================
LoKi::Particles::TrackTCosTheta::TrackTCosTheta()
    : LoKi::AuxFunBase{std::tie()}, LoKi::BasicFunctors<const LHCb::Particle*>::Function(), m_extrapolator( nullptr ) {
  // Get the extrapolator (TrackStateProvider)
  const LoKi::ILoKiSvc* svc0 = lokiSvc();
  Assert( nullptr != svc0, "Can't get LoKi service!" );
  LoKi::ILoKiSvc* svc = const_cast<LoKi::ILoKiSvc*>( svc0 );
  // a) First try to use context
  // get the context service:
  SmartIF<IAlgContextSvc> cntx( svc );
  if ( cntx ) {
    // get 'simple' algorithm from the context:
    GaudiAlgorithm* alg = Gaudi::Utils::getGaudiAlg( cntx );
    if ( 0 != alg ) {
      // get the tool from the algorithm
      const ITrackStateProvider* tool = alg->tool<ITrackStateProvider>( m_tool_name, "Extrapolator", alg );
      if ( nullptr != tool ) { m_extrapolator = tool; }
    }
    //
  }
  // b) try to use Tool Service
  if ( !m_extrapolator ) {
    SmartIF<IToolSvc> tsvc( svc );
    if ( tsvc ) {
      const ITrackStateProvider* tool = nullptr;
      StatusCode                 sc   = tsvc->retrieveTool( m_tool_name, tool );
      if ( sc.isSuccess() && 0 != tool ) { m_extrapolator = tool; }
    }
  }
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackTCosTheta::result_type LoKi::Particles::TrackTCosTheta::
                                             operator()( LoKi::Particles::TrackTCosTheta::argument p ) const {
  //
  const int errVal = -1;
  //
  if ( !p ) {
    Error( "Argument is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( !track ) {
    Error( "Track    is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  const LHCb::State* state;
  LHCb::State        extra_state;
  if ( track->hasStateAt( LHCb::State::AtT ) ) {
    state = track->stateAt( LHCb::State::AtT );
  } else {
    StatusCode sc = StatusCode::FAILURE;
    if ( m_extrapolator && track->hasT() ) { sc = m_extrapolator->state( extra_state, *track, m_t_z_position, 0 ); }
    if ( sc == StatusCode::SUCCESS ) {
      state = &extra_state;
    } else {
      state = &track->closestState( m_t_z_position );
    }
  }
  const double t_theta     = state->slopes().Theta();
  double       t_cos_theta = cos( t_theta );
  return t_cos_theta; // RETURN
}
// ============================================================================
// mandatory: virtual destructor
// ============================================================================
LoKi::Particles::TrackTCosTheta::~TrackTCosTheta() {
  if ( m_extrapolator && !gaudi() ) { m_extrapolator.reset(); }
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackTCosTheta::fillStream( std::ostream& s ) const { return s << "TRTCOSTHETA"; }
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackNumITClusters::result_type LoKi::Particles::TrackNumITClusters::
                                                 operator()( LoKi::Particles::TrackNumITClusters::argument p ) const {
  //
  const int errVal = -1;
  //
  if ( !p ) {
    Error( "Argument is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( !track ) {
    Error( "Track    is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  int nClustersOnTrack = 0;
  //
  const std::vector<LHCb::LHCbID>& ids = ( track )->lhcbIDs();
  for ( std::vector<LHCb::LHCbID>::const_iterator it = ids.begin(); it != ids.end(); ++it ) {
    if ( it->isIT() == true ) { nClustersOnTrack++; }
  } // lhcbids
  //
  return nClustersOnTrack; // RETURN
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackNumITClusters::fillStream( std::ostream& s ) const { return s << "TRITCLUSTERS"; }
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackITClusterADCMedian::result_type LoKi::Particles::TrackITClusterADCMedian::
                                                      operator()( LoKi::Particles::TrackITClusterADCMedian::argument p ) const {
  //
  const float errVal     = -2;
  float       ADC_Median = -1;
  //
  if ( !p ) {
    Error( "Argument  is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( !track ) {
    Error( "Track     is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  int                nClustersOnTrack = 0;
  std::vector<float> ADC_vec;
  //
  m_clusters = nullptr;
  //
  const std::vector<LHCb::LHCbID>& ids = ( track )->lhcbIDs();
  for ( std::vector<LHCb::LHCbID>::const_iterator it = ids.begin(); it != ids.end(); ++it ) {
    if ( it->isIT() == true ) {
      const LHCb::STChannelID stID = ( it )->stID();
      if ( m_clusters == nullptr ) {
        // ITclusters
        SmartIF<IDataProviderSvc>      ds( lokiSvc().getObject() );
        SmartDataPtr<LHCb::STClusters> data( ds, LHCb::STClusterLocation::ITClusters );
        if ( !data ) {
          Warning( "No table at location " + LHCb::STClusterLocation::ITClusters ).ignore();
          return errVal;
        }
        m_clusters = data;
      }
      LHCb::STCluster* cluster = m_clusters->object( stID );
      if ( !cluster ) {
        Error( "Cluster   is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
        return errVal; // RETURN
      }
      if ( 0 == cluster ) {
        Error( "ClusterID is invalid with ID " + Gaudi::Utils::toString( stID ) ).ignore();
        return errVal; // RETURN
      }
      //
      const float ADC = cluster->totalCharge();
      ADC_vec.push_back( ADC );
      nClustersOnTrack++;
    }
  } // lhcbids
  //
  if ( nClustersOnTrack == 0 ) {
    return ADC_Median; // RETURN
  }
  //
  std::sort( ADC_vec.begin(), ADC_vec.end() );
  int n = int( nClustersOnTrack / 2 );
  if ( nClustersOnTrack % 2 == 1 ) // odd
  {
    ADC_Median = ADC_vec[n];
  } else // even
  {
    ADC_Median = 0.5 * ( ADC_vec[n] + ADC_vec[n - 1] );
  }
  return ADC_Median; // RETURN
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackITClusterADCMedian::fillStream( std::ostream& s ) const {
  return s << "TRITCLUSTERADCMEDIAN";
}
// ============================================================================
// constructor with no parameters
// ============================================================================
LoKi::Particles::TrackITClusterDEDXMedian::TrackITClusterDEDXMedian() : LoKi::AuxFunBase{std::tie()} {
  // Get detector
  const LoKi::ILoKiSvc* svc0 = lokiSvc();
  Assert( nullptr != svc0, "Can't get LoKi service!" );
  LoKi::ILoKiSvc* svc = const_cast<LoKi::ILoKiSvc*>( svc0 );
  // a) First try to use context
  // get the context service:
  SmartIF<IAlgContextSvc> cntx( svc );
  if ( cntx ) {
    // get 'simple' algorithm from the context:
    GaudiAlgorithm* alg = Gaudi::Utils::getGaudiAlg( cntx );
    if ( 0 != alg ) {
      // get the tool from the algorithm
      DeSTDetector* det = alg->getDet<DeSTDetector>( DeSTDetLocation::IT );
      ;
      if ( nullptr != det ) { m_ITdet = det; }
    }
    //
  }
  if ( !m_ITdet ) { Error( "DeSTDetector not found" ).ignore(); }
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackITClusterDEDXMedian::result_type LoKi::Particles::TrackITClusterDEDXMedian::
                                                       operator()( LoKi::Particles::TrackITClusterDEDXMedian::argument p ) const {
  //
  const float errVal            = -2;
  float       DEDX_Median       = -1;
  float       DEDX_thin_Median  = -1;
  float       DEDX_thick_Median = -1;
  //
  if ( !p ) {
    Error( "Argument  is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( !track ) {
    Error( "Track     is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  auto         angle_fun    = TrackTCosTheta();
  const double angle_factor = angle_fun( p );
  //
  int                nClustersOnTrack      = 0;
  int                nThinClustersOnTrack  = 0;
  int                nThickClustersOnTrack = 0;
  std::vector<float> DEDX_thin_vec;
  std::vector<float> DEDX_thick_vec;
  //
  m_clusters = nullptr;
  //
  const std::vector<LHCb::LHCbID>& ids = ( track )->lhcbIDs();
  for ( std::vector<LHCb::LHCbID>::const_iterator it = ids.begin(); it != ids.end(); ++it ) {
    if ( it->isIT() == true ) {
      const LHCb::STChannelID stID = ( it )->stID();
      if ( m_clusters == nullptr ) {
        // ITclusters
        SmartIF<IDataProviderSvc>      ds( lokiSvc().getObject() );
        SmartDataPtr<LHCb::STClusters> data( ds, LHCb::STClusterLocation::ITClusters );
        if ( !data ) {
          Warning( "No table at location " + LHCb::STClusterLocation::ITClusters ).ignore();
          return errVal;
        }
        m_clusters = data;
      }
      LHCb::STCluster* cluster = m_clusters->object( stID );
      if ( !cluster ) {
        Error( "Cluster   is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
        return errVal; // RETURN
      }
      LHCb::STChannelID it_channel_id = cluster->channelID();
      if ( m_ITdet == nullptr ) {
        Error( "DeSTDetector is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
        return errVal; // RETURN
      }
      DeSTSector* it_sector = m_ITdet->findSector( it_channel_id );
      if ( !it_sector ) {
        Error( "ITSector is invalid for channel with ID " + Gaudi::Utils::toString( it_channel_id ) ).ignore();
        return errVal; // RETURN
      }
      //
      const float  ADC          = cluster->totalCharge();
      const float  DEDX         = ADC * angle_factor;
      const double st_thickness = it_sector->thickness();
      if ( st_thickness < m_thres_thickness ) {
        // thin
        DEDX_thin_vec.push_back( DEDX );
        nThinClustersOnTrack++;
      } else {
        // thick
        DEDX_thick_vec.push_back( DEDX );
        nThickClustersOnTrack++;
      }
      nClustersOnTrack++;
    }
  } // lhcbids
  //
  if ( nClustersOnTrack == 0 ) {
    return DEDX_Median; // RETURN
  }
  // Get thin median
  if ( nThinClustersOnTrack != 0 ) {
    std::sort( DEDX_thin_vec.begin(), DEDX_thin_vec.end() );
    int n_thin = int( nThinClustersOnTrack / 2 );
    if ( nThinClustersOnTrack % 2 == 1 ) // odd
    {
      DEDX_thin_Median = DEDX_thin_vec[n_thin];
    } else // even
    {
      DEDX_thin_Median = 0.5 * ( DEDX_thin_vec[n_thin] + DEDX_thin_vec[n_thin - 1] );
    }

    // correct for thickness difference
    DEDX_thin_Median = DEDX_thin_Median * m_thin_scale + m_thin_shift;
  }
  // Get thick median
  if ( nThickClustersOnTrack != 0 ) {
    std::sort( DEDX_thick_vec.begin(), DEDX_thick_vec.end() );
    int n_thick = int( nThickClustersOnTrack / 2 );
    if ( nThickClustersOnTrack % 2 == 1 ) // odd
    {
      DEDX_thick_Median = DEDX_thick_vec[n_thick];
    } else // even
    {
      DEDX_thick_Median = 0.5 * ( DEDX_thick_vec[n_thick] + DEDX_thick_vec[n_thick - 1] );
    }
  }
  // compute weighted mean:
  DEDX_Median = ( nThinClustersOnTrack * DEDX_thin_Median + nThickClustersOnTrack * DEDX_thick_Median );
  DEDX_Median /= nClustersOnTrack;
  return DEDX_Median; // RETURN
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackITClusterDEDXMedian::fillStream( std::ostream& s ) const {
  return s << "TRITCLUSTERDEDXMEDIAN";
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Particles::TrackITClusterNumOverflows::TrackITClusterNumOverflows( const int nMax )
    : LoKi::AuxFunBase( std::tie( nMax ) ), m_nMax( nMax ) {}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackITClusterNumOverflows::result_type LoKi::Particles::TrackITClusterNumOverflows::
                                                         operator()( LoKi::Particles::TrackITClusterNumOverflows::argument p ) const {
  //
  const double errVal = -1;
  //
  if ( !p ) {
    Error( "Argument  is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( !track ) {
    Error( "Track     is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  m_clusters = nullptr;
  //
  unsigned int clSize     = 0;
  int          nOverflows = 0;
  //
  const std::vector<LHCb::LHCbID>& ids = ( track )->lhcbIDs();
  for ( std::vector<LHCb::LHCbID>::const_iterator it = ids.begin(); it != ids.end(); ++it ) {
    if ( it->isIT() == true ) {
      const LHCb::STChannelID stID = ( it )->stID();
      if ( m_clusters == nullptr ) {
        // ITclusters
        SmartIF<IDataProviderSvc>      ds( lokiSvc().getObject() );
        SmartDataPtr<LHCb::STClusters> data( ds, LHCb::STClusterLocation::ITClusters );
        if ( !data ) {
          Warning( "No table at location " + LHCb::STClusterLocation::ITClusters ).ignore();
          return errVal;
        }
        m_clusters = data;
      }
      LHCb::STCluster* cluster = m_clusters->object( stID );
      if ( !cluster ) {
        Error( "Cluster   is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
        return errVal; // RETURN
      }
      if ( 0 == cluster ) {
        Error( "ClusterID is invalid with ID " + Gaudi::Utils::toString( stID ) ).ignore();
        return errVal; // RETURN
      }
      //
      clSize = cluster->size();
      //
      if ( (int)clSize <= m_nMax ) {
        for ( unsigned int stp = 0; stp < clSize; stp++ ) {
          if ( cluster->adcValue( stp ) == m_threshold ) { nOverflows++; }
        }
      }
    }
  }                  // lhcbids
  return nOverflows; // RETURN
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackITClusterNumOverflows::fillStream( std::ostream& s ) const {
  s << "TRITCLUSTEROVERFLOWS(" << m_nMax;
  return s << ")";
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackNumOTTimes::result_type LoKi::Particles::TrackNumOTTimes::
                                              operator()( LoKi::Particles::TrackNumOTTimes::argument p ) const {
  //
  const int errVal = -1;
  //
  if ( !p ) {
    Error( "Argument is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( !track ) {
    Error( "Track    is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  int nTimesOnTrack = 0;
  //
  const std::vector<LHCb::LHCbID>& ids = ( track )->lhcbIDs();
  for ( std::vector<LHCb::LHCbID>::const_iterator it = ids.begin(); it != ids.end(); ++it ) {
    if ( it->isOT() == true ) { nTimesOnTrack++; }
  } // lhcbids
  //
  return nTimesOnTrack; // RETURN
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackNumOTTimes::fillStream( std::ostream& s ) const { return s << "TROTTIMES"; }
// ============================================================================
// constructor with no parameters
// ============================================================================
LoKi::Particles::TrackTime::TrackTime()
    : LoKi::AuxFunBase{std::tie()}, LoKi::BasicFunctors<const LHCb::Particle*>::Function(), m_trackT0Tool( nullptr ) {
  // Get the TrackT0Tool
  const LoKi::ILoKiSvc* svc0 = lokiSvc();
  Assert( nullptr != svc0, "Can't get LoKi service!" );
  LoKi::ILoKiSvc* svc = const_cast<LoKi::ILoKiSvc*>( svc0 );
  // a) First try to use context
  // get the context service:
  SmartIF<IAlgContextSvc> cntx( svc );
  if ( cntx ) {
    // get 'simple' algorithm from the context:
    GaudiAlgorithm* alg = Gaudi::Utils::getGaudiAlg( cntx );
    if ( 0 != alg ) {
      // get the tool from the algorithm
      const ITrackT0Tool* tool = alg->tool<ITrackT0Tool>( m_tool_name, "Track_time", alg );
      if ( nullptr != tool ) { m_trackT0Tool = tool; }
    }
    //
  }
  // b) try to use Tool Service
  if ( !m_trackT0Tool ) {
    SmartIF<IToolSvc> tsvc( svc );
    if ( tsvc ) {
      const ITrackT0Tool* tool = nullptr;
      StatusCode          sc   = tsvc->retrieveTool( m_tool_name, tool );
      if ( sc.isSuccess() && 0 != tool ) { m_trackT0Tool = tool; }
    }
  }
}
// ============================================================================
// mandatory: virtual destructor
// ============================================================================
LoKi::Particles::TrackTime::~TrackTime() {
  if ( m_trackT0Tool && !gaudi() ) { m_trackT0Tool.reset(); }
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackTime::result_type LoKi::Particles::TrackTime::
                                        operator()( LoKi::Particles::TrackTime::argument p ) const {
  //
  const float errVal = -2;
  //
  if ( !p ) {
    Error( "Argument  is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( !track ) {
    Error( "Track     is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  ITrackT0Tool::ValueWithError track_t0   = m_trackT0Tool->trackT0( *track );
  double                       track_time = track_t0.value();
  if ( std::isnan( track_time ) ) { track_time = -1000; }
  return track_time; // RETURN
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackTime::fillStream( std::ostream& s ) const { return s << "TRTIME"; }
// ============================================================================

// ============================================================================
// The END
// ============================================================================
