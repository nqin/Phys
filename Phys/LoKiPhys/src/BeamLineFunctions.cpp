/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// STD & STD
// ============================================================================
#include <cmath>
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/BeamLineFunctions.h"
#include "LoKi/Constants.h"
#include "LoKi/ILoKiSvc.h"
// ============================================================================
/** @file
 *  Collection of "beam-line"-related functors
 *
 *  This file is part of LoKi project:
 *   ``C++ ToolKit for Smart and Friendly Physics Analysis''
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
 *  @date   2011-03-10
 */
// ============================================================================
// Constructor from bound
// ============================================================================
LoKi::Vertices::BeamSpotRho::BeamSpotRho( const double bound )
    : LoKi::AuxFunBase( std::tie( bound ) )
    , LoKi::BeamSpot( bound )
    , LoKi::BasicFunctors<const LHCb::VertexBase*>::Function() {}
// ============================================================================
// Constructor from bound & condname
// ============================================================================
LoKi::Vertices::BeamSpotRho::BeamSpotRho( const double bound, const std::string& condname )
    : LoKi::AuxFunBase( std::tie( bound, condname ) )
    , LoKi::BeamSpot( bound, condname )
    , LoKi::BasicFunctors<const LHCb::VertexBase*>::Function() {}
// ============================================================================
// MANDATORY: virtual destructor
// ============================================================================
LoKi::Vertices::BeamSpotRho::~BeamSpotRho() {}
// ============================================================================
// MANDATOTY: clone method ("virtual constructor")
// ============================================================================
LoKi::Vertices::BeamSpotRho* LoKi::Vertices::BeamSpotRho::clone() const {
  return new LoKi::Vertices::BeamSpotRho( *this );
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Vertices::BeamSpotRho::result_type LoKi::Vertices::BeamSpotRho::
                                         operator()( LoKi::Vertices::BeamSpotRho::argument v ) const {
  if ( 0 == v ) {
    Error( "invalid argument, return InvalidDistance" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return LoKi::Constants::InvalidDistance;
  }
  //
  // if the velo is opened ( x resolver position > m_resolverBound ) return -1.
  //
  if ( !closed() ) { return -1; } // RETURN
  //
  // return radial distance from vertex to beam spot
  //
  const double x_diff = v->position().x() - x();
  const double y_diff = v->position().y() - y();
  //
  return std::sqrt( x_diff * x_diff + y_diff * y_diff );
}
// ============================================================================
// OPTIONAL: nice printout
// ============================================================================
std::ostream& LoKi::Vertices::BeamSpotRho::fillStream( std::ostream& s ) const {
  s << "VX_BEAMSPOTRHO(" << resolverBound() << "";
  s << ",'" << condName() << "'";
  return s << ")";
}
// ============================================================================
// The END
// ============================================================================

// ============================================================================
// Default constructor
// ============================================================================
LoKi::Particles::DistanceToBeamLine::DistanceToBeamLine()
    : LoKi::AuxFunBase( std::tie() )
    , LoKi::BeamSpot( 0 )
    , LoKi::BasicFunctors<const LHCb::Particle*>::Function()
    , m_cut{Cuts::INTREE( Cuts::HASTRACK )} {}
// ============================================================================
// MANDATORY: virtual destructor
// ============================================================================
LoKi::Particles::DistanceToBeamLine::~DistanceToBeamLine() {}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Particles::DistanceToBeamLine* LoKi::Particles::DistanceToBeamLine::clone() const {
  return new LoKi::Particles::DistanceToBeamLine( *this );
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::DistanceToBeamLine::result_type LoKi::Particles::DistanceToBeamLine::
                                                 operator()( LoKi::Particles::DistanceToBeamLine::argument p ) const {
  if ( 0 == p ) {
    Error( "invalid argument, return InvalidDistance" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return LoKi::Constants::InvalidDistance;
  }
  if ( !m_cut.fun( p ) ) return 0.0;

  // return signed distance between particle trajectory and beam line
  const auto tx = p->momentum().Px() /* / p->momentum().Pz() */;
  const auto ty = p->momentum().Py() /*/ p->momentum().Pz() */;
  const auto dx = p->referencePoint().x() - x();
  const auto dy = p->referencePoint().y() - y();
  return ( dx * ty - dy * tx ) / std::sqrt( tx * tx + ty * ty );
}
// ============================================================================
// OPTIONAL: nice printout
// ============================================================================
std::ostream& LoKi::Particles::DistanceToBeamLine::fillStream( std::ostream& s ) const { return s << "BEAMLINEDOCA()"; }
// ============================================================================
// The END
// ============================================================================
