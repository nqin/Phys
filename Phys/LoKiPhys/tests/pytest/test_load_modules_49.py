#!/usr/bin/env python
###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Test loading LoKi Functors from Particles49
'''
__author__ = 'Hendrik Jage hendrik.jage@cern.ch'


def test_load_TRVELOCLUSTERS():
    from LoKiPhys.decorators import TRVELOCLUSTERS


def test_load_TRVELORCLUSTERS():
    from LoKiPhys.decorators import TRVELORCLUSTERS


def test_load_TRVELOPHICLUSTERS():
    from LoKiPhys.decorators import TRVELOPHICLUSTERS


def test_load_TRVELOCLUSTERADC():
    from LoKiPhys.decorators import TRVELOCLUSTERADC


def test_load_TRVELOCLUSTERDEDX():
    from LoKiPhys.decorators import TRVELOCLUSTERDEDX


def test_load_TRVELOCLUSTERSTRIPS():
    from LoKiPhys.decorators import TRVELOCLUSTERSTRIPS


def test_load_TRVELOCLUSTEROVERFLOWS():
    from LoKiPhys.decorators import TRVELOCLUSTEROVERFLOWS


def test_load_TRVELORCLUSTEROVERFLOWS():
    from LoKiPhys.decorators import TRVELORCLUSTEROVERFLOWS


def test_load_TRVELOPHICLUSTEROVERFLOWS():
    from LoKiPhys.decorators import TRVELOPHICLUSTEROVERFLOWS


def test_load_TRVELOCLUSTEROVERFLOWFRAC():
    from LoKiPhys.decorators import TRVELOCLUSTEROVERFLOWFRAC


def test_load_TRVELOCLUSTERADCMEDIAN():
    from LoKiPhys.decorators import TRVELOCLUSTERADCMEDIAN


def test_load_TRVELORCLUSTERADCMEDIAN():
    from LoKiPhys.decorators import TRVELORCLUSTERADCMEDIAN


def test_load_TRVELOPHICLUSTERADCMEDIAN():
    from LoKiPhys.decorators import TRVELOPHICLUSTERADCMEDIAN


def test_load_TRVELOCLUSTERDEDXMEDIAN():
    from LoKiPhys.decorators import TRVELOCLUSTERDEDXMEDIAN


def test_load_TRVELORCLUSTERDEDXMEDIAN():
    from LoKiPhys.decorators import TRVELORCLUSTERDEDXMEDIAN


def test_load_TRVELOPHICLUSTERDEDXMEDIAN():
    from LoKiPhys.decorators import TRVELOPHICLUSTERDEDXMEDIAN


def test_load_TRTTCLUSTERS():
    from LoKiPhys.decorators import TRTTCLUSTERS


def test_load_TRTTCOSTHETA():
    from LoKiPhys.decorators import TRTTCOSTHETA


def test_load_TRTTCLUSTERADCMEDIAN():
    from LoKiPhys.decorators import TRTTCLUSTERADCMEDIAN


def test_load_TRTTCLUSTERDEDXMEDIAN():
    from LoKiPhys.decorators import TRTTCLUSTERDEDXMEDIAN


def test_load_TRTTCLUSTEROVERFLOWS():
    from LoKiPhys.decorators import TRTTCLUSTEROVERFLOWS


def test_load_TRTCOSTHETA():
    from LoKiPhys.decorators import TRTCOSTHETA


def test_load_TRITCLUSTERS():
    from LoKiPhys.decorators import TRITCLUSTERS


def test_load_TRITCLUSTERADCMEDIAN():
    from LoKiPhys.decorators import TRITCLUSTERADCMEDIAN


def test_load_TRITCLUSTERDEDXMEDIAN():
    from LoKiPhys.decorators import TRITCLUSTERDEDXMEDIAN


def test_load_TRITCLUSTEROVERFLOWS():
    from LoKiPhys.decorators import TRITCLUSTEROVERFLOWS


def test_load_TROTTIMES():
    from LoKiPhys.decorators import TROTTIMES


def test_load_TRTIME():
    from LoKiPhys.decorators import TRTIME
