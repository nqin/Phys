/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef COMPONENT_MVAMANAGER_H
#define COMPONENT_MVAMANAGER_H 1

// Include files
#include <map>
#include <string>
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
// from DaVinci
#include "Kernel/IParticleDictTool.h"

/** @class MVAManager MVAManager.h component/MVAManager.h
 *  This is a helper class, which bundles all components needed
 *  to setup a tool chain to create an MVA classifier
 *
 *  @author Sebastian Neubert
 *  @date   2013-08-06
 */
class MVAManager : public GaudiAlgorithm {
public:
  /// Standard constructor
  MVAManager( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~MVAManager(); ///< Destructor

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

protected:
  std::string        m_dictname;
  IParticleDictTool* m_dictofFunctors; ///< collecting several LoKi functors into one dictionary
  std::string        m_transformname;
  IParticleDictTool* m_mva; ///< DictTransform performing the MVA

private:
};
#endif // COMPONENT_MVAMANAGER_H
