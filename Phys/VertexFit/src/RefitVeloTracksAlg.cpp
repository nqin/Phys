/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
// Implementation file for class : RefitVeloTracksAlg
//
// 2019-10-14 : Michael Alexander
//-----------------------------------------------------------------------------

// local
#include "RefitVeloTracksAlg.h"

#include <Kernel/IDistanceCalculator.h>
#include <LoKi/PhysExtract.h>
#include <TrackInterfaces/ITrackStateInit.h>

using LHCb::Particle;
using LHCb::Track;

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( RefitVeloTracksAlg )

//=============================================================================

//=============================================================================
// Initialization
//=============================================================================
StatusCode RefitVeloTracksAlg::initialize() {
  const StatusCode sc = GaudiAlgorithm::initialize();
  if ( sc.isFailure() ) return sc;

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;

  m_veloFitter.initialize( "TrackStateInitTool/" + m_stateInitToolName, this )
      .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  m_veloFitter.retrieve().ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

  return sc;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode RefitVeloTracksAlg::execute() {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;

  setFilterPassed( true );

  Track::ConstVector tracks;
  // Get tracks from given locations.
  for ( const auto& loc : m_tracksLocations ) {
    Track::Range _tracks = getIfExists<Track::Range>( loc );
    std::copy_if( _tracks.begin(), _tracks.end(), std::back_inserter( tracks ),
                  []( auto* track ) { return bool( track ); } );
  }
  // Get tracks from particles at given locations.
  for ( const auto& loc : m_partsLocations ) {
    Particle::Range parts = getIfExists<Particle::Range>( loc );
    if ( parts.empty() ) continue;
    for ( const auto* part : parts ) LoKi::Extract::tracks( part, std::back_inserter( tracks ) );
  }
  counter( "N. input tracks" ) += tracks.size();

  Track::Container* veloTracks = new Track::Container();
  for ( const auto* track : tracks ) {
    if ( !track->hasVelo() ) continue;
    Track* veloTrack = new Track();
    for ( const auto& id : track->lhcbIDs() )
      if ( id.isVelo() ) veloTrack->addToLhcbIDs( id );
    // Refit track as a VELO track.
    StatusCode fitresult = m_veloFitter->fit( *veloTrack );
    if ( fitresult.isFailure() ) {
      counter( "N. failed fits" )++;
      delete veloTrack;
      continue;
    }
    veloTracks->insert( veloTrack );
    veloTrack->setHistory( LHCb::Track::PatFastVelo );
    veloTrack->setType( LHCb::Track::Velo );
  }
  put( veloTracks, m_veloTracksLocation );

  return StatusCode::SUCCESS;
}
