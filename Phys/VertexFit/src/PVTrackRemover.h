/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PVTRACKREMOVER_H
#define PVTRACKREMOVER_H 1

// Include files
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IPVReFitter.h"

struct IPVOfflineTool;

/** @class PVTrackRemover PVTrackRemover.h
 *
 *  Remove weighted contribution of tracks from an LHCb::Particle
 *  to a given LHCb::RecVertex.
 *  This class is <b>temporary</b> and for testing purposes. The reFit
 *  method makes no sense and is not implemented.
 *
 *  @author Juan Palacios
 *  @date   2010-12-07
 */
class PVTrackRemover : public GaudiTool, virtual public IPVReFitter {

public:
  /// Standard constructor
  PVTrackRemover( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override;

  StatusCode reFit( LHCb::VertexBase* PV ) const override;

  StatusCode remove( const LHCb::Particle* particle, LHCb::VertexBase* referencePV ) const override;

private:
  std::string     m_pvToolType;
  IPVOfflineTool* m_pvTool = nullptr;
};
#endif // PVTRACKREMOVER_H
