/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
// Implementation file for class : RecalcLSAdaptPV3DFitterWeightsAlg
//
// 2019-10-14 : Michael Alexander
//-----------------------------------------------------------------------------

// local
#include "RecalcLSAdaptPV3DFitterWeightsAlg.h"

using LHCb::RecVertex;
using LHCb::Track;

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( RecalcLSAdaptPV3DFitterWeightsAlg )

//=============================================================================

namespace {
  constexpr const double s_myZero = 1E-12; // myzero=1E-12 small number

  /// This is copied from LSAdaptPV3DFitter
  Gaudi::XYZVector impactParameterVector( const Gaudi::XYZPoint& vertex, const Gaudi::XYZPoint& point,
                                          const Gaudi::XYZVector& direction ) {
    auto udir     = direction.unit();
    auto distance = point - vertex;
    return udir.Cross( distance.Cross( udir ) );
  }
} // namespace

//=============================================================================
// Main execution
//=============================================================================
StatusCode RecalcLSAdaptPV3DFitterWeightsAlg::execute() {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;

  setFilterPassed( true );

  Track::ConstVector tracks;
  // Get tracks from given locations.
  for ( const auto& loc : m_tracksLocations ) {
    Track::Range _tracks = getIfExists<Track::Range>( loc );
    if ( !_tracks.empty() ) tracks.insert( tracks.end(), _tracks.begin(), _tracks.end() );
  }
  counter( "N. input tracks" ) += tracks.size();

  LHCb::RecVertices* vertices = getIfExists<LHCb::RecVertices>( m_pvsLocation );
  if ( !vertices ) return StatusCode::SUCCESS;
  counter( "N. vertices" ) += vertices->size();
  // Extract PVs.
  LHCb::RecVertex::Vector pvs;
  std::copy_if( vertices->begin(), vertices->end(), std::back_inserter( pvs ),
                []( auto* pv ) { return pv->isPrimary(); } );
  counter( "N. pvs" ) += pvs.size();

  // Clear existing tracks if requested.
  if ( m_clearTracks ) {
    for ( auto* pv : pvs ) pv->clearTracks();
  }

  // Find the PV with the largest weight for this track and add the track to it.
  for ( const auto* track : tracks ) {
    double           maxweight = 0.;
    LHCb::RecVertex* bpv       = nullptr;
    for ( auto* pv : pvs ) {
      double weight = trackWeight( *track, pv->position() );
      if ( weight > maxweight ) {
        maxweight = weight;
        bpv       = pv;
      }
    }
    counter( "weights" ) += maxweight;
    if ( bpv ) bpv->addToTracks( track, maxweight );
  }

  return StatusCode::SUCCESS;
}

/// Get the track weight as calculated by LSAdaptPV3DFitter.
double RecalcLSAdaptPV3DFitterWeightsAlg::trackWeight( const LHCb::Track& track, const Gaudi::XYZPoint& seed ) const {
  const LHCb::State&             state    = track.firstState();
  auto                           unitVect = state.slopes().Unit();
  auto                           vd0      = impactParameterVector( seed, state.position(), unitVect );
  double                         d0       = std::sqrt( vd0.Mag2() );
  auto                           vd0Unit  = vd0.unit();
  ROOT::Math::SVector<double, 2> xyVec( vd0Unit.x(), vd0Unit.y() );
  double                         err2d0 = Similarity( state.covariance().Sub<Gaudi::SymMatrix2x2>( 0, 0 ), xyVec );
  double                         chi2   = 0.;
  if ( err2d0 > s_myZero ) {
    chi2 = ( d0 * d0 ) / err2d0;
  } else {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "trackWeight: pvTrack error is too small for computation" << endmsg;
    chi2 = d0 / s_myZero;
  }
  double weight = std::max( 1. - chi2 / m_ipChi2Max, 0. );
  weight        = weight * weight;
  if ( weight < m_minTrackWeight ) weight = 0.;
  if ( msgLevel( MSG::DEBUG ) ) debug() << "trackWeight: calculated weight " << weight << endmsg;
  return weight;
}
