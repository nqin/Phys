/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef L0DECREPORTSMAKER_H
#define L0DECREPORTSMAKER_H 1

// Include files
#include <string>
// from Gaudi
#include "Event/HltObjectSummary.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/VectorMap.h"

namespace LHCb {
  class L0MuonCandidate;
  class L0CaloCandidate;
} // namespace LHCb

/** @class L0DecReportsMaker L0DecReportsMaker.h
 *
 *  @author Tomasz Skwarnicki
 *  @date   2010-06-23
 *
 *  Algorithm to translate L0DO report into HltDecReport
 *
 */
class L0DecReportsMaker : public GaudiAlgorithm {

public:
  /// Standard constructor
  L0DecReportsMaker( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override; ///< Algorithm execution

private:
  // ----------------------- data members

  /// location of input L0 DU Report
  StringProperty m_inputL0DUReportLocation;

  /// location of output Hlt Summary
  StringProperty m_outputHltDecReportsLocation;
};

#endif // L0SELREPORTSMAKER_H
