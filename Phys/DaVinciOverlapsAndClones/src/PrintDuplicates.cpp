/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// local
#include "PrintDuplicates.h"

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PrintDuplicates )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrintDuplicates::PrintDuplicates( const std::string& name, ISvcLocator* pSvcLocator )
    : DaVinciAlgorithm( name, pSvcLocator ) {
  declareProperty( "MaxPrintoutsPerTESLoc", m_maxPrints = 1 );
  declareProperty( "DeepCheck", m_deepCheck = true );
  // setProperty( "OutputLevel", MSG::VERBOSE );
}

//#############################################################################
/// Execution
//#############################################################################
StatusCode PrintDuplicates::execute() {

  typedef std::map<Hash32, LHCb::Particle::ConstVector> PartHashMap;
  typedef std::map<std::string, PartHashMap>            LocHashMap;

  // local map for this event
  LocHashMap hashMap;

  // Loop over particles and compute hashes
  for ( const auto p : particles() ) {
    // sanity check
    if ( !p ) continue;
    // compute the hash for this decay tree
    const auto h = getLHCbIDsHash( p );
    // save this entry
    ( hashMap[tesLocation( p )] )[h].push_back( p );
  }

  // Add a duplicate to test code
  // if ( !particles().empty() )
  // {
  //  const auto p = particles().front();
  //  const auto h = getLHCbIDsHash(p);
  //  (hashMap[tesLocation(p)])[h].push_back(p);
  // }

  // Look for duplicates within TES locations
  for ( const auto& L : hashMap ) {
    // The TES location
    const auto& loc = L.first;

    // Loop over the distinct trees within this TES location
    for ( const auto& PH : L.second ) {
      // The duplicates vector
      const auto& dups = PH.second;

      // do we have any duplicates
      if ( dups.size() > 1 ) {

        // Check the hash values for daughters ?
        if ( !m_deepCheck ) {
          report( loc, dups );
        } else {
          auto filtDups = deepHashCheck( dups );
          for ( const auto& D : filtDups ) { report( loc, D ); }
        }
      }
    }

  } // loop over TES locations

  setFilterPassed( true );
  return StatusCode::SUCCESS;
}

void PrintDuplicates::report( const std::string& loc, const LHCb::Particle::ConstVector& parts ) {
  std::ostringstream mess;
  mess << "Found " << parts.size() << " duplicate Particles in '" << loc << "'";
  Warning( mess.str(), StatusCode::FAILURE, m_maxPrints + 1 ).ignore();
  if ( m_countPerLoc[mess.str()]++ < m_maxPrints ) { printDecay()->printTree( parts.begin(), parts.end() ); }
}

PrintDuplicates::FilteredDups PrintDuplicates::deepHashCheck( const LHCb::Particle::ConstVector& parts ) const {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "Performing deep hash comparison" << endmsg;

  typedef std::map<Hashes64, LHCb::Particle::ConstVector> HashCount;
  HashCount                                               hashCount;

  // Loop over particles
  for ( const auto& P : parts ) {
    // hashes for this particle
    Hashes64 hashes;

    if ( msgLevel( MSG::DEBUG ) )
      debug() << "Get deep hashes for Particle " << P->particleID().pid() << " key " << P->key() << endmsg;

    // Get the deep hashes
    getDeepHashes( P, hashes );

    if ( msgLevel( MSG::DEBUG ) ) debug() << "Deep Hashes : " << hashes << endmsg;

    // Sort
    std::sort( hashes.begin(), hashes.end() );

    // Save in the map
    ( hashCount[hashes] ).push_back( P );
  }

  // Save any duplicates left
  FilteredDups filtDups;
  for ( const auto& H : hashCount ) {
    if ( H.second.size() > 1 ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Deep Hashes : " << H.first << " Particles " << H.second << endmsg;
      filtDups.push_back( H.second );
    }
  }

  return filtDups;
}

void PrintDuplicates::getDeepHashes( const LHCb::Particle* p, Hashes64& hashes, unsigned int depth ) const {
  // protect against infinite recursion
  if ( depth > 999999 ) {
    Warning( "Infinite recursion in getDeepHashes" ).ignore();
    return;
  }
  ++depth;

  // get first hashes for daughters
  Hashes64 dhashes;
  dhashes.reserve( p->daughters().size() );
  for ( const auto& d : p->daughters() ) { getDeepHashes( d, dhashes, depth ); }
  // add to main hashes
  for ( const auto h : dhashes ) { hashes.push_back( h ); }

  // Fill PID type and Particle hash for this particle into a single 64 bit number
  union PIDData {
    struct {
      Hash32 pidType : 32;
      Hash32 hash : 32;
    } packed;
    Hash64 raw;
  } data;
  data.packed.pidType = p->particleID().pid();
  data.packed.hash    = getLHCbIDsHash( p );
  // make a single hash for this particle
  for ( const auto dh : dhashes ) { boost::hash_combine( data.raw, dh ); }

  // Add to the list of hashes
  hashes.push_back( data.raw );

  if ( msgLevel( MSG::DEBUG ) )
    debug() << std::string( depth, ' ' ) << " -> Particle " << p->particleID().pid() << " key " << p->key() << " hash "
            << data.raw << endmsg;
}
