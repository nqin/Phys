/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_TRACKS_H
#define LOKI_TRACKS_H 1
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/Kernel.h"
// ============================================================================
// Event
// ============================================================================
#include "Event/Track.h"
// ============================================================================
// Track Interfaces
// ============================================================================
#include "TrackInterfaces/ITrackSelector.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/BeamSpot.h"
#include "LoKi/ExtraInfo.h"
#include "LoKi/Interface.h"
#include "LoKi/Track.h"
#include "LoKi/TrackTypes.h"
// ============================================================================
#include "KalmanFilter/FastVertex.h"
// ============================================================================
namespace LoKi {
  // ==========================================================================
  /** @namespace LoKi::Tracks Tracks.h LoKi/Tracks.h
   *  Namespace with few basic "track"-functors
   *  @see LHCb::Track
   *
   *  This file is a part of LoKi project -
   *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
   *
   *  The package has been designed with the kind help from
   *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
   *  contributions and advices from G.Raven, J.van Tilburg,
   *  A.Golutvin, P.Koppenburg have been used in the design.
   *
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2007-06-08
   */
  namespace Tracks {
    using namespace LoKi::Track;
    // ========================================================================
    /** @class Filter
     *  Simple class GAUDI_API to use "track-selector"
     *  @see ITrackSelector
     *  @see LoKi:Cuts::TrFILTER
     *  @author Vanya BELYAEV ibelayev@physics.syr.edu
     *  @date 2007-06-10
     */
    class GAUDI_API Filter : public Selector {
    public:
      // ======================================================================
      /// constructor form the tool name
      Filter( const std::string& nick );
      /// MANDATORY: clone method ("virtual constructor")
      Filter* clone() const override { return new Filter( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument t ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// the default constructor is disabled
      Filter(); // the default constructor is disabled
      // ======================================================================
    protected:
      // ======================================================================
      /// set new selector tool
      void getSelector() const;
      // ======================================================================
    private:
      // ======================================================================
      /// the tool typename
      std::string m_nick; // the tool typename
      // ======================================================================
    };
    // ========================================================================
    /**
     *  @class FastDOCAToBeamLine
     *  use TTrDOCA and BEAMSPOT to evaluate the closest distance of a track to
     *  the beam line, similar to BEAMSPOTRHO
     *  @see LoKi::BeamSpot
     *  @see LoKi::Cuts::VX_BEAMSPOTRHO
     *  @author Pieter David pieter.david@cern.ch
     *  @date 2012-02-24
     */
    class GAUDI_API FastDOCAToBeamLine : LoKi::BeamSpot, public LoKi::BasicFunctors<const LHCb::Track*>::Function {
    public:
      // =====================================================================
      /// Constructor from resolver bound
      FastDOCAToBeamLine( const double bound );
      /// Constructor from resolved bound and condition name
      FastDOCAToBeamLine( const double bound, const std::string& condname );
      /// update the condition
      StatusCode updateCondition() override;
      /// MANDATORY: clone method ("virtual constructor")
      FastDOCAToBeamLine* clone() const override { return new FastDOCAToBeamLine( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument v ) const override;
      /// OPTIONAL: nice printout
      std::ostream& fillStream( std::ostream& s ) const override { return s << "Tr_FASTDOCATOBEAMLINE"; }

    private:
      /// beamline
      LoKi::FastVertex::Line m_beamLine;
    };
    // ========================================================================
  } // namespace Tracks
  namespace Cuts {
    // ========================================================================
    /** @typedef  Tr_FASTDOCATOBEAMLINE
     *  Fast DOCA to beam line using TTrDOCA and BEAMSPOT, similar to the
     *  BEAMSPOTRHO cut
     *  @see LoKi::Cuts::TTrDOCA
     *  @see HltUtils::closestDistanceMod
     *  @see LoKi::Tracks::DistanceOfClosestApproach
     *  @see LoKi::BeamSpot
     *  @see LoKi::Cuts::BEAMSPOT
     *  @see LoKi::Vertices::BeamSpotRho
     *  @see LoKi::Cuts::BEAMSPOTRHO
     *  @author Pieter David pieter.david@cern.ch
     *  @date 2012-02-24
     */
    typedef LoKi::Tracks::FastDOCAToBeamLine Tr_FASTDOCATOBEAMLINE;
    // ========================================================================
  } // namespace Cuts
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
// The END
// ============================================================================
#endif // LOKI_TRACKS_H
